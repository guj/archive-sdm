# Alex Sim (ASim@lbl.gov)
# Lawrence Berkeley National Laboratory
# Feb. 10, 2015

#SRC = /Users/asim/Documents/Xcode/Repo/CASCADE/sdmarchive
SRC = . 
INCLUDES = -I. -Wno-deprecated
LIB_FLAGS   = -lpthread
#LIB_FLAGS   = -lnsl -lsocket -lpthread
#LFS_INCLUDES	=	-D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64
DEBUG   = -g -fPIC

#C++	= /usr/local/pkg/gcc/4.9.1/bin/g++
C++ 	= /opt/gcc/4.9.2/bin/g++
.SUFFIXES: .cpp
CC_OBJS = $(EXE_SRC:.cpp=.o)
.cpp.o:
	cd ${<D}; $(C++) $(DEBUG) $(INCLUDES) -c ${<F}

###################################################################
# Source C++ files
###################################################################
HRM_SRC	= sdmmss_nersc.cpp \
		sdmmss_util.cpp \
		sdmmss_config.cpp \
		sdmmss_urlparse.cpp \
		sdmmss_logger.cpp \
		sdmmss_fetcher.cpp \
		sdmmss_fetcher_hsi.cpp

EXE_SRC	= $(HRM_SRC)
OBJS = $(CC_OBJS) 

###################################################################
# Target C++ binary Name
###################################################################
EXE = sdmmss_nersc
LIBFOO = libsdm.so
###################################################################
# For ALL including orbix C++ compilation 
###################################################################
all:	$(EXE) ${LIBFOO}

$(EXE):	$(OBJS)
	$(C++) $(DEBUG) $(OBJS) -o $(EXE) $(LIB_FLAGS) 

$(LIBFOO): $(OBJS)
	$(C++)  -shared -Wl,-soname,${LIBFOO} -o ${LIBFOO} $(OBJS) 

###################################################################
# For clean 
###################################################################
clean: 
	\rm -f $(OBJS) core $(EXE)

######################
depend:
	@ $(C++) $(INCLUDES) -M $(HRM_SRC) >> Makefile

###################################################################
# to make a gzipped tar file of sources
###################################################################
TAR_OBJ	= sdmmss-`/bin/date '+%m%d%y%H%M'`.tar
maketar	:
#	\rm -f $(TAR_OBJ).gz $(TAR_OBJ)
	tar cvf $(TAR_OBJ) *.h *.cpp Makefile* *.sh README.txt
	gzip $(TAR_OBJ)
	chmod 644 $(TAR_OBJ).gz

###################################################################
# for forceful rebuild
# automatically generates dependencies of each source files
###################################################################
rebuild	:
