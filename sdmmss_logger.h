//	File: hrm_logger.h
//	Author: Alex Sim <ASim@lbl.gov>
//	Copyright 1998-2001 the Regents of the University of California
//
//  Purpose: The declaration of class Logger
//
//	Notes:
//		See hrm_logger.cpp (the implementation file) for notes on how to use this class.
//
//	Created: 980831 by Henrik Nordberg
//	Last Modified: 111601

#ifndef HRM_LOGGER_H_
#define HRM_LOGGER_H_

#include "sdmmss_const.h"

#ifdef _WIN32
	#include <windows.h>
	#include <process.h>	// Win32 threads
	#if defined(HAS_POSIX_THREADS)
		#if !defined(_POSIX_C_SOURCE)
			#define _POSIX_C_SOURCE 200809L
		#endif // !defined(_POSIX_C_SOURCE)
		#include <pthread.h>    // POSIX thread library
	#endif
#else
	#if !defined(_POSIX_C_SOURCE)
		#define _POSIX_C_SOURCE 200809L
	#endif // !defined(_POSIX_C_SOURCE)
	#include <pthread.h>    // POSIX thread library
#endif

class Logger {
public:

	enum severity { sev_info, sev_warning, sev_error };
	enum logMethod { log_file }; // log_corba, log_db may be added

	Logger(char const * fn, char const * module_name, logMethod method = log_file, bool append=true);
	virtual ~Logger();

	virtual void Log(char const * s_event, char const * s_query_token=0, char const* fid=0, char const * custom_s=0);
	void use_unsafe_locking(bool locking)
	{
		MutexLock ml1(&m_log_mutex);		// don't want to change what mutex to use, while one of them is in use
		MutexLock ml2(&m_static_log_mutex);
		m_unsafe_locking = locking;
	}

	Logger& operator << (char const * s_event) { Log(s_event); return *this; }

protected:
	virtual void Log_to_file(char const * str, enum severity = sev_error);
	void init(bool b_append);

private:
	// disable copying; these functions have no definitions
	Logger(const Logger& rhs);
	Logger& operator = (const Logger& rhs);
	void renameLogFile();

	pthread_mutex_t m_log_mutex;
	static pthread_mutex_t m_static_log_mutex;
	static int m_num_instances;
	bool m_unsafe_locking;
	char * m_filename;
	char * m_module_name;
	FILE* m_file;
	int m_num_lines;
	int m_max_num_lines;
};

#endif // HRM_LOGGER_H_
