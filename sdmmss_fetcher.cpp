// HRM_FETCHER.CPP
// Alex Sim <ASim@lbl.gov>
// Lawrence Berkeley National Laboratory
// 1999-2003

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>          // remove, sprintf 
#include <stdlib.h>         // system, atoi, rand 
#include <string.h>         // strcat  
#include <sys/stat.h>       // struct stat, chmod 
#include <unistd.h>         // sleep
#include <time.h>
#include <vector>
#include "sdmmss_const.h"
//#include <hrm_i.h>
#include "sdmmss_fetcher.h"
//#include <hrmManager.h>
//#include <hrmFile.h>
//#include <hrmCache.h>
#include "sdmmss_config.h"
#include "sdmmss_util.h"

//extern Config config;

FileFetcher::FileFetcher(mss_location_t mssloc, Config* config, int dlevel, char* msslogin, char* msspasswd) : probabilitySameTape(0), transferOverhead(1) {  
    _config = config;
    time_t tid;
    time(&tid);
    seed48((short unsigned int*) &tid);

    if (config->getValue("MSSRetrial"))
      max_num_trials = atoi(config->getValue("MSSRetrial"));
    else 
      max_num_trials = 7;
    if (config->getValue("MSSWaitTime")) 
      WAIT_TIME = atoi(config->getValue("MSSWaitTime"));
    else 
      WAIT_TIME = 120;
    LONG_WAIT_TIME = 2*WAIT_TIME;
    
    mssLocation = mssloc;
    //
    if ((msslogin != NULL) && (msspasswd != NULL)) {
      mssDefaultLogin = true;
      mssLogin = strdup(msslogin);
      mssPasswd = strdup(msspasswd);
    }
    else mssDefaultLogin = false;
    //
    char host[256];
    char* hostptr=NULL;
    char* hostport=NULL;
    char* tk=NULL;
    strcpy(host, (*config)["MSSHostName"]);
    hostptr = strtok_r(host, ":", &tk);
    if (hostptr != NULL) mssHostName = strdup(hostptr);
    hostport = strtok_r(tk, " ", &tk);
    if (hostport == NULL) {
		mssHostPort = strdup(" ");
	}
	else {
		mssHostPort = strdup(hostport);
	}

	debugLevel = dlevel;
	qout = new aOut (*config, aOut::out_display, dlevel);  // dlevel=10
	//qout = *aout;

    pthread_mutexattr_t    mutex_attr;
    pthread_mutexattr_init(&mutex_attr);
#ifndef LINUX
    pthread_mutexattr_setpshared(&mutex_attr, PTHREAD_PROCESS_SHARED);
#endif
	if( pthread_mutex_init(&tO_mutex,&mutex_attr) != 0 ) {
		std::cout << "pthread_mutex_init() failed in FileFetcher() ctor" << std::endl;
		throw "pthread_mutex_init() failed in FileFetcher() ctor";
	}
	if( pthread_mutex_init(&pST_mutex,&mutex_attr) != 0 ) {
		std::cout << "pthread_mutex_init() failed in FileFetcher() ctor" << std::endl;
		throw "pthread_mutex_init() failed in FileFetcher() ctor";
	}
}

FileFetcher::~FileFetcher() {
	delete [] mssHostName;
	delete [] mssHostPort;
	pthread_mutex_destroy(&tO_mutex);
	pthread_mutex_destroy(&pST_mutex);
}

char*  FileFetcher::getHostName() {
	return mssHostName;
}

char*  FileFetcher::getHostPort() {
	return mssHostPort;
}

//
// getTransferOverhead() returns the transferOverhead for the last transfer.
// the transfer overhead is the ratio between the total transfer time from 
// HPSS to local cache and the real transfer time, i.e., the time between 
// HPSS cache to local cache. A large ratio means that there was a tape 
// mounting.
double FileFetcher::getTransferOverhead() {
	double to;
	pthread_mutex_lock(&tO_mutex);
	to = transferOverhead;
	pthread_mutex_unlock(&tO_mutex);
	return to;
}

//
// setTransferOverhead() does that.
//
void FileFetcher::setTransferOverhead(double to) {
	pthread_mutex_lock(&tO_mutex);
	transferOverhead = to;
	pthread_mutex_unlock(&tO_mutex);
	return;
}

//
// setProbabilitySameTape() sets the probability that the next requested
// file will be from the same tape as the file just transfered, if such
// file exists. otherwise the file from the head of the wait queue will be
// served. the purpose of this feature is to avoid excessive mounting and
// dismounting of tapes, but without overdoing it by overskipping the files
// at the head of the queue. this function takes as argument a int that
// characterizes the average number of files that should be requested from 
// a tape before requesting the file at the head of the queue. this value
// is used to calculate the probability given by p = n/(n+1). by default a
// value n=0 is assumed, which corresponds to p = 0. to change the
// default to a different value launch the cmanager with the switch -t n, 
// where n is the desired value.
void FileFetcher::setProbabilitySameTape(int n) {
	pthread_mutex_lock(&pST_mutex);
	probabilitySameTape = (float) n/(n+1);
	pthread_mutex_unlock(&pST_mutex);
}

//
// getProbabilitySameTape() does that
//
float FileFetcher::getProbabilitySameTape() {
	float pst = 0;
	pthread_mutex_lock(&pST_mutex);
	pst = probabilitySameTape;
	pthread_mutex_unlock(&pST_mutex);
	return pst;
}

char* FileFetcher::getStringID() {
    time_t tid;
    time(&tid);
    char strid[256];
    sprintf(strid,"%lx.%lx", tid, static_cast<unsigned long int>(drand48()*1000000000L) );
    char* sid= new char[16];
    strncpy(sid,strid,15);
    sid[15]=0;

	return sid;
}
