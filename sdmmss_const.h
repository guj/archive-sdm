//	File: hrm_const.h
//	Author: Alex Sim <ASim@lbl.gov>
//
//  Purpose: Defines constants and macros
//
//	Created: 111601

#ifndef hrm_const_h
#define hrm_const_h

#include <errno.h>	// errno
#include <cerrno>	// errno
#include <vector>
#include <string>

// Note: <iostream.h> or <iostream> are not included, since people
// might have opinions as to which one is the right one.
// (so include one of them before this one, since this one uses cerr)

// Compiler independent definitions:

#define SDMMSS_VERSION "SDMarchive API v0.1"
#define SDMMSS_DATE    "Fri Feb 13 13:07:45 PST 2015"
#define SDMMSS_COPYRIGHT "Copyright (c) 1998-2015 Lawrence Berkeley National Laboratory"

const int MAX_LINE = 2048;
const int MAX_BINS = 1024;
const int MAX_PROPERTIES = 256;
const int MAX_SQL_QUERY_LENGTH = 512;
const float LOW_STAT_FLOAT = -999.;
const int LOW_STAT_ERROR = 65534;
const int MEGABYTE = 1048576;

#ifndef mmax
	#define mmax(x,y) ((x)>(y))?(x):(y)
#endif

// to accomodate the previous declaration in CM
//typedef unsigned long HID_T;

// Compiler dependent definitions:

#if defined(_WIN32)
	#include <windows.h>
	#define mkdir(x,y) _mkdir(x)
	#define chmod(x,y) 0			// not needed for Win32
	#include <stdlib.h>				// _MAX_PATH
	#define PATH_MAX _MAX_PATH

//	#define ctime_r(t,buff) {char* b = ctime(t); strcpy(buff,b); }

	#define drand48 rand
	#define srand48 srand

//	#define  MessageBox(x1,x2,x3,x4); {} // fake message box
	#define strnicmp strncasecmp
	#define stricmp strcasecmp

#elif defined(__GNUC__)
    #include <limits.h> // PATH_MAX

#else
	#include <syslimits.h> // PATH_MAX

#endif

#ifdef LINUX
#define getpassphrase getpass
#endif


#define REASON std::cerr << "Reason: " << strerror(errno) << std::endl;
#define edl	"\n"

//namespace HRM {   // This is from hrmDefs.idl
  typedef long long MSS_TIME_OUT_T;
  typedef std::string MSS_REQUEST_ID_T;
  typedef std::string MSS_FID_T;
  typedef std::vector<MSS_FID_T> MSS_FIDSET_T;
  typedef std::string MSS_HINT_T;

  enum MSS_ACCESS_TYPE_T {
    MSS_IMPLICIT,   // use access info for the request not for the file in this case
    MSS_PLAIN,  // plain login/passwd for ftp/pftp - BNL
    MSS_GSI,    // GSI proxy string after gss_export_t - LBNL
    MSS_ENCRYPT,    // SSH proxy string from netrc - LBNL
    MSS_KERBEROS, // kerberos info - ORNL
    MSS_SSH,        // ssh after HRM's public key is inserted (put case)- NCAR
    MSS_SCP,        // generic ssh - SPs
    MSS_NONE      // when none required
  };

  struct MSS_ACCESS_INFO {
    std::string login;
    std::string passwd;   // GSI and other security info
    MSS_ACCESS_TYPE_T type;
    std::string project_id;  // NCAR specific
    std::string read_passwd;  // NCAR specific
    std::string write_passwd;  // NCAR specific
    long retention;  // in days, NCAR specific
  };
  typedef MSS_ACCESS_INFO MSS_ACCESS_INFO_T;

  enum MSS_MODE_T {
    MSS_PULL, MSS_PUSH
  };
  struct MSS_REQUEST {
    MSS_FID_T fid;          // logical file name
    std::string source_url;  // physical source file path in URL
    std::string target_url;  // physical remote file path in URL
    long long size;     // in bytes
    MSS_HINT_T hint;        // some hint for efficient retrieval/migration
    MSS_ACCESS_INFO_T at_source; // access info at source MSS, used in get
    MSS_ACCESS_INFO_T at_target; // access info at target MSS, used in get/put
  };
  typedef MSS_REQUEST MSS_REQUEST_T;
  typedef std::vector<MSS_REQUEST_T> MSS_REQUEST_SET_T;

  enum MSS_OVERWRITE_MODE_T {
    MSS_ALWAYS_YES, MSS_ALWAYS_NO, MSS_YES_SIZE_DIFF, MSS_YES_SIZE_LESS
  };

  enum MSS_RETURN_CODE_T {
// NR : Non-recoverable error - no need to re-try
// ADV : Advisory - no action is needed
    MSS_REQUEST_QUEUED,         // 0
    MSS_REQUEST_DONE,           // 1
    MSS_REQUEST_FAILED,         // 2
        // NR
    MSS_UNKNOWN_REQUEST,        // 3
        // invalid request (id) or unknown request id - NR
    MSS_NOT_ENOUGH_SPACE,       // 4
        // cache is not enough - NR
    MSS_FILE_DOES_NOT_EXIST,    // 5
        // file path is wrong on MSS or in disk - NR 
    MSS_RELEASE_DONE,           // 6
    MSS_RELEASE_FAILED,         // 7
        // should not affect the caller
    MSS_ABORT_DONE,             // 8
    MSS_ABORT_FAILED,           // 9
        // should not affect the caller
    MSS_FATAL_MSS_ERROR,        // 10
        // MSS in fatal error - NR
    MSS_ERROR,              // 11 
        // MSS down or in error and TRM will keep in the queue until succeed
        // ADV
    MSS_FILE_TIMED_OUT,         // 12 
        // file pin expired
    MSS_DUPLICATE_REQUEST_ID,   // 13
        // existing request id for a new request - get a new id
    MSS_FILE_TRANSFERRED_TO_CLIENT,     // 14
        // When SRM pushes the file into the client's place
        // ADV
    MSS_FILE_TRANSFERRED_TO_SRMCACHE,   // 15
        // When a file gets into the SRM managed disk cache
        // ADV
    MSS_FILE_PINNED,            // 16
        // caller may pull the data from TURL.
    MSS_FILE_LIMIT_REACHED,     // 17
        // number of files limit in the disk is reached.
        // ADV
    MSS_FILE_SIZE_LIMIT_REACHED,    // 18
        // number of file size limit in the disk is reached.
        // ADV
    MSS_USER_NOT_AUTHORIZED,    // 19
        // user has no permission to open/write a file into the path - NR
    MSS_USER_NOT_AUTHENTICATED, // 20
        // user has wrong authentication information - NR for the same request
        // (i.e. bad proxy, wrong login/passwd)
    MSS_FILE_ALREADY_ON_MSS,    // 21
        // for MSS archival, depending on overwriting mode, it is ADV.
        // if overwriting mode is ALWAYS_NO, REQUEST_DONE will follow.
    MSS_NO_DIRECTORY_PERMISSION,    // 22
        // user has no permission to open/write a file into the path - NR
        // can be applied on both for disk and MSS
    MSS_FILE_WRITTEN_ON_TAPE,   // 23
        // for MSS archival, ADV
    MSS_SPACE_ALLOCATED,        // 24
        // caller may push the data into the TURL
    MSS_FILE_ALREADY_ON_LOCAL,  // 25
        // for MSS retrieval, files will not be over-written to the 
        // target local path without -overwrite option for TRM. 
        // REQUEST_DONE will follow.
    MSS_FILE_SIZE_ZERO          // 26
        // depending on the option, zero length files will not be 
        // replicated from MSS.
  };

  typedef std::string MSS_PROTOCOL_T; // for string

  struct MSS_STATUS_CODE {
    MSS_RETURN_CODE_T code;
    std::string explanation;
  };
  typedef MSS_STATUS_CODE MSS_STATUS_CODE_T;

  struct MSS_RETURN_STATUS {
    MSS_FID_T fid;
    MSS_STATUS_CODE_T code;
  };
  typedef MSS_RETURN_STATUS MSS_RETURN_STATUS_T;
  typedef std::vector<MSS_RETURN_STATUS_T> MSS_REQUEST_STATUS_SET_T;

  struct MSS_FILE_STATUS {
    MSS_FID_T fid;
    std::string returnUrl;
    long long timeToService;
    unsigned long long size;
    MSS_STATUS_CODE_T status;
    MSS_TIME_OUT_T pin_expiration;
    MSS_TIME_OUT_T pin_duration;
  };
  typedef MSS_FILE_STATUS MSS_FILE_STATUS_T;
  typedef std::vector<MSS_FILE_STATUS_T> MSS_FILE_STATUS_SET_T;

  struct MSS_FILE_INFO {
    MSS_FID_T fid;
    unsigned long long size;
  };
  typedef MSS_FILE_INFO MSS_FILE_INFO_T;
  typedef std::vector<MSS_FILE_INFO_T> MSS_FILE_INFO_SET_T;

  struct MSS_PATH {
    std::string dir;
    MSS_FILE_INFO_SET_T files;
    std::vector<MSS_PATH> sub_dir;
  };
  typedef MSS_PATH  MSS_PATH_SET_T;

//}


struct MutexLock {
    MutexLock(pthread_mutex_t *mutex) { if( pthread_mutex_lock( m_mutex = mutex ) != 0 ) throw "pthread_mutex_lock() failed"; }
    ~MutexLock() { if( pthread_mutex_unlock( m_mutex ) != 0 ) throw "pthread_mutex_unlock() failed"; }
    void lock() { if( pthread_mutex_lock( m_mutex ) != 0 ) throw "pthread_mutex_lock() failed"; }
    void unlock() { if( pthread_mutex_unlock( m_mutex ) != 0 ) throw "pthread_mutex_lock() failed"; }
private:
    pthread_mutex_t *m_mutex;
};


#endif // ifndef hrm_const_h
