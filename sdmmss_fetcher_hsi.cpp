// HRM_FETCHERHSI.CPP
// Alex Sim <ASim@lbl.gov>
// Lawrence Berkeley National Laboratory
// 1999-2007

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>          // remove, sprintf 
#include <stdlib.h>         // system, atoi, rand 
#include <string.h>         // strcat  
#include <sys/stat.h>       // struct stat, chmod 
#include <sys/statvfs.h>
#include <unistd.h>         // sleep
#include <time.h>
#include <vector>
#include "sdmmss_const.h"
#include "sdmmss_fetcher_hsi.h"
//#include "hrmCache.h"
#include "sdmmss_config.h"
#include "sdmmss_util.h"
#include "sdmmss_urlparse.h"

//extern Config config;
//extern HPSSResourceManager *cm;
//extern Cache *cache;
//extern aOut qout;  
//#define 	qout	std::cout
//#define		edl		"\n"

bool hsi_filename_compare(char* c1, char* c2) {
		if ((c1 == NULL) || (c2 == NULL)) return false;
        if (strstr(c1, c2) != NULL) return true;
        else return false;
}


FileFetcherHSI::FileFetcherHSI(Config* config, mss_location_t mssloc, int dlevel, char* hpsslogin, char* hpsspasswd) : FileFetcher(mssloc, config, dlevel, hpsslogin, hpsspasswd) {
	debugLevel = dlevel;
	// debugLevel = 100;
}

FileFetcherHSI::~FileFetcherHSI() {
	delete [] hsipath;
}

/*
? ls -l /nersc/gc5/asim/tiny.test
/nersc/gc5/asim:
-rw-r-----   1 asim      gc5               11 Aug 30  2004 tiny.test

? ls -l /nersc/gc5/junmin/tiny.test
*** hpss_Lstat: No such file or directory [-2: HPSS_ENOENT] 
    /nersc/gc5/junmin/tiny.test

? ls /nersc/gc5/junmin
 
/nersc/gc5/junmin:
3G            hey_tiny/     junmin/       little/       one/          smallfile 
    tiny/         write/        xysmail.txt   

? ls -l /nersc/gc5/asim/tiny3.test            
/nersc/gc5/asim:
-rw-r--r--   1 asim      gc5               80 May 15  2007 tiny3.test

? ls -l /nersc/gc5/asim/mytest
*** hpss_Lstat: No such file or directory [-2: HPSS_ENOENT] 
    /nersc/gc5/asim/mytest

? ls -l /nersc/gc5/asim/test2/\*.2022*.nc
/nersc/gc5/asim/test2:
-rw-r-----   1 asim      gc5        390083812 Jun 20  2003 B06.44.atm.2022-01_hb
.nc
-rw-r-----   1 asim      gc5        352334560 Jun 20  2003 B06.44.atm.2022-02_hb
.nc
-rw-r-----   1 asim      gc5        390083812 Jun 20  2003 B06.44.atm.2022-03_hb
.nc
-rw-r-----   1 asim      gc5        377500728 Jun 20  2003 B06.44.atm.2022-04_hb
.nc
-rw-r-----   1 asim      gc5        390083812 Jun 20  2003 B06.44.atm.2022-05_hb
.nc

? ls -d /nersc/gc5/zimm/ScaTest/RUN1_100/
/nersc/gc5/zimm/ScaTest/RUN1_100:
/          

? ls -d /nersc/gc5/zimm/ScaTest/RUN1_100/Event
/nersc/gc5/zimm/ScaTest/RUN1_100:
Event/  

? ls -d /nersc/gc5/zimm/ScaTest/
/nersc/gc5/zimm/ScaTest:
/         

? ls -V /nersc/gc5/asim/tiny.test
/nersc/gc5/asim:
-rw-r-----   1 asim      gc5           4     8850 DISK            11 Aug 30  200
4 tiny.test
Storage   VV   Stripe  
 Level   Count  Width  Bytes at Level
----------------------------------------------------------------------------
 1 (tape)   1       1  11
  VV[ 0]:   Object ID: 5607ee94-f7e3-11d8-bde1-0004ac493f6a
            ServerDep: e65fcc4a-7833-11d2-a346-0004ac493f6a
  Pos:   29   PV List: EA333300  

? ls -V /nersc/gc5/asim/tiny.test2
*** hpss_Lstat: No such file or directory [-2: HPSS_ENOENT] 
    /nersc/gc5/asim/tiny.test2


ls:
-a : list all entries, including "hidden" files whose names begin with "."
-c : use time of last modification for sorting (deferred)
-d : if file is a dir, list its name instead of its contents
-l : long list format
-p : put a slash after each name if the file is a directory (deferred)
-r : reverse alpha or age sort order, as appropriate (deferred)
-s : display size as well as name if -1 (numeral 1) option used
-u : use time of last access for sorting instead of last modification (deferred)
-x : multicolumn output format, with entries sorted across page (deferred)
-A : print annotation info
-C : multicolumn output format, with entries sorted down the columns (deferred)
-F : puts a / after directory filenames, or * if executable (deferred)
-H : print headings on long listings
-O : print unordered "-l" or "-1" format listings
-P : print one-line with position/volume info
-R : recursively list directories

-T : "type" where type is one of w,r,c or m. This allows the user to specify whi
ch HPSS time value is displayed when one of the "long list" options (e.g. "ls -l
") is used. The letters stand for:

    w - last write time (default)
    r - last read time
    c - file creation time
    m - time of last metadata update 

-U : print HPSS-specific information
-V : print volume for 1st tape level
-X : print extended volume info (for all levels)
-1 : (numeral 1) forces one name-per-line list 


? ls -lR test.llnl
 
/nersc/gc5/asim/test.llnl:
drwxr-s---   2 asim      gc5              512 Nov 30  2004 1%_to2x
-rw-r-----   1 asim      gc5         12837259 Nov 30  2004 asim.test
-rw-r-----   1 asim      gc5         12837259 Nov 30  2004 asim.test2
drwxr-s---   2 asim      gc5              512 Nov 30  2004 test2
drwxr-s---   2 asim      gc5              512 Nov 30  2004 test3
 
/nersc/gc5/asim/test.llnl/1%_to2x:
-rw-r-----   1 asim      gc5         12837259 Nov 30  2004 asim.test6
 
/nersc/gc5/asim/test.llnl/test2:
-rw-r-----   1 asim      gc5         12837259 Nov 30  2004 asim.test3
 
/nersc/gc5/asim/test.llnl/test3:
-rw-r-----   1 asim      gc5         12837259 Nov 30  2004 asim.test4


? ls -l /nersc/gc5/junmin/little
*** hpss_Opendir: Access denied  [-13: HPSS_EACCES] 
    /nersc/gc5/junmin/little
/nersc/gc5/junmin/little:

O:[/home/asim]: get /home/asim/hi4
*** no such HPSS file: /home/asim/hi4t

O:[/home/asim]: ls -l /home/asim/hi4  
*** hpss_Lstat: No such file or directory [-2: HPSS_ENOENT] 
    /home/asim/hi4


HSI_ERROR_CODES=EXIT_CODE_BELOW

EX_USAGE 	64 	Command line usage error. 
                The command was used incorrectly (wrong number of arguments, 
				bad flag, bad syntax in a parameter, or whatever).

EX_DATAERR 	65 	Data format error. The user's input data was incorrect 
				in some way.

EX_NOINPUT 	66 	Cannot open input. User's input file did not exist or 
				was not readable.

EX_NOUSER 	67 	Addressee unknown. User specified does not exist.

EX_NOHOST 	68 	Host name unknown. Host specified does not exist. 
				Used in mail addresses or network requests.

EX_UNAVAILABLE 	69 	Service unavailable. Occurs if a support program or 
					file does not exist. Can be used as a catchall message 
					when not known why something is not working.

EX_SOFTWARE 70 	Internal software error inconsistency, etc. in HSI 
					code (non-operating system- related errors).

EX_OSERR 	71 	Operating system error, such as cannot fork, cannot 
				create pipe, getuid, returning a non-existent user, etc..

EX_OSFILE 	72 	Critical OS file missing, cannot be opened, or has some 
				sort of error (syntax error).

EX_CANTCREAT 	73 	Cannot create user output file.

EX_IOERR 	74 	Input/output error, occurred while doing I/O on some file.

EX_TEMPFAIL 75 	Temporary failure. User is invited to retry.

EX_PROTOCOL 76 	Remote error in protocol. The remote system returned 
					something that was not possible during a protocol exchange.

EX_NOPERM 	77 	Permission denied. User did not have sufficient permission 
				to perform the operation.

*/

mss_message_t FileFetcherHSI::getHSIError(char* logpath) {
    const int SIZE = 1024;   // some buffer size
    FILE* inputfile= fopen(logpath,"r");
    if (!inputfile) {
        (*qout) << "ERROR: Could not read " << logpath << edl;
        char why[128];
        strcpy(why, "mss listing failed");
        throw why;
    }
    char line[SIZE];
    if (debugLevel > 7) (*qout) << "HSI output" << edl;

    //mss_message_t msserr= mss_other_error;
    mss_message_t msserr= mss_transfer_done;
    while( !feof(inputfile) ) {
        if( fgets(line,SIZE,inputfile) == 0 ) break;
        if( !line[0] || line[0]=='\n' || line[0]=='\r') {
             continue;
        }
        line[ strlen(line) - 1 ] = 0; // get rid of the '\n'
        if ( 0 != strstr(line,"HSI_ERROR_CODES=") ) {
	    if (_config->getValue("DEBUG")) (*qout) << line << edl;
            char* ervalue = strchr(line,'=') + 1;
            ervalue[-1] = 0;
            msserr=getHSIErrorByValue(atoi(ervalue));
	    break;
        } else if (0 != strstr(line, "***") ) { // e.g. " *** ls: No such file or directory [-2: HPSS_ENOENT] "
	    if (_config->getValue("DEBUG")) (*qout) << line << edl;
	    msserr=mss_error;
	}
    }

    fclose(inputfile);

    return msserr;
}

mss_message_t FileFetcherHSI::getHSIErrorByValue(int errcode) {
/*
    mss_connection_ok,
    mss_transfer_done,
    mss_command_successful,
    mss_file_exists,
    mss_no_such_file,
    mss_no_such_directory,
    mss_no_access_permission,
    mss_limit_reached,
    mss_error,
    mss_system_not_available,
    mss_wrong_login,
    mss_gss_failure,
    mss_other_error
*/

	mss_message_t mycode=mss_other_error;

	switch (errcode) {
		case 0:
			(*qout) << "HSI command OK" << edl;
			mycode=mss_command_successful;
			break;
		case 64:
			(*qout) << "HSI command usage error" << edl;
			mycode=mss_other_error;
			break;
		case 65:
			(*qout) << "HSI input data format error" << edl;
			mycode=mss_other_error;
			break;
		case 66:
			(*qout) << "HSI cannot open input" << edl;
			mycode=mss_other_error;
			break;
		case 67:
			(*qout) << "User known" << edl;
			mycode=mss_wrong_login;
			break;
		case 68:
			(*qout) << "Unknown hostname " << edl;
			mycode=mss_system_not_available;
			break;
		case 69:
			(*qout) << "Service not available" << edl;
			mycode=mss_system_not_available;
			break;
		case 70:
			(*qout) << "internal software error inconsistency in HSI code" << edl;
			mycode=mss_error;
			break;
		case 71:
			(*qout) << "OS error" << edl;
			mycode=mss_error;
			break;
		case 72:
			(*qout) << "File is missing" << edl;
			mycode=mss_no_such_file;
			break;
		case 73:
			(*qout) << "Cannot create user output file" << edl;
			mycode=mss_other_error;
			break;
		case 74:
			(*qout) << "Input/output error, occurred while doing I/O on some file" << edl;
			mycode=mss_other_error;
			break;
		case 75:
			(*qout) << "Temporary failure. User is invited to retry" << edl;
			mycode=mss_system_not_available;
			break;
		case 76:
			(*qout) << "Remote error in protocol" << edl;
			mycode=mss_error;
			break;
		case 77:
			(*qout) << "Permission denied. User did not have sufficient permission to perform the operation" << edl;
			mycode=mss_no_access_permission;
			break;
		case 255:
			(*qout) << "Connection refused" << edl;
			mycode=mss_error;
			break;
		default:
			(*qout) << "HSI command usage error" << edl;
			mycode=mss_other_error;
			break;
	}
	return mycode;
}

//HRM::PATH_SET_T* FileFetcherHSI::srmls(const char* uid, const char* requestID,
//                                const char* path, HRM::ACCESS_INFO_T source,
//								bool recursive, bool toplevel) {
MSS_PATH_SET_T* FileFetcherHSI::srmls(const char* path, MSS_ACCESS_INFO_T* source,
				      bool recursive, bool toplevel) {
//	HRM::PATH_SET_T* temp = new HRM::PATH_SET_T;
    MSS_PATH_SET_T* temp = new MSS_PATH_SET_T;
    char tfilename[1024]; char mypath[1024];
    bool lspattern=false;
    if ((strrchr(path, '/') == NULL) && ((strrchr(path, '/')+1) != NULL)) {
        strcpy(mypath, path);    
    } else {
        strcpy(tfilename, strrchr(path, '/')+1);
        if (strchr(tfilename, '*') == NULL) {
            strcpy(mypath, path);
        }
        else {
            strncpy(mypath, path, strlen(path)-strlen(tfilename)-1);
	    mypath[strlen(path)-strlen(tfilename)-1] = 0;
	    lspattern=true;
        }
    }

    if (debugLevel > 7) {
        (*qout) << "MY SRMLS PATH: " << mypath << edl;
	(*qout) << "MY SRMLS FILENAME: " << tfilename << edl;
    }

    temp->dir.assign(mypath);

// need to add use id or session id to be unique
    char* sid = getStringID();
    char logftpmssg[1024];
    sprintf(logftpmssg, "%s.lslog.%s", _config->getValue("MSSLogFile"), sid);

// without * in the filename
    mss_message_t lsresult = mssListing(mypath, source, logftpmssg);

// AS2015
//	if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
    if (( lsresult == mss_transfer_done ) || (lsresult == mss_command_successful)) {
        const int SIZE = 1024;   // some buffer size
	FILE* inputfile= fopen(logftpmssg,"r");
	if (!inputfile) {
	   (*qout) << "ERROR: Could not read " << logftpmssg << edl;
	   char why[128];
	   strcpy(why, "mss listing failed");
	   throw why;
	   return temp;
	}

	std::vector<char*> breakfile;

        if (lspattern) {
	    char tfilename2[1024];
	    strcpy(tfilename2, tfilename);
	    char *trfilename=NULL;
	    char *tsfilename=NULL;
	    for (trfilename=strtok_r(tfilename2,"*", &tsfilename); trfilename!=NULL; trfilename=strtok_r(tsfilename,"*", &tsfilename)) {
	       breakfile.push_back(strdup(trfilename));
	    }
	    if (debugLevel > 7) {
	        (*qout) << "parsing files for star: ";
		for (int ti=0;ti<breakfile.size(); ti++) {
		     (*qout) << breakfile[ti] << " ";
		}
		(*qout) << edl;
	    }
	}
	
	char line[SIZE];
	int totalcount=0;
	if (debugLevel > 7) (*qout) << "HSI output" << edl;
	bool parsestart=false;
	bool parseend=false;
	bool onefile=false;
	std::vector<char*> dirs;
	std::vector<char*> fids;
	std::vector<long long> sizes;
	
	while( !feof(inputfile) ) {
	  if( fgets(line,SIZE,inputfile) == 0 ) break;
	  if( !line[0] || line[0]=='\n' || line[0]=='\r') {
	    continue;
	  }
	  line[ strlen(line) - 1 ] = 0; // get rid of the '\n'
	  if (_config->getValue("DEBUG")) (*qout) << line << edl;
	  if ( 0 != strstr(line,"HSI_ERROR_CODES=") ) {
	    char* ervalue = strchr(line,'=') + 1;
	    ervalue[-1] = 0;
	    mss_message_t msserr=getHSIErrorByValue(atoi(ervalue));
	    if (atoi(ervalue)> 0) {
	      char why[1024];
	      sprintf(why, "ERROR %s while browsing to %s", ervalue, path);
	      throw why;
	    }
	  }
	  if ( 0 != strstr(line,"-13: HPSS_EACCES") ) {
	    (*qout) << "ACCESS denied to " << path << edl;
	    char why[1024];
	    strcpy(why, "ACCESS denied to ");
	    strcat (why, path);
	    throw why;
	  }
	  
	  if ((line[0] == 'd') || (line[0] == '-')) {
	    // actual parsing for file sizes and the file names
	    /*
	      PARSED: -rw-r-----
	      PARSED: 1
	      PARSED: asim
	      PARSED: gc5
	      PARSED: 22921216
	      PARSED: Jan
	      PARSED: 3
	      PARSED: 12:37
	      PARSED: set163_01_32evts_dst.xdf.STAR.DB
	    */
	    std::vector<char*> parsing;
	    char* tk=NULL;
	    for (char *ti=strtok_r(line, " ", &tk); ti != NULL; ti=strtok_r(tk, " ", &tk)) {
	        char value[1024];
		strcpy(value,"");
		char* tj=strchr(ti,' ');
		if (tj == NULL) strcpy(value, ti);
		else {
		  for (; tj!=NULL; tj=strchr(value, ' ')) {
		    strcat(value, tj+1);
		  }
		}
		parsing.push_back(strdup(value));
	    }
	    if (debugLevel > 7) (*qout) << "HERE: " << path << " " << parsing[8] << " " << parsing[4] << edl;
	    if (parsing[0][0] == 'd') {
	      if (debugLevel > 7) (*qout) << "DIRECTORY: " << parsing[8] << edl;
	      dirs.push_back(strdup(parsing[8]));
	    }
	    else {
	      totalcount++;
	      if (totalcount == 1) {
		if (!strcmp(path, parsing[8])) {
		  if (debugLevel > 7) (*qout) << "ONE FILE request!" << edl;
		}
	      }
	      if (lspattern) {
		bool lspresult=true;
		int lspi=0;
		while  (lspresult && lspi<breakfile.size()) {
		  if ((lspresult = hsi_filename_compare(parsing[8], breakfile[lspi])) == false) {
		    break;
		  }
		  lspi++;
		}
		if (lspresult) {
		  fids.push_back(strdup(parsing[8]));
		  sizes.push_back(atoll(parsing[4]));
		}
		else {
		  if (debugLevel > 7) (*qout) << "SKIPPING: " << parsing[8] << edl;
		  totalcount--;
		}
	      }
	      else {
		fids.push_back(strdup(parsing[8]));
		sizes.push_back(atoll(parsing[4]));
	      }
	    }
	    for (int ki=0; ki<parsing.size(); ki++) {
	      delete parsing[ki];
	    }
	  }
	}
	fclose(inputfile);
	if (!_config->is_true("EnableDetailedLog")) unlink(logftpmssg);
	
	
	//		HRM::FILE_INFO_SET_T* fset = new HRM::FILE_INFO_SET_T;
	MSS_FILE_INFO_SET_T* fset = new MSS_FILE_INFO_SET_T;
// AS2015
//	fset->length(fids.size());
	if (debugLevel>2) {
	  (*qout) << "SRMLS: DIRS=" << path << edl;
	  (*qout) << "TOTAL count=" << totalcount << edl;
	  (*qout) << "TOTAL2count=" << (int) fids.size() << "=" << (int) sizes.size() << edl;
	}
	for (int ii=0; ii<fids.size(); ii++)  {
	  //				HRM::FILE_INFO_T* finfo = new HRM::FILE_INFO_T;
	  MSS_FILE_INFO_T* finfo = new MSS_FILE_INFO_T;
	  finfo->fid.assign(fids[ii]);
	  delete fids[ii];
	  finfo->size = sizes[ii];
	  //(*fset)[ii] = *finfo;
	  (*fset).push_back(*finfo);
	  if (debugLevel>2) (*qout) << "SRMLS: FILE=" << finfo->fid << edl;
	}
	temp->files = *fset;
// AS2015
//		(temp->sub_dir).size(dirs.size());
	
	// recursive directory listing now
	for (int kk=0; kk<dirs.size(); kk++) {
	     char newdir[1024];
	     strcpy(newdir, path);
	     strcat(newdir, "/");
	     strcat(newdir, dirs[kk]);
	     if (debugLevel>2) { 
	        (*qout) << "dirs[" << kk << "]=" << newdir << edl; 
	     } 
	     if (recursive && !lspattern) { 
	       //				HRM::PATH_SET_T* newpath = srmls(uid, requestID, newdir, source, recursive, false); 
	       MSS_PATH_SET_T* newpath = srmls(newdir, source, recursive, false); 
	       (temp->sub_dir)[kk] = *newpath; 
	     } 
	     else { 
	       //				HRM::PATH_SET_T* rtemp = new HRM::PATH_SET_T;
	       MSS_PATH_SET_T* rtemp = new MSS_PATH_SET_T;
	       rtemp->dir.assign(newdir);
	       //				HRM::FILE_INFO_SET_T* rfset = new HRM::FILE_INFO_SET_T;
	       MSS_FILE_INFO_SET_T* rfset = new MSS_FILE_INFO_SET_T;
// AS 2015
//				rfset->length(0);
	       rtemp->files = *rfset;
// AS 2015
//				(rtemp->sub_dir).length(0);
	       (temp->sub_dir)[kk] = *rtemp;
	     }
	}
// remove dirs later
	if ((fids.size() ==0) && (dirs.size()==0) && toplevel) {
	    if (debugLevel>2) (*qout) << "SRMLS: empty or non existing path" << edl;
	    char why[128];
	    strcpy(why, "empty directory or non-existing path");
	    throw why;
	}
	unlink(logftpmssg);
    }
    else if ( lsresult == mss_other_error ) {
        char why[128];
        strcpy(why, "mss listing failed for unknown reason");
        throw why;
    }
    else if (( lsresult == mss_no_such_file ) || (lsresult == mss_no_such_directory)) {
        char why[128];
        strcpy(why, "NO such file or directory");
        throw why;
    }
    else if ( lsresult == mss_no_access_permission ) {
        char why[128];
        strcpy(why, "No access permission");
        throw why;
    } else if ( lsresult == mss_limit_reached ) {
        char why[128];
        strcpy(why, "MSS access limit reached. Try again later");
        throw why;
    } else if ( lsresult == mss_gss_failure) {
        char why[128];
        strcpy(why, "GSS authentication failure");
        throw why;
    } else if ( lsresult == mss_wrong_login ) {
        char why[128];
        strcpy(why, "Incorrect MSS login");
        throw why;
    } else if (lsresult == mss_system_not_available) {
// AS2015
//		cm->setLastTransferRate(-1.0);
        char why[128];
        strcpy(why, "MSS not available");
        throw why;
    } else if (lsresult == mss_error) {
// AS2015
//		cm->setLastTransferRate(-1.0);
        char why[128];
        strcpy(why, "MSS error");
        throw why;
    } else {
        char why[128];
        strcpy(why, "mss listing failed for unknown reasons");
        throw why;
    }

    return temp;
}


//mss_message_t FileFetcherHSI::mssListing(const char* rpath, HRM::ACCESS_INFO_T source, char* logftpmssg) {
mss_message_t FileFetcherHSI::mssListing(const char* rpath, MSS_ACCESS_INFO_T* source, char* logftpmssg) {
    mss_message_t result = mss_transfer_done;

    char* mylogin = NULL;
    char* mypasswd = NULL;
// AS2015
//  if (!source.login.empty()) TSRMCrypto::decrypt(source.login.c_str(), &mylogin);
//  if (!source.passwd.empty()) TSRMCrypto::decrypt(source.passwd.c_str(), &mypasswd);

    (*qout) << "TYPE: " << source->type << edl;

    char ftptest[4096];
//	if ((source.type == HRM::NONE) || (source.type == HRM::KERBEROS)) {
    if ((source->type == MSS_NONE) || (source->type == MSS_IMPLICIT)) {
      sprintf(ftptest, "#! /bin/sh\n \
%s -q \"out %s; ls -l %s ; end\"     \
echo \"HSI_ERROR_CODES=\"$? >> %s    \
echo \"Date: `date`\" >> %s",
_config->getValue("HSI"), logftpmssg,rpath,
logftpmssg,
logftpmssg);
	}
	else {
	  (*qout) << "ERROR: unknown HRM access type" << edl;
	  return mss_other_error;
	  if (mylogin!=NULL) delete[] mylogin;
	  if (mypasswd!=NULL) delete[] mypasswd;
	}

	if (debugLevel > 7) {
		(*qout) << "HSI script: " << edl;
		(*qout) << ftptest << edl;
	}

	FILE *pftpPtr;
	if ((pftpPtr=popen(ftptest, "r")) != NULL) {
	    if (debugLevel > 2) (*qout) << "MSSLS: errno=" << (int) errno << edl;
	    pclose(pftpPtr);	
	} else {
	    qout->alog(1, "MSSLS", "POPEN_FAILED", rpath, "");
	    sleep(90);
	    if ((pftpPtr=popen(ftptest, "r")) == NULL) {
	      if (debugLevel > 2) {
		(*qout) << "MSSLS: errno=" << (int) errno << edl;
		(*qout) << "ERROR: POPEN failed" << edl;
	      }
	      qout->alog(1, "MSSLS", "POPEN_FAILED", rpath, "");
	      if (mylogin!=NULL) delete[] mylogin;
	      if (mypasswd!=NULL) delete[] mypasswd;
	      return mss_other_error;
	    }
	    else {
	      if (debugLevel > 2) (*qout) << "MSSLS: errno=" << (int) errno << edl;
	      pclose(pftpPtr);
	    }
	}
	
	result = getHSIError(logftpmssg);
	if (( result == mss_no_such_file ) || (result == mss_no_such_directory)) {
	    if (debugLevel > 2) (*qout) << "File/Directory does not exist in HPSS."
				     << edl << "\tHPSS Path: " << rpath << edl;
	}
	else if ( result == mss_no_access_permission ) {
	    if (debugLevel > 2) (*qout) << "File has no read permission in HPSS." 
				     << edl << "\tHPSS Path: " << rpath << edl;
	} else if ( result == mss_limit_reached ) { 	
		if (debugLevel > 2) (*qout) << "Too many pftps. Try later." << edl;
	} else if ( result == mss_gss_failure) {
		if (debugLevel > 2) (*qout) << "  WARNING: GSI proxy has a problem." << edl;
	} else if ( result == mss_wrong_login ) {
		if (debugLevel > 2) (*qout) << "  WARNING: Either userid or password are WRONG." << edl;
	} else if ( result == mss_system_not_available ) {
		if (debugLevel > 2) (*qout) << "HPSS system not available. Try later." << edl;
	} else if ( result == mss_command_successful ) {
		result = mss_transfer_done;
		if (debugLevel > 2) (*qout) << "MSS Listing done (command_success):" << rpath << edl;
	} else if ( result == mss_transfer_done ) {
		// parsing the result of ls
		if (debugLevel > 2) (*qout) << "MSS Listing done:" << rpath << edl;
	} else {
		if (debugLevel > 2) (*qout) << "Other errors: " << result << edl;
	}

    if (mylogin!=NULL) delete[] mylogin;
    if (mypasswd!=NULL) delete[] mypasswd;

    return result; // ok if result = mss_transfer_done.
}

//char* FileFetcherHSI::makeDirectoryRecursive(const HRM::PATH_SET_T *dirs, char* temp) {
char* FileFetcherHSI::makeDirectoryRecursive(const MSS_PATH_SET_T *dirs, char* temp) {
    HRM_URL_T* source_url = new HRM_URL_T;
    int urlresult=0;
    urlresult=hrm_url_parse(dirs->dir.c_str(), source_url);
    char mydirpath[1024];
    strcpy(mydirpath, source_url->url_path);

	strcat(temp, "mkdir ");
//	strcat(temp, dirs->dir);
	strcat(temp, mydirpath);
	strcat(temp, "; ");
	hrm_url_destroy(source_url);

	for (int j=0; j<(dirs->sub_dir).size(); j++) {
	     makeDirectoryRecursive(&((dirs->sub_dir)[j]), temp);
	}
	return temp;
}
/*
? mkdir mytest
mkdir: /nersc/gc5/asim/mytest
? mkdir /nersc/gc5/asim/mytest
mkdir: /nersc/gc5/asim/mytest
? mkdir /nersc/gc5/asim/mytest
mkdir: /nersc/gc5/asim/mytest
? rmdir /nersc/gc5/asim/mytest
rmdir: /nersc/gc5/asim/mytest
? rmdir /nersc/gc5/asim/mytest
*** Warning: `/nersc/gc5/asim/mytest' is not a directory - skipped
? mkdir /nersc/gc5/junmin/smallfile
*** hpss_Mkdir: Access denied  [-13: HPSS_EACCES] 
    /nersc/gc5/junmin/smallfile
? rmdir /nersc/gc5/junmin/tiny
*** hpss_Rmdir: Access denied  [-13: HPSS_EACCES] 
    /nersc/gc5/junmin/tiny
*/

//HRM::STATUS_CODE_T* FileFetcherHSI::makeDirectory(const char* uid, 
//	const char* requestID, const HRM::PATH_SET_T *dirs, const HRM::ACCESS_INFO_T *target) {
MSS_STATUS_CODE_T* FileFetcherHSI::makeDirectory(const MSS_PATH_SET_T *dirs, const MSS_ACCESS_INFO_T *target) {

    (*qout) << "HERE: " << dirs->dir.size() << edl;
    (*qout) << dirs->dir << edl;
    (*qout) << "FILES: " << (dirs->files).size() << edl;
    (*qout) << "SUB DIR" << (dirs->sub_dir).size() << edl;
    for (int j=0; j<(dirs->sub_dir).size(); j++) {
        (*qout) << "ROUND " << j << edl;
    }

    char* sid = getStringID(); // used for requestID as well as uid
//	HRM::STATUS_CODE_T* temp = new HRM::STATUS_CODE_T;
	MSS_STATUS_CODE_T* temp = new MSS_STATUS_CODE_T;
    char    *ftp = new char [8192000];
    char    ftptransferlog[1024];
    strcpy(ftptransferlog, _config->getValue("MSSLogFile"));
    strcat(ftptransferlog, ".mkdirlog.");
    strcat(ftptransferlog, sid);

    mss_message_t pftpmssg = mss_command_successful;
    long counter = 0;

    char* mylogin = NULL;
    char* mypasswd = NULL;
// AS2015
//    if (!target->login.empty()) TSRMCrypto::decrypt(target->login.c_str(), &mylogin);
//    if (!target->passwd.empty()) TSRMCrypto::decrypt(target->passwd.c_str(), &mypasswd);

    (*qout) << "TYPE: " << target->type << edl;
//    if ((target->type == HRM::NONE) || (target->type == HRM::KERBEROS)) {
    if ((target->type == MSS_NONE) || (target->type == MSS_IMPLICIT)) {
      sprintf(ftp, "#! /bin/sh\n \
%s -q \"out %s; "
	      ,_config->getValue("HSI"), ftptransferlog);
    }
	else {
	  (*qout) << "MSSM: Invalid HPSS access type" << edl;
// AS2015
//	    cm->logEvent("MSSM",requestID,"mkdir","STATUS=REQUEST_FAILED");
        if (mylogin!=NULL) delete[] mylogin;
        if (mypasswd!=NULL) delete[] mypasswd;
//	    temp->code = HRM::REQUEST_FAILED;
	    temp->code = MSS_REQUEST_FAILED;
	    temp->explanation.assign("invalid HPSS access type");
		delete[] ftp;
	    return temp;
	}

#ifdef NEVER // junmin
	HRM_URL_T* source_url = new HRM_URL_T;
	int urlresult=0;
	urlresult=hrm_url_parse(dirs->dir.c_str(), source_url);
	char mydirpath[1024];
	strcpy(mydirpath, source_url->url_path);
	hrm_url_destroy(source_url);
#else
	char mydirpath[1024];
	strcpy(mydirpath, dirs->dir.c_str());
#endif
	strcat(ftp, "mkdir ");
//	strcat(ftp, (char*) dirs->dir);
	strcat(ftp, mydirpath);
	strcat(ftp, " ; ");


	for (int j=0; j<(dirs->sub_dir).size(); j++) {
	    makeDirectoryRecursive(&((dirs->sub_dir)[j]), ftp);
	}
	strcat(ftp, "end\"\necho \"HSI_ERROR_CODES=\"$? >> ");
	strcat(ftp, ftptransferlog);
	strcat(ftp, "\necho \"Date: `date`\" >>");
	strcat(ftp, ftptransferlog);

	if (debugLevel > 10) {
	    (*qout) << "HSI script" << edl;
	    (*qout) << ftp << edl;
	}

	qout->alog(10, "MSSM",sid,"mkdir","STATUS=MKDIR");
	FILE *pftpPtr;
	if ((pftpPtr=popen(ftp, "r")) != NULL) {
	     if (debugLevel > 2) (*qout) << "MSSM: errno=" << (int) errno << edl;
	     pclose(pftpPtr);
	}
	else {
	     qout->alog(1, "MSSM", "POPEN_FAILED", "", "");
	     sleep(90);
	     if ((pftpPtr=popen(ftp, "r")) == NULL) {
	       if (debugLevel > 2) {
		   (*qout) << "MSSM: errno=" << (int) errno << edl;
		   (*qout) << "ERROR: POPEN failed" << edl;
	       }
	        qout->alog(1, "MSSM", "POPEN_FAILED", "", "");
	        if (mylogin!=NULL) delete[] mylogin;
	        if (mypasswd!=NULL) delete[] mypasswd;
//			temp->code = HRM::REQUEST_FAILED;
			temp->code = MSS_REQUEST_FAILED;
			temp->explanation.assign("OS SYSTEM error: file descriptor cannot be opened");
			delete[] ftp;
			return temp;
		}
		else {
   			if (debugLevel > 2) (*qout) << "MSSM: errno=" << (int) errno << edl;
			pclose(pftpPtr);
		}
	}

	pftpmssg = getHSIError(ftptransferlog);
	if ((pftpmssg == mss_limit_reached) || (pftpmssg == mss_system_not_available)) {
	  (*qout) << "some directory creation failed due to the HPSS down" << edl;
	  do {
	    if (!_config->is_true("EnableDetailedLog")) unlink(ftptransferlog);
	    if (pftpmssg == mss_system_not_available) {
// AS2015
//					cm->setLastTransferRate(-1.0);
	      sleep(LONG_WAIT_TIME);
	    }
	    else {
// AS2015
//					if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
	      sleep(WAIT_TIME);
	    }
	    if ( (pftpmssg == mss_system_not_available) && 
		 (counter > (max_num_trials-3) ) ) {
// AS2015
//					cm->setLastTransferRate(-1.0);
	      sleep(LONG_WAIT_TIME);
	    }
	    char ltemp[1024];
	    sprintf(ltemp, "%s.%d", ftptransferlog, counter);
	    rename(ftptransferlog, ltemp);

	    if ((pftpPtr=popen(ftp, "r")) != NULL) {
	      if (debugLevel > 2) (*qout) << "MSSM: errno=" << (int) errno << edl;
	      pclose(pftpPtr);
	      pftpmssg = getHSIError(ftptransferlog);
	      if ( (counter >= max_num_trials) && 
		   (pftpmssg == mss_limit_reached ) &&
		   (pftpmssg == mss_system_not_available)  ) {
// AS2015
//			        	cm->logEvent("MSSM",requestID,"mkdir","ERROR=UNABLE_TO_CREATE_DIR");
	      }
	    }
	    if (debugLevel > 2) (*qout) << "MSSM: errno=" << (int) errno << edl;
	    counter++;
	  } while( (pftpmssg == mss_limit_reached) &&
		   (pftpmssg == mss_system_not_available) &&
		   (counter < max_num_trials) );
	}

//	if (!_config->is_true("EnableDetailedLog")) unlink(ftptransferlog);
//	HRM::STATUS_CODE_T* temp = new HRM::STATUS_CODE_T;
	if ( pftpmssg == mss_command_successful || pftpmssg == mss_file_exists )  {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
		if (!_config->is_true("EnableDetailedLog")) {
			unlink(ftptransferlog);
		}
// AS2015
//		cm->logEvent("MSSM",requestID,"mkdir","STATUS=REQUEST_DONE");
//		temp->code = HRM::REQUEST_DONE;
		temp->code = MSS_REQUEST_DONE;
		temp->explanation.assign("");
	}
	else if ( pftpmssg == mss_no_access_permission )  {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//		cm->logEvent("MSSM",requestID,"mkdir","STATUS=REQUEST_FAILED");
//		temp->code = HRM::USER_NOT_AUTHORIZED;
		temp->code = MSS_USER_NOT_AUTHORIZED;
		temp->explanation.assign("mss_no_access_permission");
	}
	else if ( pftpmssg == mss_no_such_directory )  {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//		cm->logEvent("MSSM",requestID,"mkdir","STATUS=REQUEST_FAILED");
//		temp->code = HRM::FILE_DOES_NOT_EXIST;
		temp->code = MSS_FILE_DOES_NOT_EXIST;
		temp->explanation.assign("mss_no_such_directory");
	}
	else if ( pftpmssg != mss_limit_reached && pftpmssg != mss_system_not_available )  {
// AS2015
//		cm->setLastTransferRate(-1.0);
//		cm->logEvent("MSSM",requestID,"mkdir","ERROR=UNABLE_TO_CREATE_DIR");
//		cm->logEvent("MSSM",requestID,"mkdir","ERROR=SYSTEM_NOT_AVAILABLE");
//		temp->code = HRM::REQUEST_FAILED;
		temp->code = MSS_REQUEST_FAILED;
		temp->explanation.assign("try again later");
	}
	else {
		if (pftpmssg == mss_limit_reached) {
// AS2015
//			if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//			cm->logEvent("MSSM",requestID,"mkdir","ERROR=PFTP_LIMIT_REACHED");
//			temp->code = HRM::REQUEST_FAILED;
		  temp->code = MSS_REQUEST_FAILED;
		  temp->explanation.assign("MSS access limit reached");
		}
		else if (pftpmssg == mss_error) {
// AS2015
//			cm->setLastTransferRate(-1.0);
//			cm->logEvent("MSSM",requestID,"mkdir","ERROR=HPSS_ERROR");
//			temp->code = HRM::FATAL_MSS_ERROR;
			temp->code = MSS_FATAL_MSS_ERROR;
			temp->explanation.assign("HPSS errore. Try later?");
		}
		else {
// AS2015
//			if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//			temp->code = HRM::REQUEST_FAILED;
			temp->code = MSS_REQUEST_FAILED;
			temp->explanation.assign("Try again later");
		}
	}

    if (mylogin!=NULL) delete[] mylogin;
    if (mypasswd!=NULL) delete[] mypasswd;

	delete[] ftp;
	return temp;
}


//HRM::STATUS_CODE_T* FileFetcherHSI::removePath(const char* uid, 
//	const char* requestID, const char* path, HRM::ACCESS_INFO_T target,
//	bool isdirectory) {
MSS_STATUS_CODE_T* FileFetcherHSI::removePath(const char* path, MSS_ACCESS_INFO_T target,
	bool isdirectory) {

    char* sid = getStringID(); // used for requestID as well as uid
//	HRM::STATUS_CODE_T* temp = new HRM::STATUS_CODE_T;
	MSS_STATUS_CODE_T* temp = new MSS_STATUS_CODE_T;
    char    ftp[8192];
    char    ftptransferlog[1024];
	strcpy(ftptransferlog, _config->getValue("MSSLogFile"));
	strcat(ftptransferlog, ".removelog.");
    strcat(ftptransferlog, sid);

	mss_message_t pftpmssg = mss_command_successful;
    long counter = 0;

    char* mylogin = NULL;
    char* mypasswd = NULL;
// AS2015
//    if (!target.login.empty()) TSRMCrypto::decrypt(target.login.c_str(), &mylogin);
//    if (!target.passwd.empty()) TSRMCrypto::decrypt(target.passwd.c_str(), &mypasswd);

//	if ((target.type == HRM::NONE) || (target.type == HRM::KERBEROS)) {
	if ((target.type == MSS_NONE) || (target.type == MSS_KERBEROS)) {
sprintf(ftp, "#! /bin/sh\n \
%s -q \"out %s; "
,_config->getValue("HSI"), ftptransferlog);
	}
	else {
		(*qout) << "MSSM: Invalid HPSS access type" << edl;
// AS2015
//		    cm->logEvent("MSSR",requestID,"remove","STATUS=REQUEST_FAILED");
//		    temp->code = HRM::REQUEST_FAILED;
		    temp->code = MSS_REQUEST_FAILED;
		    temp->explanation.assign("invalid HPSS access type");
	        if (mylogin!=NULL) delete[] mylogin;
	        if (mypasswd!=NULL) delete[] mypasswd;
		    return temp;
	}

// it only works with file
	strcat(ftp, "rm ");
	strcat(ftp, path);

	strcat(ftp, "; end\" \necho \"HSI_ERROR_CODES=\"$? >> ");
	strcat(ftp, ftptransferlog);
	strcat(ftp, "\necho \"Date: `date`\" >> ");
	strcat(ftp, ftptransferlog);

	if (debugLevel > 10) {
		(*qout) << "HSI script" << edl;
		(*qout) << ftp << edl;
	}

	qout->alog(10, "MSSR",sid,"remove","STATUS=REMOVE");
	FILE *pftpPtr;
	if ((pftpPtr=popen(ftp, "r")) != NULL) {
   		if (debugLevel > 2) (*qout) << "MSSR: errno=" << (int) errno << edl;
		pclose(pftpPtr);
	}
	else {
		qout->alog(1, "MSSR", "POPEN_FAILED", "", "");
		sleep(90);
		if ((pftpPtr=popen(ftp, "r")) == NULL) {
			if (debugLevel > 2) {
				(*qout) << "MSSR: errno=" << (int) errno << edl;
				(*qout) << "ERROR: POPEN failed" << edl;
			}
			qout->alog(1, "MSSR", "POPEN_FAILED", "", "");
//			temp->code = HRM::REQUEST_FAILED;
			temp->code = MSS_REQUEST_FAILED;
			temp->explanation.assign("OS SYSTEM error: file descriptor cannot be opened");
            if (mylogin!=NULL) delete[] mylogin;
            if (mypasswd!=NULL) delete[] mypasswd;

			return temp;
		}
		else {
   			if (debugLevel > 2) (*qout) << "MSSR: errno=" << (int) errno << edl;
			pclose(pftpPtr);
		}
	}

    pftpmssg = getHSIError(ftptransferlog);
	if ((pftpmssg == mss_limit_reached) || (pftpmssg == mss_system_not_available)) {
			(*qout) << "some directory creation failed due to the HPSS down" << edl;
		    do {
				if (!_config->is_true("EnableDetailedLog")) unlink(ftptransferlog);
				if (pftpmssg == mss_system_not_available) {
// AS2015
//					cm->setLastTransferRate(-1.0);
					sleep(LONG_WAIT_TIME);
				}
				else {
// AS2015
//					if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
					sleep(WAIT_TIME);
				}
				if ( (pftpmssg == mss_system_not_available) && 
					(counter > (max_num_trials-3) ) ) {
// AS2015
//					cm->setLastTransferRate(-1.0);
					sleep(LONG_WAIT_TIME);
				}
				char ltemp[1024];
				sprintf(ltemp, "%s.%d", ftptransferlog, counter);
				rename(ftptransferlog, ltemp);
				if ((pftpPtr=popen(ftp, "r")) != NULL) {
   					if (debugLevel > 2) (*qout) << "MSSR: errno=" << (int) errno << edl;
					pclose(pftpPtr);
					pftpmssg = getHSIError(ftptransferlog);
// AS2015
//			        if ( (counter >= max_num_trials) && 
//						(pftpmssg == mss_limit_reached ) &&
//						(pftpmssg == mss_system_not_available)  ) {
//			        	cm->logEvent("MSSR",requestID,"mkdir","ERROR=UNABLE_TO_REMOVE_PATH");
//					}
				}
    			if (debugLevel > 2) (*qout) << "MSSR: errno=" << (int) errno << edl;
				counter++;
		    } while( (pftpmssg == mss_limit_reached) &&
				 (pftpmssg == mss_system_not_available) &&
				 (counter < max_num_trials) );
	}

	if ( pftpmssg == mss_command_successful ) { 
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
		if (!_config->is_true("EnableDetailedLog")) {
			unlink(ftptransferlog);
		}
// AS2015
//		cm->logEvent("MSSR",requestID,"remove","STATUS=REQUEST_DONE");
//		temp->code = HRM::REQUEST_DONE;
		temp->code = MSS_REQUEST_DONE;
		temp->explanation.assign("");
	}
	else if ( pftpmssg == mss_file_exists )  {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//		cm->logEvent("MSSR",requestID,"remove","STATUS=REQUEST_FAILED");
//		temp->code = HRM::REQUEST_FAILED;
		temp->code = MSS_REQUEST_FAILED;
		temp->explanation.assign("file exists in the path");
	}
	else if ( pftpmssg == mss_no_access_permission )  {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//		cm->logEvent("MSSR",requestID,"mkdir","STATUS=REQUEST_FAILED");
//		temp->code = HRM::USER_NOT_AUTHORIZED;
		temp->code = MSS_USER_NOT_AUTHORIZED;
		temp->explanation.assign("mss_no_access_permission");
	}
	else if ( pftpmssg == mss_no_such_directory )  {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//		cm->logEvent("MSSR",requestID,"mkdir","STATUS=REQUEST_FAILED");
//		temp->code = HRM::FILE_DOES_NOT_EXIST;
		temp->code = MSS_FILE_DOES_NOT_EXIST;
		temp->explanation.assign("mss_no_such_directory");
	}
	else if ( pftpmssg != mss_limit_reached && pftpmssg != mss_system_not_available )  {
// AS2015
//		cm->setLastTransferRate(-1.0);
//		cm->logEvent("MSSR",requestID,"remove","ERROR=UNABLE_TO_CREATE_DIR");
//		cm->logEvent("MSSR",requestID,"remove","ERROR=SYSTEM_NOT_AVAILABLE");
//		temp->code = HRM::REQUEST_FAILED;
		temp->code = MSS_REQUEST_FAILED;
		temp->explanation.assign("unavailable system and try again later");
	}
	else {
		if (pftpmssg == mss_limit_reached) {
// AS2015
//			if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//			cm->logEvent("MSSR",requestID,"remove","ERROR=PFTP_LIMIT_REACHED");
//			temp->code = HRM::REQUEST_FAILED;
			temp->code = MSS_REQUEST_FAILED;
			temp->explanation.assign("MSS access limit reached");
		}
		else if (pftpmssg == mss_error) {
// AS2015
//			cm->setLastTransferRate(-1.0);
//			cm->logEvent("MSSR",requestID,"remove","ERROR=HPSS_ERROR");
//			temp->code = HRM::FATAL_MSS_ERROR;
			temp->code = MSS_FATAL_MSS_ERROR;
			temp->explanation.assign("HPSS error. Try later?");
		}
		else {
// AS2015
//			if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//			cm->logEvent("MSSR",requestID,"remove","ERROR=OTHER_ERROR");
//			temp->code = HRM::REQUEST_FAILED;
			temp->code = MSS_REQUEST_FAILED;
			temp->explanation.assign("Try again later");
		}
	}

    if (mylogin!=NULL) delete[] mylogin;
    if (mypasswd!=NULL) delete[] mypasswd;

	return temp;
}

//
// checkMSS() is used at launch time to check whether logging to HPSS is
// possible, and if not it will output a warning with a possible reason.
//
// AS: 03032003
// cannot check HPSS with HSI and PFTP at the initial time 
// because default login and passwd is not assumed anymore
mss_message_t FileFetcherHSI::checkMSS() {
        (*qout) << "HSI connection is being checked..." << edl;
        (*qout) << "    Make sure you have a proper credentials to access HPSS. .." << edl;
        (*qout) << "    e.g. at NERSC, DCE credential is needed with hsi -l" << edl;
        (*qout) << "         at ORNL, Kerberos credential is needed with /krb5/bin/kinit DCE_principle" << edl;
		mss_message_t myhsi_conn = mss_connection_ok;
        try {
			myhsi_conn = checkHSI();
                if ( myhsi_conn != mss_connection_ok ) {
                    (*qout) << "CAN'T LOGIN TO HSI ... HSI login is wrong..." << edl;
                    (*qout) << "Check the HSI manually, and try again" << edl;
                } else {
                    (*qout) << "\tHSI login/connection Ok." << edl;
// AS2015
//                    cm->logEvent("TRM_STATUS",0,0,"STATUS=HSI_CONNECTION_OKAY");
                }
        } catch(char const* s) {
                (*qout) << s << edl;
                REASON;
        }

    return myhsi_conn; // ok if result = mss_connection_ok.
}

//
// getPFTPError(), to be defined below, parses the output of pftp transfer
// to find possible reasons for the transfer failure. Examples of possible
// failures that this function can handle are show now. These are real
// examples.
/*
*/

// AS 030603: cannot be used anymore because there is no default HPSS user.
// If used, it'll use the DCE auto login
mss_message_t FileFetcherHSI::checkHSI() {
  mss_message_t result = mss_connection_ok;
  try {
        // having file size and tape ID through HSI...
    char *hpsshsihostname = 0;
    char *hsiportnumber=0;
    char space[10] = " ";
    char temphostname[1024];
    if (_config->getValue("HPSSHSIHostName")) {
      strcpy(temphostname, _config->getValue("HPSSHSIHostName"));    
    } else {
      strcpy(temphostname, _config->getValue("MSSHostName"));
    }
    char* hk = NULL;
    hpsshsihostname = strtok_r((char*)temphostname, ":", &hk);
    hsiportnumber = strtok_r(hk, " ", &hk);
    if ( hsiportnumber == 0 ) {
      // hsiportnumber = space; // no -p if no port - junmin 02/23/15
    }
    
    char cmd[1024];
    char outfile[512];
    strcpy(outfile, _config->getValue("MSSLogFile"));
    strcat(outfile, ".hsi.init");
    
    strcpy(cmd, (*_config)["HSI"]);
    qout->alog(0, "HSI", 0,"HOST", hpsshsihostname);
    if (_config->getValue("HPSSHSIHostName")) {
      strcat(cmd, " -q -h ");
      strcat(cmd, hpsshsihostname);
      
      if (hsiportnumber != 0) { // -p without valid port stalls
	strcat(cmd, " -p ");
	strcat(cmd, hsiportnumber);
      }
      qout->alog(0, "HSI", 0,"PORT", hsiportnumber);    
    } else {
      strcat(cmd, " -q ");
      if (hpsshsihostname != NULL) strcat(cmd, hpsshsihostname);
    }
    strcat(cmd, " \"out ");
    strcat(cmd, outfile);
    strcat(cmd, "; pwd");
    strcat(cmd, "\"");
    
    char line[1024];
    FILE *ptr;
    if ((ptr = popen(cmd, "r")) != NULL) {
      if (debugLevel > 5) (*qout) << "HSI execution: " << cmd << edl;
      pclose(ptr);
    }
    bool resultCheck=false;
    if ((ptr=fopen(outfile, "r")) != NULL) {
      while (fgets(line, 1024, ptr) != NULL) {
	if( !line[0] || line[0]=='\n' || line[0]=='\r' ||
	    line[0]=='!' || line[0]=='#' ) {
	  continue;
	}
	line[strlen(line)-1] = 0;
	if (strlen(line) > 0) {
	  resultCheck = true;
	  break;
	}
      }
      fclose(ptr);
    }
    if (!_config->is_true("EnableDetailedLog")) unlink(outfile);
    if (!resultCheck) result = mss_other_error;
  }
  catch (...) {
    result = mss_other_error;
  }
  return result;
}

//
// getPFTPError() is used if a file transfer failed. It looks at the pftp
// output to try to find the reason for failure.
//
mss_message_t FileFetcherHSI::getMSSError(const char* ftptransferlog) {
	const int SIZE = 1024;	// some buffer size
	char buffer[SIZE];
	mss_message_t okdone = mss_other_error;

	std::ifstream inReadPFTPError(ftptransferlog, std::ios::in);

    if (!inReadPFTPError) {
        (*qout) << "File " << ftptransferlog << " could not be opened" << edl;
		return mss_other_error;
    }

	while ( inReadPFTPError.getline(buffer, SIZE) ) {
        if (( 0 != strstr(buffer,"No such file or directory") ) || 
			(0 != strstr(buffer,"-2: HPSS_ENOENT") )) {
            if (debugLevel > 1) (*qout) << "No such file in HPSS." << edl;
			inReadPFTPError.close();
            return mss_system_not_available;
        }
        if ( ( 0 != strstr(buffer,"Access denied") ) || 
			(0 != strstr(buffer,"-13: HPSS_EACCES")) ) {
            if (debugLevel > 1) (*qout) << "no permission: " << edl;
			inReadPFTPError.close();
			return mss_no_access_permission; 
									// cannot read file for GET
									// cannot write file for PUT
        }
        if ( ( 0 == strncmp(buffer,"530 Could not load thread state (pass)",38) ) ||
			(0 == strncmp(buffer,"Please login with USER and PASS",31) ) ) {
            (*qout) << "HPSS service not available." << edl;
			inReadPFTPError.close();
            return mss_system_not_available;
		}
        if ( 0 == strncmp(buffer,"421 Service not available",25) ) {
            (*qout) << "HPSS service not available." << edl;
			inReadPFTPError.close();
            return mss_system_not_available;
        }
        if ( 0 == strncmp(buffer,"Not connected",13) ) {
            if (debugLevel > 1) (*qout) << "HPSS DOWN." << edl;
			inReadPFTPError.close();
            return mss_system_not_available;  
        }
        if ((0 != strstr(buffer,"Cannot get Shared Memory")) ||
			(0 != strstr(buffer,"Please Notify the System")) ) {
            if (debugLevel > 1) (*qout) << "HPSS DOWN: Cannot get Shared Memory" << edl;
			inReadPFTPError.close();
            return mss_system_not_available;  
		}
		// limit of pftps reached at BNL
        if (0 != strstr(buffer,"Access denied due to active ftp login limit")) {
            (*qout) << "Limit of pftps reached." << edl;
			char syscommand[2048];
			strcpy(syscommand, "grep 530 ");
			strcat(syscommand, ftptransferlog);
            system(syscommand);	// this outputs the 530 message
			inReadPFTPError.close();
            return mss_limit_reached;
        }
		// limit of pftps reached at LBNL
        if ( 0 == strncmp(buffer,"421 Service not available - maximum number of sessions exceeded",63) ) {
            (*qout) << "Limit of pftps reached." << edl;
			inReadPFTPError.close();
            return mss_limit_reached;
        }

        if ( 0 == strncmp(buffer,"GSSAPI error: No local mapping for Globus ID",44) ) {
            (*qout) << "GSSAPI error: No Local mapping for DN. Check the GSS proxy." << edl;
			inReadPFTPError.close();
            return mss_gss_failure;
		}
        if ( ( 0 == strncmp(buffer,"GSSAPI authentication failed",28) ) ||
        	( 0 == strncmp(buffer,"GSSAPI error",12) ) ) {
            (*qout) << "GSSAPI auth failed. Check the GSS proxy." << edl;
			inReadPFTPError.close();
            return mss_gss_failure;
		}
        if ( 0 == strncmp(buffer,"ftp: SIZE command failed",24) ) {
            (*qout) << "HPSS coule not find the file." << edl;
			inReadPFTPError.close();
            return mss_no_such_file;
        }
        if ( 0 != strstr(buffer,"529 Bad Data Transfer")) {
            (*qout) << "HPSS ports are closed by firewall possibly." << edl;
            (*qout) << "Or disk size is limited." << edl;
            inReadPFTPError.close();
            return mss_error;
        }
        if ( 0 != strstr(buffer,"501 Bad input parameters")) {
            (*qout) << "HPSS has a problem and needs a check." << edl;
            inReadPFTPError.close();
            return mss_error;
        }
        if ( 0 != strstr(buffer,"not a plain file")) {
            (*qout) << "local file system has a problem and needs a check." << edl;
            inReadPFTPError.close();
            return mss_other_error;
        }
        if ((0 != strstr(buffer,"Connection refused")) ||
			(0 != strstr(buffer,"425 Can't build data connection")) ) {
            if (debugLevel > 1) (*qout) << "HPSS DOWN: 425 error" << edl;
			inReadPFTPError.close();
            return mss_system_not_available;  
		}

// ORNL:  530 Login incorrect. - Reenter username and password.
        if ( ( 0 == strncmp(buffer,"Login incorrect",15) ) ||
			(0 == strncmp(buffer,"530 Login incorrect",19) ) ||
			(0 == strncmp(buffer,"Login failed",12) ) ) {
            (*qout) << "Login incorrect. Check username and password." << edl;
			system("banner login incorrect");
			inReadPFTPError.close();
            return mss_wrong_login;
        }
// account does not exist
// 530 User /.../dce.ccs.ornl.gov/srm (unknown) access denied. 
        if ( (0 != strstr(buffer,"access denied")) && 
			(0 != strncmp(buffer,"hpss_Opendir failed Code: -13", 29)) &&
			(0 != strncmp(buffer,"530 User", 8)) ) {
            (*qout) << "Login incorrect. Check username and password." << edl;
			inReadPFTPError.close();
            return mss_wrong_login;
		}
		if ( ( 0 == strncmp(buffer,"226 Transfer complete",21) ) 
          || ( 0 == strncmp(buffer,"226 Transfer Complete",21) ) ) {
            (*qout) << "Looks like everything is okay." << edl;
            okdone=mss_transfer_done;
			inReadPFTPError.close();
			return mss_transfer_done;
        }
		if ( 0 == strncmp(buffer,"257 MKD command successful",26) ) {
            (*qout) << "Looks like mkdir is okay." << edl;
            okdone=mss_command_successful;
        }
		if ( 0 == strncmp(buffer,"550",3) ) {
            (*qout) << "Looks like mkdir or rmdir failed due to target path." << edl;
	        if (0 != strstr(buffer,"File exists")) {
				inReadPFTPError.close();
            	okdone=mss_file_exists;
	            (*qout) << "Looks like mkdir/rmdir failed for existing directory or file." << edl;
				return mss_file_exists;
			}
	        else if (0 != strstr(buffer,"Permission denied")) {
            	okdone=mss_no_access_permission;
	            (*qout) << "Looks like mkdir/rmdir failed for access permission." << edl;
				inReadPFTPError.close();
				return mss_no_access_permission;
			}
	        else if (0 != strstr(buffer,"No such file or directory")) {
				inReadPFTPError.close();
	            (*qout) << "Looks like ls/mkdir/rmdir failed for non existing file or directory." << edl;
				return mss_no_such_directory;
			}
	        else if (0 != strstr(buffer,"Arguments too long")) {
				inReadPFTPError.close();
	            (*qout) << "results are too much that pftp cannot handle." << edl;
				return mss_other_error;
			}
        }
		if ( 0 == strncmp(buffer,"250 DELE command successful",27) ) {
            (*qout) << "Looks like delete is okay." << edl;
            okdone=mss_command_successful;
        }
		if ( 0 == strncmp(buffer,"250 RMD command successful",26) ) {
            (*qout) << "Looks like rmdir is okay." << edl;
            okdone=mss_command_successful;
        }
	}

	inReadPFTPError.close();
	(*qout) << "File Transfer OKAY COMMAND_SUCCESSFUL: " << ftptransferlog << edl;
	okdone=mss_command_successful;
   // okdone=mss_transfer_done;
	return okdone; // if nothing above worked. very unlikely.
}

// not in use!
// handleHPSSError() tries to handle the HPSS error. There is an HPSS error
// if getPFTPError() returns a positive number. The system can handle errors
// number 5 and 16. It just retries the transfer after a pause (WAIT_TIME).
//
int FileFetcherHSI::handleMSSError(int MSSerror) {

    if ( MSSerror == 16) {
        if (debugLevel) {
			(*qout) << "HPSS Error: -16 -> EBUSY: Mount device busy" << edl;
        	(*qout) << "HRM will try to handle this error." << edl;
		}
        return WAIT_TIME;
    } else if ( MSSerror == 5 ) {
        if (debugLevel) {
	        (*qout) << "HPSS Error: -5 -> EIO: I/O error" << edl;
	        (*qout) << "HRM will try to handle this error." << edl;
		}
        return WAIT_TIME;
    } else {
        if (debugLevel) {
	        (*qout) << "HPSS Error: -" << MSSerror << edl;
	        (*qout) << "HRM doesn't know how to handle this error." << edl;
		}
        return 0;
    }
}

//
// getPFTPTransferTime() is used if the file transfer was successful. It gets
// the transfer time from the pftp output.
//
// this is not really useful for HSI becuase output does not contain time/size

double FileFetcherHSI::getMSSTransferTime(const char* ftptransferlog) {
	const int SIZE = 1024;	// some buffer size
	char buffer[SIZE];
	double t = 0;

	std::ifstream inReadPFTPTime(ftptransferlog, std::ios::in);

    if (!inReadPFTPTime) {
        (*qout) << "File " << ftptransferlog << " could not be opened" << edl;
		return t;
    }

	while ( inReadPFTPTime.getline(buffer, SIZE) ) {
		if ( (0 != strstr(buffer,"bytes received"))
			|| (0 != strstr(buffer,"bytes sent")) ) {
			int i = 0;
			char *tokenPtr;
			char cpybuffer[SIZE];
			strcpy(cpybuffer,buffer);
			char *tk=NULL;
			tokenPtr = strtok_r(cpybuffer," ", &tk);
			if ( 0 != strstr(buffer,"minute") ) {
				while (tokenPtr != 0) {
					++i;
					if ( i == 5 ) t += 60.00*atoi(tokenPtr);
					if ( i == 6 ) t += atof(tokenPtr+8);
					tokenPtr = strtok_r(tk," ", &tk);
				}
			}
			else {
				while (tokenPtr != 0) {
					++i;
					if ( i == 5 ) t += atof(tokenPtr);
					tokenPtr = strtok_r(tk," ", &tk);
				}
			}
			inReadPFTPTime.close();
			return t;
		}
	}
	inReadPFTPTime.close();
	return t;
}

bool FileFetcherHSI::checkLocalTarget(char* localfilename, char* sid, long long filesize) 
{
    std::ofstream createLocalFile(localfilename, std::ios::out);
    
    if (!createLocalFile) {
        (*qout) << "Unable to create file " << localfilename << "." << edl;
	(*qout) << "\tMaybe you have no permission to write in that directory..." <<edl;
	(*qout) << "\tOr maybe target directory doesn't exist." << edl;
	(*qout) << "\tTry to create target directory." << edl;
	char* p = strrchr(localfilename,'/');	// p will be always != 0...
	int ldir = strlen(localfilename)-strlen(p);
	char dir[1024];
	strncpy(dir,localfilename,ldir+1);
	try {
	  MakeDirectory(dir);
	  if (debugLevel > 10) (*qout) << "\tDirectory " << dir << " created." << edl;
	} catch (char const* s) {
	  (*qout) << s << edl;
	  char cus_item[1024];
	  // AS2015 OLDHRM
	  //			removeAndServeNextFile(filePtr);
	  strcpy(cus_item, "REASON=UNABLE_TO_CREATE_DIRECTORY_");
	  strcat(cus_item, localfilename);
	  // AS2015
	  //			cm->logEvent("MSSG",qid,remotefilename,cus_item);
	  // AS2015 CACHE
	  //			cache->updateEmptyCacheSize((double) filesize);
	  //			cache->updateUsedCacheSize((double) (0 - filesize));
	  //			filePtr->setStatus(HRM::REQUEST_FAILED);
	  // AS2015
	  //			filePtr->setStatus(REQUEST_FAILED);
	  //			filePtr->call_back(HRM::REQUEST_FAILED, cus_item);
	  // AS2015
	  //			filePtr->call_back(REQUEST_FAILED, cus_item);
	  qout->alog(1, "MSSGET", sid, localfilename, "STATUS=REQUEST_FAILED");
	  // AS2015
	  //			cm->releaseFile(filePtr);
	  return false;
	}
	createLocalFile.open(localfilename,std::ios::out);
	// directory created but failed to create file anyway.
	if (!createLocalFile) {
	  (*qout) << "Unable to create file " << localfilename <<  "." << edl;
	  // AS2015 OLDHRM
	  //			removeAndServeNextFile(filePtr);
	  char cus_item[1024];
	  strcpy(cus_item, "REASON=UNABLE_TO_CREATE_FILE_");
	  strcat(cus_item, localfilename);
	  // AS2015
	  //			cm->logEvent("MSSG",qid,remotefilename,cus_item);
	  // AS2015 CACHE
	  //			cache->updateEmptyCacheSize((double) filesize);
	  //			cache->updateUsedCacheSize((double) (0 - filesize));
	  //			filePtr->setStatus(HRM::REQUEST_FAILED);
	  // AS2015
	  //			filePtr->setStatus(REQUEST_FAILED);
	  //			filePtr->call_back(HRM::REQUEST_FAILED, cus_item);
	  // AS2015
	  //			filePtr->call_back(REQUEST_FAILED, cus_item);
	  qout->alog(1, "MSSGET", sid, localfilename, "STATUS=REQUEST_FAILED");
	  // AS2015
	  //            cm->releaseFile(filePtr);
	  return false;
	} else createLocalFile.close();
    } else createLocalFile.close();

    // to check the space left on the device before writing into
    // dec 13 2004
    char tempdiskpath[1024];
    strcpy(tempdiskpath, localfilename);
    struct statvfs tempfs;
    double tempfd;
    char* tempp = strrchr(tempdiskpath,'/');   // p will be always != 0...
    int templdir = strlen(tempdiskpath)-strlen(tempp);
    char tempdir[1024];
    strncpy(tempdir,tempdiskpath,templdir);
    tempdir[templdir]=0;
    int tempfsresult = statvfs(tempdir, &tempfs);
    if ( tempfsresult == 0 ) {
       tempfd = (double)(((double)tempfs.f_bavail)*((double)tempfs.f_frsize));
       if (tempfd < (double) filesize) {
	   (*qout) << "Unable to create file " << localfilename <<  "." << edl;
	   (*qout) << "Unable to create file, left size: " << tempfd << edl;
	   (*qout) << "Unable to create file, need size: " << filesize << edl;
	   // AS2015 OLDHRM
	   //            removeAndServeNextFile(filePtr);
	   char cus_item[1024];
	   strcpy(cus_item, "REASON=NOT_ENOUGH__SPACE_ON_DEVICE");
	   strcat(cus_item, localfilename);
	   // AS2015
	   //            cm->logEvent("MSSG",qid,remotefilename,cus_item);
	   // AS2015 CACHE
	   //            cache->updateEmptyCacheSize((double) filesize);
	   //            cache->updateUsedCacheSize((double) (0 - filesize)); 
	   // AS2015
	   //            filePtr->setStatus(HRM::REQUEST_FAILED);
	   //            filePtr->setStatus(REQUEST_FAILED);
	   //            filePtr->call_back(HRM::REQUEST_FAILED, cus_item);
	   // AS2015
	   //            filePtr->call_back(REQUEST_FAILED, cus_item);
	   qout->alog(1, "MSSGET", sid, localfilename, "STATUS=REQUEST_FAILED_NOT_ENOUGH_SPACE_ON_DEVICE");
	   // AS2015
	   //            cm->releaseFile(filePtr);
	   return false;
       }
    } else {
       (*qout) << "MSSG: The DiskPath entry is wrong?  " << localfilename << ":" << tempdir << edl;
    }
    
    // let's make the file writable even before we transfer it. if we
    // kill the HRM during a transfer we are guaranteed the file will be
    // word writable.
    if ( chmod(localfilename, S_IWUSR | S_IRUSR | S_IROTH | S_IWOTH | S_IRGRP | S_IWGRP) != 0 && errno != EPERM ) (*qout) << "Could not make " << localfilename << " writable." << edl;
    if (debugLevel > 10) {
      (*qout) << "QID: " << sid <<edl;
      (*qout) << "LOCAL: " << localfilename << edl;
    }

    return true;
}

void FileFetcherHSI::handleError(mss_message_t pftpmssg, char* sid, char* localfilename, char* remotefilename, char* ftp, char* ftptransferlog, bool nothingTxfed)
{
  if ( pftpmssg == mss_no_such_file ) { // it will give up for good
      if (debugLevel > 1) (*qout) << "File does not exist in HPSS." 
			       << edl << "\tHPSS Path: " << remotefilename << edl
			       << "\tLocal Path: " << localfilename << edl;
      qout->alog(1, "MSSGET", sid, remotefilename, "STATUS=MSS_NO_SUCH_FILE");
  } else if ( pftpmssg == mss_error ) { // will give up for now
      if (debugLevel > 1) (*qout) << "File has error in HPSS." << edl 
			       << "\tHPSS Path: " << remotefilename << edl
			       << "\tLocal Path: " << localfilename << edl;
      qout->alog(1, "MSSGET", sid, remotefilename, "STATUS=MSS_ERROR_ON_FILE");
  } else if ( pftpmssg == mss_no_access_permission ) { // will give up for now
      if (debugLevel > 1) (*qout) << "File has no read permission in HPSS." 
			       << edl << "\tHPSS Path: " << remotefilename << edl
			       << "\tLocal Path: " << localfilename << edl;
      qout->alog(1, "MSSGET", sid, remotefilename, "STATUS=MSS_NO_READ_PERMISSION");
  } else if ( pftpmssg == mss_limit_reached ) { // will give up for now
      if (debugLevel > 1) (*qout) << "Too many pftps. Will try later." << edl;
      qout->alog(1, "MSSGET", sid, remotefilename, "STATUS=MSS_LIMIT_REACHED_WILL_TRY_AGAIN");
  } else if ( pftpmssg == mss_gss_failure ) { // it will give up for good
      if (debugLevel > 1) (*qout) << "  WARNING: GSI proxy has a problem." << edl;
      qout->alog(1, "MSSGET", sid, remotefilename, "STATUS=MSS_GSI_PROXY_ERROR");
  } else if ( pftpmssg == mss_wrong_login ) { // it will give up for good
      if (debugLevel > 1) (*qout) << "  WARNING: Either userid or password are WRONG." << edl;
      qout->alog(1, "MSSGET", sid, remotefilename, "STATUS=MSS_WRONG_LOGIN");
  } else {	// try again
    // this handles all the other errors (mss_message_t):
      long counter = 0;
      bool noBytesTxfed = nothingTxfed;
      do {
	//if ( filestatus.st_size == 0 ) { // wait if no partial transfer
	if (noBytesTxfed) {
	    (*qout) << "HRM will try to transfer " << localfilename << " in " << WAIT_TIME << " secs." << edl;
	    
	    if ( pftpmssg == mss_system_not_available ) {
	      sleep(LONG_WAIT_TIME);		  
	    } else {
	      sleep(WAIT_TIME);
	    }
	    if ( ( pftpmssg == mss_system_not_available ) && (counter > (max_num_trials-3)) ) {		     
	      sleep(LONG_WAIT_TIME);
	    }
	}
	
	++counter;
	
	(*qout) << "Will try to transfer " << localfilename << " again: " << counter << edl;		   
	
	char ltemp[512];
	sprintf(ltemp, "%s.%d", ftptransferlog, counter);
	rename(ftptransferlog, ltemp);

	time_t  lt3 = 0, lt4 = 0;
	double transfertime = 0;
	time(&lt3);	
	FILE *pftpPtr;

	if ((pftpPtr=popen(ftp, "r")) != NULL) {
	  if (debugLevel > 2) (*qout) << "MSSG: errno=" << (int) errno << edl;
	  pclose(pftpPtr);
	}
	time(&lt4);
	transfertime = (double) difftime(lt4,lt3);
	//					transfertime = getMSSTransferTime(ftptransferlog);
	//	                stat(localfilename, &filestatus);

	noBytesTxfed = updatePftpMsg(localfilename, ftptransferlog, pftpmssg);

	if ( (counter >= max_num_trials) && ((pftpmssg != mss_transfer_done) || (pftpmssg != mss_command_successful))) {
	   (*qout) << "Unable to transfer File with " << localfilename << " after trying " << counter << " times." << edl;
	   pftpmssg = mss_error;	      
	}  
      } while(((pftpmssg != mss_transfer_done) || (pftpmssg != mss_command_successful)) && (counter < max_num_trials) );
  }
}

bool FileFetcherHSI::updatePftpMsg(char* localfilename, char* ftptransferlog, mss_message_t& pftpmssg)
{
#ifdef _HRM64
  struct stat64 filestatus;
  int retstat2 = stat64(localfilename, &filestatus);   // get file status
#else
  struct stat filestatus;
  int retstat2 = stat(localfilename, &filestatus);   // get file status
#endif
  if (( retstat2 != 0 ) && (errno == 29)) {
      short checkno=0;
      bool repeatcheck=false;
      sleep(5);
      do {
#ifdef _HRM64
	retstat2 = stat64(localfilename, &filestatus);
#else
	retstat2 = stat(localfilename, &filestatus);
#endif
	if (errno == 29) {
	  if (debugLevel > 1) {
	    (*qout) << "ERRORNO 29: Since local file path has illegal seek problem because of possible file system sync problem, it'll sleep 5seconds until the next check. " << edl;
	    (*qout) << "It'll repeat " << 5-checkno << " more times, and gives up, since it's not a non-existing file path: " << localfilename << edl;
	  }
	  sleep(5);
	  checkno++;
	  if (checkno < 5) repeatcheck=true;
	  else repeatcheck=false;		   
	} else 
	  repeatcheck=false;
      } while (repeatcheck);
      
      if (debugLevel > 1) {
	(*qout) << "File " << localfilename << " transfered!" << edl;		       
      }
      pftpmssg = mss_transfer_done;
  } else {
      if (debugLevel > 1) {
          (*qout) << "File " << localfilename<< " received:" << filestatus.st_size << edl;	     
      }
      pftpmssg = getHSIError(ftptransferlog);
  }

  return (filestatus.st_size == 0);
}

//
// pftpFileGet() is launched from inside a thread and starts a script to
// transfer a file using pftp. after the transfer is done checks whether
// it was successful.
//
//void FileFetcherHSI::mssFileGet(void *pass_info) {
void FileFetcherHSI::mssFileGet(char* localFilePath, char* remoteFilePath, long long remoteFileSize, MSS_ACCESS_TYPE_T fileAccessType) {
#ifdef _HRM64
    struct stat64 filestatus;
#else
    struct stat filestatus;
#endif

// file prep
    char* sid = getStringID();
    char    localfilename[1024], remotefilename[1024];
    strcpy(localfilename, localFilePath);
    strcpy(remotefilename, remoteFilePath);
    long long filesize = remoteFileSize;
// prep end

    // AS2015
    //File    *filePtr=(File *) pass_info;
    time_t  lt1 = 0, lt2 = 0;
    time_t  lt3 = 0, lt4 = 0;
    char    ftp[4096];
    char    space[10] = " ";
    char    int2char[16];
    char    ftptransferlog[512];
    strcpy(ftptransferlog, _config->getValue("MSSLogFile"));
    strcat(ftptransferlog, ".getstransferlog.");
    // AS2015
    //strcat(ftptransferlog,filePtr->getSID());
    strcat(ftptransferlog, sid);

    char    TRANSFERTIME[32] = "TRANSFERTIME=";
    mss_message_t pftpmssg = mss_transfer_done;
    long size_of_file_MB = 0, counter = 0;
    double transfertime = 0, totaltime = 0;

    // start clock
    time(&lt1);

    if (debugLevel > 10) {
      (*qout) << "LOCAL: " << localfilename << edl;
      (*qout) << "REMOTE size: " << filesize << edl;
    }
    // AS2015
    //strcpy(qid, filePtr->getFileQueryID());
    if (debugLevel > 10) {
      (*qout) << "QID: " << sid <<edl;
    }

    // let's make sure the file can be transfered to target directory by
    // creating an empty file there with the target name.
    if (!checkLocalTarget(localfilename, sid, remoteFileSize)) {
      return;
    }    
    
    if (debugLevel > 3) {
      printf("\tThread %d will transfer file\n", pthread_self());
      (*qout) << "\tFile name: " << localfilename << edl << "\tFile size: " << filesize << " bytes."  <<edl;	   
    }
    
    //	HRM::ACCESS_TYPE_T myfiletype = filePtr->getType();
    MSS_ACCESS_TYPE_T myfiletype = fileAccessType;
    (*qout) << "TYPE: " << myfiletype  << edl;
    
    if ((myfiletype == MSS_IMPLICIT) || (myfiletype == MSS_NONE)) {
        sprintf(ftp, "#! /bin/sh\n \
%s -q \"out %s; get -c on %s : %s ; end\"		\
echo \"HSI_ERROR_CODES=\"$? >> %s		\
echo \"Date: `date`\" >> %s			\
ls -l %s >> %s",
	      _config->getValue("HSI"), ftptransferlog, 
	      localfilename, remotefilename,
	      ftptransferlog,
	      ftptransferlog,
	      localfilename, ftptransferlog);    
    } else {
      (*qout) << "HPSS TYPE not implemented: " << myfiletype << edl;
      qout->alog(1, "MSSGET", sid, remotefilename, "STATUS=REQUEST_FAILED_ACCESS_TYPE_UNKNOWN");
      
      return;
    }
    
    if (debugLevel > 100) {
	(*qout) << "HSI script" << edl;
	(*qout) << ftp << edl;
    }

    
    qout->alog(10, "MSSGET",sid,remotefilename,"STATUS=RETRIEVE_START");
    time(&lt3);
    FILE *pftpPtr;
    if ((pftpPtr=popen(ftp, "r")) != NULL) {
      if (debugLevel > 2) (*qout) << "MSSGET: errno=" << (int) errno << edl;
      pclose(pftpPtr);    
    } else {
      if (debugLevel > 2) (*qout) << "MSSGET: errno=" << (int) errno << edl;
      qout->alog(1, "MSSGET", sid, remotefilename, "STATUS=POPEN_FAILED");
      sleep(90);
      if ((pftpPtr=popen(ftp, "r")) == NULL) {
	if (debugLevel > 2) {
	  (*qout) << "MSSGET: errno=" << (int) errno << edl;
	  (*qout) << "ERROR: POPEN failed" << edl;
	}
	// AS2015 OLDHRM
	//			removeAndServeNextFile(filePtr, true, false);
	qout->alog(1, "MSSGET", sid, remotefilename, "STATUS=POPEN_FAILED");
	char cus_item[1024];
	strcpy(cus_item, "REASON=OS_ERROR_FAILED_TO_POPEN");
	strcat(cus_item, remotefilename);
	// AS2015
	//			filePtr->setStatus(HRM::MSS_ERROR);
	//			filePtr->setStatus(MSS_ERROR);
	//			filePtr->call_back(HRM::MSS_ERROR, cus_item);
	//			filePtr->call_back(MSS_ERROR, cus_item);
	return;      
      } else {
	if (debugLevel > 2) (*qout) << "MSSGET: errno=" << (int) errno << edl;
	pclose(pftpPtr);
      }
    }
    
    time(&lt4);
    
    
    // may change this later...
#ifdef _HRM64
    int retstat = stat64(localfilename, &filestatus);   // get file status 
#else
    int retstat = stat(localfilename, &filestatus);   // get file status 
#endif

    if (( retstat != 0 ) && (errno == 29))  {
      short checkno=0;
      bool repeatcheck=false;
      sleep(5);
      do {
#ifdef _HRM64
	retstat = stat64(localfilename, &filestatus);  
#else
	retstat = stat(localfilename, &filestatus);  
#endif
	if (errno == 29) {
	   if (debugLevel > 1) {
	       (*qout) << "ERRORNO 29: Since local file path has illegal seek problem because of possible file system sync problem, it'll sleep 5seconds until the next check. " << edl;
	       (*qout) << "It'll repeat " << 5-checkno << " more times, and gives up, since it's not a non-existing file path: " << localfilename << edl;
	   }
	   sleep(5);
	   checkno++;
	   if (checkno < 5) repeatcheck=true;
	   else repeatcheck=false;
	}
	else repeatcheck=false;
      } while (repeatcheck);
    }
    
    // make sure the file really exists. uneccessary, maybe...
    if ( retstat != 0 ) {
      // if this happens there is no point in going further.
      pftpmssg = getMSSError(ftptransferlog);
      if ( pftpmssg != mss_limit_reached ) 
	(*qout) << "Function stat() failed for file " << localfilename << edl;
      return;
    } 

    //	transfertime = getMSSTransferTime(ftptransferlog);
    transfertime = (double) difftime(lt4,lt3);
    if ( (transfertime == 0) || (filestatus.st_size == 0) ) {
       if (filesize == (long long)filestatus.st_size) { 
	   pftpmssg = mss_transfer_done;
	   if (debugLevel > 10) {
	     (*qout) << "File " << localfilename << " staged to " << filestatus.st_size << "=" << filesize << edl;
	   }      
       } else {
	   // file was not transfered correctly... should I care about it?
	   if ( filestatus.st_size != 0 ) { // partial transfer
	      if (debugLevel > 1) {
	          (*qout) << "File " << localfilename 
		       << " was not transfered correctly..." << edl
		       << "transferTime: " << transfertime 
		       << "   for FID: " << sid << edl
		       << "Size expected: " << filesize
		       << ",  Size transfered: " << filestatus.st_size << edl;
	      }
	   } else {
	      // file was not transfered at all
	      if (debugLevel > 1) (*qout) << "File " << localfilename << " was not transfered." << edl;
	   }

	   pftpmssg = getHSIError(ftptransferlog);
	   handleError(pftpmssg, sid, localfilename, remotefilename, ftp, ftptransferlog, (filestatus.st_size == 0));
       }
    } else {
      pftpmssg = mss_transfer_done;
      if (debugLevel > 10) {
	(*qout) << "File " << localfilename << " staged with " << filestatus.st_size << edl;
      }
      if ( filesize != filestatus.st_size ) { 
	// wrong file size in catalog!
	// remove wrong values
	// AS2015 CACHE
	//			cache->updateEmptyCacheSize((double) filesize);
	//			cache->updateUsedCacheSize((double) (0 - filesize));
	filesize = filestatus.st_size;
	// add correct values
	// AS2015 CACHE
	//			cache->updateEmptyCacheSize((double) (0 - filesize));
	//			cache->updateUsedCacheSize((double) filesize);
	//			filePtr->setRemoteFileSize(filesize);
      }
    }
    
    //	cm->removeFileFromTransferQueue(filePtr);
    
    // removes file from list of files being transfered or queued.
    // if the transfer failed no more trials will happen.
    if ( (pftpmssg == mss_transfer_done) || (pftpmssg != mss_command_successful) )  {
      qout->alog(2, "MSSGET",sid,remotefilename,"STATUS=RETRIEVED");
      unlink(ftptransferlog);
      // AS2015 OLDHRM
      //			removeAndServeNextFile(filePtr);    
    } else if ( (pftpmssg == mss_no_such_file) || 
	      (pftpmssg == mss_no_such_directory) ||
	      (pftpmssg == mss_wrong_login) ||
	      (pftpmssg == mss_gss_failure) ||
	      (pftpmssg == mss_other_error) ||
	      (pftpmssg == mss_error) ||
	      (pftpmssg == mss_no_access_permission)  )  {
      // AS2015
      //		cm->logState(filePtr->getSID()); // gives up if transfer failed
      qout->alog(2, "MSSGET",sid,remotefilename,"ERROR=UNABLE_TO_CACHE_AND_GIVEUP");
      // AS2015
      //		if (pftpmssg == mss_error) cm->logEvent("MSSG",qid,remotefilename,"ERROR=MSS_ERROR");
      // AS2015 OLDHRM
      //			removeAndServeNextFile(filePtr);    
    } else {
      // AS2015
      //		if (pftpmssg == mss_limit_reached) 
      //			cm->logEvent("MSSG",qid,remotefilename,"ERROR=PFTP_LIMIT_REACHED");
      //		if (pftpmssg == mss_system_not_available) 
      //			cm->logEvent("MSSG",qid,remotefilename,"ERROR=MSS_DOWN");
      //		cm->addToHeadOfWaitQueue(filePtr); // adds to list of files queued
      qout->alog(2, "MSSGET",sid,remotefilename,"ERROR=UNABLE_TO_CACHE_AND_PUT_TO_QUEUE_AGAIN");
      sleep(WAIT_TIME);
      // AS2015 OLDHRM
      //			removeAndServeNextFile(filePtr, true, true);
    }
    
    //	serveNextFile(filePtr->getFileTapeID());
    /*
	// if there is a file request queued process it now. if f<p get a file
	// from the same tape, if possible. otherwise get the file at the head
	// of the queue. see setProbabilitySameTape() above. the "while" instead
	// of a "if" guarantees that the number of pftps can increase from the
	// present level if the number of allowed pftps is incresed.
	while ( cm->getNumberMSSPending() < cm->getNumberMSSAllowed()  
			&& cm->getSizeOfWaitQueue() > 0 ) {
		float f = (rand()%1000)/1000;
		if ( f < getProbabilitySameTape() )
			cm->serveFileOfWaitQueueFromThisTape(filePtr->getFileTapeID());
		else
			cm->serveHeadOfWaitQueue();
	}
    */

    // stop clock
    time(&lt2);

    // Call QM to update on transfer status.
    if ( (pftpmssg == mss_transfer_done) || (pftpmssg != mss_command_successful) ) { // successful transfer
        if (!_config->is_true("EnableDetailedLog")) {
	  unlink(ftptransferlog);     
	}
	qout->alog(2, "MSSGET",sid,remotefilename,"STATUS=RETREIVE_DONE");
	// if transfer succeeds remove logfile
        sprintf(int2char,"%.2f",transfertime);
        strcat(TRANSFERTIME,int2char);
	// AS2015
	//        cm->logEvent("MSSG_FINISHED",qid,remotefilename,TRANSFERTIME);
        size_of_file_MB = (long) filesize/MEGABYTE; // convert bytes to MB
	
	totaltime = (double) difftime(lt2,lt1);
	// if this happens then most likely the system failed to open
	// msstransferlog. to make results reasonable assume the following...
	if ( transfertime < 1 ) transfertime = totaltime; // just in case...
	// AS2015
	//		cm->setLastTransferRate((double)size_of_file_MB/transfertime);
	
        if (debugLevel) {
#ifdef STDCOUT
	  cout << "Stats:   FID   Size (MB)   Transfer (sec)   Total (sec)   Transfer Rate (MB/s)"
	       << endl << setw(12) << localfilename << setw(12) << size_of_file_MB 
	       << setiosflags(std::ios::fixed | std::ios::showpoint) << setw(17) 
	       << setprecision(2) << int2char 
	       << setiosflags(std::ios::fixed | std::ios::showpoint) << setw(14) 
	       << setprecision(2) << totaltime
	       << setiosflags(std::ios::fixed | std::ios::showpoint) << setw(17)
	       << setprecision(2) << (float)size_of_file_MB/transfertime << endl;
	  (*qout) << "FILE SIZE: " << filesize << edl;
	  (*qout) << "Real Total Time (sec): " << difftime(lt4,lt3) << edl;
#else
	  char agtr2[1024];
	  sprintf(agtr2, "Stats:   FID   Size (MB)   Transfer (sec)   Total (sec)   Transfer Rate (MB/s)\n");
	  (*qout) << agtr2;
	  sprintf(agtr2, "%12s  %ld  %s  %lf  %.2f\n", localfilename, size_of_file_MB, 
		  int2char,  totaltime, (float)size_of_file_MB/transfertime);
	  (*qout) << agtr2;
	  sprintf(agtr2, "FILE SIZE: %lld\n", filesize);
	  (*qout) << agtr2;
	  sprintf(agtr2, "Real Total Time (sec): %f\n", difftime(lt4,lt3));
	  (*qout) << agtr2;
#endif
	}
	
	// setTransferOverhead to most recent value.
	double to = totaltime/transfertime;
	setTransferOverhead(to);
	if (debugLevel) {
#ifdef STDCOUT
	  std::cout << "Transfer Overhead: " << setiosflags(std::ios::fixed | std::ios::showpoint) << setprecision(2) << to << endl;
#else
	  char agtr3[1024];
	  sprintf(agtr3,"Transfer Overhead: %lf\n", to);
	  (*qout) << agtr3;
#endif
	}
	//		filePtr->setStatus(HRM::REQUEST_DONE);
	// AS2015
	//		filePtr->setStatus(REQUEST_DONE);
	//		filePtr->call_back(HRM::REQUEST_DONE);
	// AS2015
	//		filePtr->call_back(REQUEST_DONE);
	// AS2015
	//        cm->logEvent("CALLBACK",qid,filePtr->getFileID(),"STATUS=GET_REQUEST_DONE");
	//		cm->releaseFile(filePtr);
    } else if ( pftpmssg == mss_no_such_file ) {
      // AS2015
      //		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
      //        cm->logEvent("MSSG_FAILED",qid,remotefilename,"REASON=NO_SUCH_FILE_IN_HPSS");
      qout->alog(2, "MSSGET",sid,remotefilename,"STATUS=MSS_NO_SUCH_FILE");
      unlink(localfilename);      // this deletes any part of the file
      // AS2015 CACHE
      //        cache->updateEmptyCacheSize((double) filesize);
      //        cache->updateUsedCacheSize((double) (0 - filesize));
      //		filePtr->setStatus(HRM::FILE_DOES_NOT_EXIST);
      // AS2015
      //		filePtr->setStatus(FILE_DOES_NOT_EXIST);
      //		filePtr->call_back(HRM::FILE_DOES_NOT_EXIST, "no such path in hpss");
      // AS2015
      //		filePtr->call_back(FILE_DOES_NOT_EXIST, "no such path in hpss");
      //        cm->logEvent("CALLBACK",qid,filePtr->getFileID(),"STATUS=FILE_DOES_NOT_EXIST");
      //		cm->releaseFile(filePtr);
    } else if ( pftpmssg == mss_no_access_permission ) {
      // AS2015
      //		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
      //        cm->logEvent("MSSG_FAILED",qid,remotefilename,"REASON=FILE_CANNOT_BE_READ");
      qout->alog(2, "MSSGET",sid,remotefilename,"STATUS=FILE_CANNOT_BE_READ");
      unlink(localfilename);      // this deletes any part of the file
// AS2015 CACHE
//        cache->updateEmptyCacheSize((double) filesize);
//        cache->updateUsedCacheSize((double) (0 - filesize));
//		filePtr->setStatus(HRM::USER_NOT_AUTHORIZED);
// AS2015
//		filePtr->setStatus(USER_NOT_AUTHORIZED);
//		filePtr->call_back(HRM::USER_NOT_AUTHORIZED, "file cannot be read");
// AS2015
//		filePtr->call_back(USER_NOT_AUTHORIZED, "file cannot be read");
// AS2015
//        cm->logEvent("CALLBACK",qid,filePtr->getFileID(),"STATUS=FILE_CANNOT_BE_READ");
//		cm->releaseFile(filePtr);
    } else if ( pftpmssg == mss_limit_reached ) {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//        cm->logEvent("MSSG_POSTPONED",qid,remotefilename,"REASON=TOO_MANY_PFTPS");
		qout->alog(2, "MSSGET",sid,remotefilename,"STATUS=TOO_MANY_CONNECTIONS");
        unlink(localfilename);      // this deletes any part of the file
// AS2015 CACHE
//        cache->updateEmptyCacheSize((double) filesize);
//        cache->updateUsedCacheSize((double) (0 - filesize));
    } else if ( pftpmssg == mss_error ) {
// AS2015
//		cm->setLastTransferRate(-1.0);
//        cm->logEvent("MSSG_FAILED",qid,remotefilename,"REASON=UNRECOVERABLE_HPSS_ERROR");
		qout->alog(2, "MSSGET",sid,remotefilename,"STATUS=UNRECOVERABLE_HPSS_ERROR");
        unlink(localfilename);      // this deletes any part of the file
//		filePtr->call_back(HRM::MSS_ERROR, "mss_error");
//		filePtr->call_back(MSS_ERROR, "mss_error");
// AS2015
//		cm->releaseFile(filePtr);
//        cm->logEvent("CALLBACK",qid,filePtr->getFileID(),"STATUS=MSS_ERROR");
// AS2015 CACHE
//        cache->updateEmptyCacheSize((double) filesize);
//        cache->updateUsedCacheSize((double) (0 - filesize));
    } else if ( pftpmssg == mss_system_not_available ) {
// AS2015
//		cm->setLastTransferRate(-1.0);
//        cm->logEvent("MSSG_POSTPONED",qid,remotefilename,"REASON=MSS_DOWN");
		qout->alog(2, "MSSGET",sid,remotefilename,"STATUS=MSS_DOWN");
        unlink(localfilename);      // this deletes any part of the file
// AS2015 CACHE
//        cache->updateEmptyCacheSize((double) filesize);
//        cache->updateUsedCacheSize((double) (0 - filesize));
    } else if ( pftpmssg == mss_gss_failure ) {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//        cm->logEvent("MSSG_FAILED",qid,remotefilename,"REASON=GSS_FAILURE");
		qout->alog(2, "MSSGET",sid,remotefilename,"STATUS=GSS_FAILURE");
        unlink(localfilename);      // this deletes any part of the file
// AS2015 CACHE
//        cache->updateEmptyCacheSize((double) filesize);
//        cache->updateUsedCacheSize((double) (0 - filesize));
//		filePtr->call_back(HRM::USER_NOT_AUTHENTICATED, "gss failure");
// AS2015
//		filePtr->call_back(USER_NOT_AUTHENTICATED, "gss failure");
//        cm->logEvent("CALLBACK",qid,filePtr->getFileID(),"STATUS=GSS_FAILURE");
//		cm->releaseFile(filePtr);
    } else if ( pftpmssg == mss_wrong_login ) {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//        cm->logEvent("MSSG_FAILED",qid,remotefilename,"REASON=WRONG_LOGIN");
		qout->alog(2, "MSSGET",sid,remotefilename,"STATUS=WRONG_LOGIN");
        unlink(localfilename);      // this deletes any part of the file
// AS2015 CACHE
//        cache->updateEmptyCacheSize((double) filesize);
//        cache->updateUsedCacheSize((double) (0 - filesize));
//		filePtr->call_back(HRM::USER_NOT_AUTHENTICATED, "wrong login");
//		filePtr->call_back(USER_NOT_AUTHENTICATED, "wrong login");
// AS2015
//        cm->logEvent("CALLBACK",qid,filePtr->getFileID(),"STATUS=WRONG_LOGIN");
//		cm->releaseFile(filePtr);
    } else {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//        cm->logEvent("MSSG_FAILED",qid,remotefilename,"REASON=OTHER_ERROR");
		qout->alog(2, "MSSGET",sid,remotefilename,"STATUS=OTHER_ERROR");
        unlink(localfilename);      // this deletes any part of the file
// AS2015 CACHE
//        cache->updateEmptyCacheSize((double) filesize);
//        cache->updateUsedCacheSize((double) (0 - filesize));
//		filePtr->call_back(HRM::REQUEST_FAILED, "other unknown error");
//		filePtr->call_back(REQUEST_FAILED, "other unknown error");
/* AX FIX
// When DRM is ready getRealFreeCacheSize() will be gone
*/
// AS2015 CACHE
//        if (cache->getRealFreeCacheSize() < (double) filesize) {
//			cache->initializeEmptyCacheSize();
            // transfer failed because..
//			filePtr->call_back(HRM::NOT_ENOUGH_SPACE, "not enough local space");
//			filePtr->call_back(NOT_ENOUGH_SPACE, "not enough local space");
//        	cm->logEvent("CALLBACK",qid,filePtr->getFileID(),"STATUS=NOT_ENOUGH_SPACE");
//           if (debugLevel) (*qout) << "Transfer of file failed for " << localfilename << " because there is not enough" << edl << "empty cache space." << edl;
//        }
//		cm->releaseFile(filePtr);
    }

// AS2015
//	if ( cm->getSizeOfWaitQueue() == 0 ) {
//		if (debugLevel) {
//			(*qout) << "Number of PFTP pending: " << cm->getNumberMSSPending() << edl
//				<< "Number of requests queued: " << cm->getSizeOfWaitQueue() 
//				<< edl;
//		}
//	}

//	if ( pftpmssg != mss_limit_reached ) // file added to queue above
//		cm->releaseFile(filePtr);
//		pthread_mutex_lock(&(filePtr->releasedLock));
//        cm->removeFile(filePtr, true);
//        if (filePtr != NULL) pthread_mutex_unlock(&(filePtr->releasedLock));

    return;
}

//
// getPFTPTransferBytes() is used if the file transfer was successful. It gets
// the transfered bytes from the pftp output.
// this is used only in PUT case
//
double FileFetcherHSI::getMSSTransferBytes(char* logfile) {
    char ftptransferlog[512];
    const int SIZE = 1024;   // some buffer size
    char buffer[SIZE];
    double t = 0;

    strcpy(ftptransferlog,logfile);

    std::ifstream inReadPFTPTime(ftptransferlog, std::ios::in);

    if (!inReadPFTPTime) {
        (*qout) << "File " << ftptransferlog << " could not be opened" << edl;
        return t;
    }

/*
ftp> pput /home/dm/users/u1/asim/src/ppdg/hrm-3.0/hrm/hsi.out /nersc/gc5/asim/x/
hsi.out
200 Command Complete (43, /nersc/gc5/asim/x/hsi.out, 0, 1, 1048576).
200 Command Complete.
150 Transfer starting.
226 Transfer Complete.(moved = 43).
43 bytes sent in 0.23 seconds (0.19 Kbytes/s)
200 Command Complete.
*/
    while ( inReadPFTPTime.getline(buffer, SIZE) ) {
        if ( 0 != strstr(buffer,"bytes sent") ) {
            char sizebuf[1024];
            strcpy(sizebuf, buffer);
            int si=0;
            char* vk = NULL;
            char* value = strtok_r(sizebuf, " ", &vk);
            while ((si != 6) && (value != NULL)) {
                 value = strtok_r(vk, " ", &vk);
                 si++;
            }
            if (si == 6) {
                 t = atof(value);
            }
            std::cout << "\tMSSTransferedSize=" << value << "=" << t << std::endl;
            inReadPFTPTime.close();
            return t;
        }
    }
    inReadPFTPTime.close();
    return t;
}


//
// pftpFilePut() is launched from inside a thread and starts a script to
// transfer a file using pftp. after the transfer is done checks whether
// it was successful.
//
//void FileFetcherHSI::mssFilePut(void *pass_info) {
void FileFetcherHSI::mssFilePut(char* localFilePath, char* remoteFilePath, long long localFileSize, MSS_ACCESS_TYPE_T fileAccessType) {
#ifdef _HRM64
    struct stat64 filestatus;
#else
    struct stat filestatus;
#endif

// file prep
    char* sid = getStringID();
    char    localfilename[1024], remotefilename[1024];
    strcpy(localfilename, localFilePath);
    strcpy(remotefilename, remoteFilePath);
    long long filesize = localFileSize;
// prep end

//    File    *filePtr=(File *) pass_info;
    time_t  lt1 = 0, lt2 = 0;
    time_t  lt3 = 0, lt4 = 0;
    char    ftp[4096];
    char    space[10] = " ";
    char    int2char[16];
    char    ftptransferlog[512];
    strcpy(ftptransferlog, _config->getValue("MSSLogFile"));
    strcat(ftptransferlog, ".puttransferlog.");
    strcat(ftptransferlog,sid);

    char    TRANSFERTIME[32] = "TRANSFERTIME=";
	mss_message_t pftpmssg = mss_transfer_done;
    long size_of_file_MB = 0, counter = 0;
	double transfertime = 0, totaltime = 0;

    // start clock
    time(&lt1);

// AS2015
//    strcpy(localfilename, filePtr->getLocalFileName());
//    strcpy(remotefilename, filePtr->getRemoteFileName());
//    strcpy(qid, filePtr->getFileQueryID());

// for PUT, do checkLocalFile rather than createLocalFile
#ifdef _HRM64
    int lotstat = stat64(localfilename, &filestatus);   // get file status 
#else
    int lotstat = stat(localfilename, &filestatus);   // get file status 
#endif
	if ( lotstat != 0 ) {
		// local file does not exist
		// already checked when request comes in, but just in case...
		// no point of goinf further...
		// remote directory is also checked when request comes in
		(*qout) << "Unable to access local file: " << localfilename << edl;
//		cm->removeFileFromTransferQueue(filePtr);
//		serveNextFile(filePtr->getFileTapeID());
// AS2015 OLDHRM
//		removeAndServeNextFile(filePtr);

		char cus_item[1024];
		strcpy(cus_item, "REASON=UNABLE_TO_ACCESS_LOCAL_FILE_");
		strcat(cus_item, localfilename);
// AS2015
//		cm->logEvent("MSSP",qid,remotefilename,cus_item);
//		filePtr->call_back(HRM::NO_DIRECTORY_PERMISSION, cus_item);
//		filePtr->call_back(NO_DIRECTORY_PERMISSION, cus_item);
		qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=NO_DIRECTORY_PERMISSION");
// AS2015
//		cm->releaseFile(filePtr);
		return;
	}

// AS2015
//    filesize = filePtr->getLocalFileSize();
    if (debugLevel > 1) {
#ifdef STDCOUT
		cout << "\tThread " << pthread_self() << " will transfer file " << endl;
		(*qout) << "\tFile name: " << localfilename 
		<< edl << "\tFile size: " << filesize << " bytes." << edl;
#else
		char agtr4[2048];
		sprintf(agtr4,"\tThread %d will transfer files\n", pthread_self());
		(*qout) << agtr4;
		sprintf(agtr4,"\tFile name: %s size: %lld bytes \n", localfilename, filesize);
		(*qout) << agtr4;
#endif
	}

//	HRM::ACCESS_TYPE_T myfiletype = filePtr->getType();
	MSS_ACCESS_TYPE_T myfiletype = fileAccessType;
	(*qout) << "TYPE: " << myfiletype << edl;
// BNL needs "quote site setcos 18"
	char hpsssitecmd[1024];
	if (_config->getValue("MSSSITECMD")) {
		strcpy(hpsssitecmd, _config->getValue("MSSSITECMD"));
		strcat(hpsssitecmd, "\n");
	}
	else {
		strcpy(hpsssitecmd, "\n");
	}


//	if ((myfiletype == HRM::NONE) || (myfiletype == HRM::KERBEROS)) {
	if ((myfiletype == MSS_IMPLICIT) || (myfiletype == MSS_NONE)) {
	    if (_config->getValue("checksum") == NULL) {
	        sprintf(ftp, "#! /bin/sh\n \
                        %s -q \"out %s; put -c on %s : %s ; end\"	\
                        echo \"HSI_ERROR_CODES=\"$? >> %s		\
                        echo \"Date: `date`\" >> %s",
			_config->getValue("HSI"), ftptransferlog,
			localfilename, remotefilename,
			ftptransferlog,
			ftptransferlog);
	    } else {
	        sprintf(ftp, "#! /bin/sh\n \
                        %s -q \"out %s; put -c on -H %s  %s : %s ; end\"	\
                        echo \"HSI_ERROR_CODES=\"$? >> %s		\
                        echo \"Date: `date`\" >> %s",
			_config->getValue("HSI"), ftptransferlog, _config->getValue("checksum"),
			localfilename, remotefilename,
			ftptransferlog,
			ftptransferlog);
	    }
	} else {
	  (*qout) << "HPSS TYPE not implemented: " << myfiletype << edl;
// AS2015 OLDHRM
//			removeAndServeNextFile(filePtr);

//            filePtr->setStatus(HRM::REQUEST_FAILED);
//            filePtr->setStatus(REQUEST_FAILED);
//            filePtr->call_back(HRM::REQUEST_FAILED, "invalid access type");
//            filePtr->call_back(REQUEST_FAILED, "invalid access type");
            qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=REQUEST_FAILED_ACCESS_TYPE_UNKNOWN");
// AS2015
//            cm->releaseFile(filePtr);
			return;
	}

	if (debugLevel > 1) {
		if(debugLevel > 10) {
			(*qout) << "HSI script" << edl;
			(*qout) << ftp << edl;
			(*qout) << "ACCESS_INFO" << edl;
		}
// AS2015
//		(*qout) << "Number of PFTP pending: " << cm->getNumberMSSPending() << edl
//			<< "Number of requests queued: " << cm->getSizeOfWaitQueue() 
//			<< edl;
	}

	qout->alog(1, "MSSPUT", sid, remotefilename, "STATUS=ARCHIVE_START");
    time(&lt3);
	FILE *pftpPtr;
	if ((pftpPtr=popen(ftp, "r")) != NULL) {
		if (debugLevel > 2) (*qout) << "MSSPUT: errno=" << (int) errno << edl;
		pclose(pftpPtr);
	}
	else {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
		if (debugLevel > 2) (*qout) << "MSSPUT: errno=" << (int) errno << edl;
		qout->alog(1, "MSSPUT", sid, remotefilename, "STATUS=POPEN_FAILED");
		sleep(WAIT_TIME);
		if ((pftpPtr=popen(ftp, "r")) == NULL) {
			if (debugLevel > 2) {
				(*qout) << "MSSPUT: errno=" << (int) errno << edl;
				(*qout) << "ERROR: POPEN failed" << edl;
			}
// AS2015
//			cm->setLastTransferRate(-1.0);
			qout->alog(1, "MSSPUT", sid, remotefilename, "STATUS=POPEN_FAILED");
			char cus_item[1024];
			strcpy(cus_item, "REASON=OS_ERROR_FAILED_TO_POPEN");
			strcat(cus_item, remotefilename);
// AS2015
//			filePtr->setStatus(HRM::MSS_ERROR);
//			filePtr->call_back(HRM::MSS_ERROR, cus_item);
// AS2015 OLDHRM
//			removeAndServeNextFile(filePtr, true, true);

			return;
		}
		else {
			if (debugLevel > 2) (*qout) << "MSSPUT: errno=" << (int) errno << edl;
			pclose(pftpPtr);
		}
	}
	if (debugLevel > 2) (*qout) << "MSSPUT: errno=" << (int) errno << edl;

    time(&lt4);

	if (debugLevel > 1) {
#ifdef STDCOUT
//		cout << "Aggregated transfer rate: " << setiosflags(std::ios::fixed | std::ios::showpoint) << setprecision(2) << cm->getTransferRate()/MEGABYTE << " MB/sec" << endl;
#else
//		char agtr5[1024];
//		sprintf(agtr5, "Aggregated transfer rate: %.2f MB/sec\n",  cm->getTransferRate()/MEGABYTE);
//		(*qout) << agtr5;
#endif
	}

// AS2015 CHECK
// make sure this remoteFileSize is properly collected.
// if not, need to be either removed or replaced by something else
	double remoteFileSize = getMSSTransferBytes(ftptransferlog);

//	transfertime = getMSSTransferTime(ftptransferlog);
	transfertime = (double) difftime(lt4,lt3);
	if( filesize != (long long) remoteFileSize ) { 
	  // file was not transfered correctly... should I care about it?
	  if ( remoteFileSize != 0 ) { // partial transfer
	    if (debugLevel) {
	      (*qout) << "File " << localfilename 
		      << " was not transfered correctly to" 
		      << remotefilename << edl
		      << "transferTime: " << transfertime 
		      << "   for FID: " << sid << edl
		      << "Size expected: " << filesize
		      << ",  Size transfered: " << remoteFileSize << edl;
	    }
	  } else {
	    // file was not transfered at all
	    if (debugLevel) 
	      (*qout) << "File " << localfilename << " to " 
		      << remotefilename
		      << " was not transfered." << edl;
	  }
	  pftpmssg = getHSIError(ftptransferlog);
	  if ( pftpmssg == mss_no_access_permission ) { // it will give up for good
            if (debugLevel) (*qout) << "File has no write permission in HPSS." << edl 
				    << "\tHPSS Path: " << remotefilename
				    << "\tLocal Path: " << localfilename << edl;
	    qout->alog(0, "MSSPUT", sid, remotefilename, "ERROR=NO_WRITE_PERMISSION");
	  } else if ( pftpmssg == mss_no_such_file ) { // will give up for now
            if (debugLevel) (*qout) << "File does not exist in HPSS." 
				    << edl << "\tHPSS Path: " << remotefilename << edl
				    << "\tLocal Path: " << localfilename << edl;
	    qout->alog(0, "MSSPUT", sid, remotefilename, "ERROR=NO_SUCH_FILE");
	  } else if ( pftpmssg == mss_limit_reached ) { // will give up for now
            if (debugLevel) (*qout) << "Too many pftps. Will try later." << edl;
	    qout->alog(0, "MSSPUT", sid, remotefilename, "ERROR=LIMIT_REACHED");
	  } else if ( pftpmssg == mss_gss_failure ) { // it will give up for good
            if (debugLevel) (*qout) << "  WARNING: Either userid or password are WRONG." << edl;
	    qout->alog(0, "MSSPUT", sid, remotefilename, "ERROR=GSS_FAILURE");
	  } else if ( pftpmssg == mss_wrong_login ) { // it will give up for good
            if (debugLevel) (*qout) << "  WARNING: Either userid or password are WRONG." << edl;
	    qout->alog(0, "MSSPUT", sid, remotefilename, "ERROR=WRONG_LOGIN");
	  } else {	// try again
			// this handles all the other errors (mss_messages):
	    // AS2015
	    //			if ( pftpmssg == mss_system_not_available ) {
	    //				cm->setLastTransferRate(-1.0);
	    //       		cm->logEvent("MSSP",qid,remotefilename,"ERROR=MSS_DOWN");
	    //			}
            do {
	      qout->alog(1, "MSSPUT", sid, remotefilename, "ERROR=UNABLE_TO_ARCHIVE_TRY_AGAIN");
	      if ( remoteFileSize == 0 ) { // wait if no partial transfer
		if (debugLevel) 
		  (*qout) << "HRM will try to transfer " << localfilename
			  << " in " << WAIT_TIME
			  << " secs." << edl;
		if ( pftpmssg == mss_system_not_available ) {
		  // AS2015
		  //						cm->setLastTransferRate(-1.0);
		  sleep(LONG_WAIT_TIME);
		}
		else {
		  // AS2015
		  //						cm->setLastTransferRate(-1.0);
		  sleep(WAIT_TIME);
		}
		if ( ( pftpmssg == mss_system_not_available ) && 
		     (counter > (max_num_trials-3)) ) {
		  // AS2015
		  //						cm->setLastTransferRate(-1.0);
		  sleep(LONG_WAIT_TIME);
		}
	      }
	      ++counter;
	      if (debugLevel) {
		(*qout) << "Will try to transfer " << localfilename
			<< " again." << edl;
	      }
	      char ltemp[512];
	      sprintf(ltemp, "%s.%d", ftptransferlog, counter);
	      rename(ftptransferlog, ltemp);
	      if ((pftpPtr=popen(ftp, "r")) != NULL) {
		if (debugLevel > 2) (*qout) << "MSSPUT: errno=" << (int) errno << edl;
		pclose(pftpPtr);
	      }
	      if (debugLevel > 2) (*qout) << "MSSPUT: errno=" << (int) errno << edl;
	      remoteFileSize = getMSSTransferBytes(ftptransferlog);
	      if (filesize == (long long) remoteFileSize) {
		if (debugLevel) {
		  (*qout) << "File " << localfilename << " transfered!" << edl;			  
		}
		pftpmssg = mss_transfer_done;
		qout->alog(1, "MSSPUT", sid, remotefilename, "STATUS=ARCHIVE_DONE");
	      } else {
		if (debugLevel) {
		  (*qout) << "File " << localfilename
			  << " : " << filesize << "=?" << remoteFileSize << edl;
		}
		pftpmssg = getHSIError(ftptransferlog);
	      }
	      if ( (counter >= max_num_trials) && 
		   ((pftpmssg != mss_transfer_done) || (pftpmssg != mss_command_successful)) ) 
	      {
		if (debugLevel) (*qout) << "Unable to transfer File with " 
					<< localfilename
					<< " after trying " << counter << " times." << edl;
		qout->alog(1, "MSSPUT", sid, remotefilename, "ERROR=UNABLE_TO_ARCHIVE");
		pftpmssg = mss_error;
	      }
            } while( ((pftpmssg != mss_transfer_done) || (pftpmssg != mss_command_successful)) && (counter < max_num_trials) );
	  }
	} else {
	  pftpmssg = mss_transfer_done;
	  unlink(ftptransferlog);
	}
	

	// removes file from list of files being transfered or queued.
	// if the transfer failed no more trials will happen.
	if (( pftpmssg == mss_transfer_done ) || (pftpmssg == mss_command_successful)) {
		qout->alog(5, "MSSPUT", sid, remotefilename, "STATUS=ARCHIVED");
// AS2015 OLDHRM
//			removeAndServeNextFile(filePtr);

	}
	else if ( (pftpmssg != mss_limit_reached) && 
			(pftpmssg != mss_error) &&
			(pftpmssg != mss_system_not_available) ) {
// AS2015
//		cm->logState(filePtr->getSID()); // gives up if transfer failed
		qout->alog(5, "MSSPUT", sid, remotefilename, "ERROR=UNABLE_TO_ARCHIVE_AND_GIVEUP");
// AS2015 OLDHRM
//			removeAndServeNextFile(filePtr);

	}
	else {
// AS2015
//		cm->logEvent("MSSP",qid,remotefilename,"ERROR=PFTP_LIMIT_REAHCED_OR_MSS_ERROR_AND_PUT_TO_QUEUE");
		qout->alog(5, "MSSPUT", sid, remotefilename, "ERROR=MSS_ERROR_AND_PUT_TO_QUEUE");
		sleep(WAIT_TIME);
// AS2015 OLDHRM
//			removeAndServeNextFile(filePtr, true, true);

	}

    // stop clock
    time(&lt2);

    // update on transfer status.
    if (( pftpmssg == mss_transfer_done ) || (pftpmssg == mss_command_successful)) { // successful transfer
        if (!_config->is_true("EnableDetailedLog")) {
		unlink(ftptransferlog);
	}
				     // if transfer succeeds remove logfile
        sprintf(int2char,"%.2f",transfertime);
        strcat(TRANSFERTIME,int2char);
// AS2015
//        cm->logEvent("MSSP_FINISHED",qid,remotefilename,TRANSFERTIME);
        size_of_file_MB = (long) filesize/MEGABYTE; // convert bytes to MB

		totaltime = (double) difftime(lt2,lt1);
		// if this happens then most likely the system failed to open
		// msstransferlog. to make results reasonable assume the following...
		if ( transfertime < 1 ) transfertime = totaltime; // just in case...
// AS2015
//		cm->setLastTransferRate((double)size_of_file_MB/transfertime);

        if (debugLevel) {
#ifdef STDCOUT
			cout << "Stats:   FID   Size (MB)   Transfer (sec)   Total (sec)   Transfer Rate (MB/s)"
			<< endl << setw(12) << localfilename << setw(12) << size_of_file_MB 
			<< setiosflags(std::ios::fixed | std::ios::showpoint) << setw(17) 
			<< setprecision(2) << int2char 
			<< setiosflags(std::ios::fixed | std::ios::showpoint) << setw(14) 
			<< setprecision(2) << totaltime
			<< setiosflags(std::ios::fixed | std::ios::showpoint) << setw(17)
			<< setprecision(2) << (float)size_of_file_MB/transfertime << endl;
			(*qout) << "Real Total (sec):  " << difftime(lt4,lt3) << edl;
#else
			char agtr6[1024];
			sprintf(agtr6, "Stats:   FID   Size (MB)   Transfer (sec)   Total (sec)   Transfer Rate (MB/s)\n");
			(*qout) << agtr6;
			sprintf(agtr6, "%s   %ld  %s  %lf  %f \n", localfilename, size_of_file_MB, 
					int2char, totaltime, (float)size_of_file_MB/transfertime);
			(*qout) << agtr6;
			sprintf(agtr6, "Real Total (sec):  %d\n", difftime(lt4,lt3));
			(*qout) << agtr6;
#endif
		}

		// setTransferOverhead to most recent value.
		double to = totaltime/transfertime;
		setTransferOverhead(to);
		if (debugLevel) {
#ifdef STDCOUT
//			cout << "Transfer Overhead: " << setiosflags(ios::fixed | ios::showpoint) << setprecision(2) << to << endl;
#else
//			char agtr7[1024];
//			sprintf(agtr7, "Transfer Overhead: %lf\n", to);
//			(*qout) << agtr7;
#endif
		}
// AS2015 CACHE
//        cache->updateEmptyCacheSize((double) filesize);
//        cache->updateUsedCacheSize((double) (0 - filesize));
//		filePtr->setStatus(HRM::REQUEST_DONE);
//		filePtr->setStatus(REQUEST_DONE);
//		filePtr->call_back(HRM::REQUEST_DONE);
//		filePtr->call_back(REQUEST_DONE);
		qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=PUT_REQUEST_DONE");
// AS2015
//		cm->releaseFile(filePtr);
    } else if ( pftpmssg == mss_no_such_file ) {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//        cm->logEvent("MSSP",qid,remotefilename,"ERROR=NO_SUCH_FILE_IN_HPSS");
//        unlink(localfilename);      // this deletes any part of the file
// AS2015 CACHE
//        cache->updateEmptyCacheSize((double) filesize);
//        cache->updateUsedCacheSize((double) (0 - filesize));
//		filePtr->setStatus(HRM::FILE_DOES_NOT_EXIST);
//		filePtr->setStatus(FILE_DOES_NOT_EXIST);
//		filePtr->call_back(HRM::FILE_DOES_NOT_EXIST, "no such path in hpss");
//		filePtr->call_back(FILE_DOES_NOT_EXIST, "no such path in hpss");
		qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=FILE_DOES_NOT_EXIST");
// AS2015
//		cm->releaseFile(filePtr);
    } else if ( pftpmssg == mss_no_access_permission ) {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//        cm->logEvent("MSSP",qid,remotefilename,"ERROR=FILE_CANNOT_BE_WRITTEN");
//		filePtr->setStatus(HRM::USER_NOT_AUTHORIZED);
//		filePtr->setStatus(USER_NOT_AUTHORIZED);
//		filePtr->call_back(HRM::USER_NOT_AUTHORIZED, "file cannot be written");
//		filePtr->call_back(USER_NOT_AUTHORIZED, "file cannot be written");
		qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=FILE_DOES_NOT_EXIST");
// AS2015
//		cm->releaseFile(filePtr);
    } else if ( pftpmssg == mss_limit_reached ) {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//        cm->logEvent("MSSP",qid,remotefilename,"ERROR=POSTPONED_TOO_MANY_PFTPS");
		qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=POSTPONED_TOO_MANY_CONNECTIONS");
    } else if ( pftpmssg == mss_error ) {
// AS2015
//		cm->setLastTransferRate(-1.0);
//        cm->logEvent("MSSP",qid,remotefilename,"ERROR=UNRECOVERABLE_HPSS_ERROR");
//		filePtr->setStatus(HRM::MSS_ERROR);
//		filePtr->setStatus(MSS_ERROR);
//		filePtr->call_back(HRM::MSS_ERROR, "hpss error");
//		filePtr->call_back(MSS_ERROR, "hpss error");
		qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=MSS_ERROR");
// AS2015
//		cm->releaseFile(filePtr);
    } else if ( pftpmssg == mss_system_not_available ) {
// AS2015
//		cm->setLastTransferRate(-1.0);
//        cm->logEvent("MSSP",qid,remotefilename,"ERROR=POSTPONED_MSS_DOWN");
		qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=POSTPONED_MSS_DOWN");
    } else if ( pftpmssg == mss_gss_failure ) {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//        cm->logEvent("MSSP",qid,remotefilename,"ERROR=GSS_FAILURE");
//		filePtr->call_back(HRM::USER_NOT_AUTHENTICATED, "gss failure");
//		filePtr->call_back(USER_NOT_AUTHENTICATED, "gss failure");
		qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=GSS_FAILURE");
// AS2015
//		cm->releaseFile(filePtr);
    } else if ( pftpmssg == mss_wrong_login ) {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//        cm->logEvent("MSSP",qid,remotefilename,"ERROR=WRONG_LOGIN");
//		filePtr->call_back(HRM::USER_NOT_AUTHENTICATED, "wrong login");
//		filePtr->call_back(USER_NOT_AUTHENTICATED, "wrong login");
		qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=WRONT_LOGIN");
// AS2015
//		cm->releaseFile(filePtr);
    } else {
// AS2015
//		if (cm->getLastTransferRate() < 0) cm->setLastTransferRate(0.0);
//        cm->logEvent("MSSP",qid,remotefilename,"ERROR=NOTCHECKED");
//		filePtr->call_back(HRM::REQUEST_FAILED, "other unknown error");
//		filePtr->call_back(REQUEST_FAILED, "other unknown error");
		qout->alog(1, "MSSPUT", sid, localfilename, "STATUS=UNKNOWN");
// AS2015
//		cm->releaseFile(filePtr);
    }

// AS2015
//	if ( cm->getSizeOfWaitQueue() == 0 ) {
//		if (debugLevel) {
//			(*qout) << "Number of PFTP pending: " << cm->getNumberMSSPending() << edl
//				<< "Number of requests queued: " << cm->getSizeOfWaitQueue() 
//				<< edl;
//		}
//	}

    return;
}





