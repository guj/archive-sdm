// File: hrm_logger.cpp
// Author: Alex Sim <ASim@lbl.gov>
// Copyright 1998-2001 the Regents of the University of California
//
//  Purpose: The implementation of class Logger
//
// Notes:
// 	Create a log object by specifying the file name of the file
// 	to send log messages to and the name of program as it should
// 	appear in the logfile.
// 	Simple log messages are sent through the log object by the left shift operator:
//
// 	Logger log("hrm.log","hrm");
// 	log << "start";
// 	// do something
// 	log << "event1";
//
// 	more advanced usage:
// 	log.Log("event2", query_token, file_id, event_it_id, "all is well");
//
// 	The log object can safely be used from multiple concurrent threads.
//
// Created: 980831 by Henrik Nordberg
// Last Modified: 111601
// added by Luis Bernardo: m_num_lines, m_max_num_lines, renameLogFile().
// comments: when the log file reaches 500K lines the file is renamed with
// a time stamp appended and a new log file is created.

#include "sdmmss_logger.h"

#ifdef _WIN32
	#include <windows.h>
	#include <process.h>	// Win32 threads
	#include <strstrea.h>	// class strstream
	#if defined(HAS_POSIX_THREADS)
		#if !defined(_POSIX_C_SOURCE)
			#define _POSIX_C_SOURCE 200809L
		#endif // !defined(_POSIX_C_SOURCE)
		#include <pthread.h>    // POSIX thread library
	#endif
#else
	#if !defined(_POSIX_C_SOURCE)
		#define _POSIX_C_SOURCE 200809L
	#endif // !defined(_POSIX_C_SOURCE)
	#include <pthread.h>	// POSIX thread library
	#include <strstream>	// class strstream
#endif

#include <errno.h>		// errno
#include <cerrno>		// errno
#include <stdio.h>		// FILE, fopen(), fprintf(), fclose(), rename()
#include <string.h>		// strlen()
#include <time.h> 		// time_t, ctime_r()
#include <sys/types.h>	// chmod() (mode_t)
#include <sys/stat.h>	// chmod()

#include "sdmmss_util.h"	// strnewdup()
#include "sdmmss_logger.h"	// class Logger

int Logger::m_num_instances=0;
pthread_mutex_t Logger::m_static_log_mutex;

Logger::Logger(char const * fn, char const * module_name, logMethod method, bool b_append/*=true*/) : 
	m_filename( fn?strnewdup(fn):0 ), m_module_name( module_name?strnewdup(module_name):0 ), m_unsafe_locking(false), m_file(0), m_num_lines(0), m_max_num_lines(500000)
{
	init(b_append);
}

void Logger::init(bool b_append)
{
	if( pthread_mutex_init(&m_log_mutex,0) != 0 )
		throw "pthread_mutex_init() failed in Logger() init()";

	if( !m_num_instances++ )
		if( pthread_mutex_init(&m_static_log_mutex,0) != 0 )
			throw "pthread_mutex_init() failed in Logger() init(), for m_static_log_mutex";

	if( m_filename )
	{
#ifdef _HRM64
		struct stat64 filestatus;
		if ( stat64(m_filename, &filestatus) == 0 ) {	// the file exists
#else
		struct stat filestatus;
		if ( stat(m_filename, &filestatus) == 0 ) {	// the file exists
#endif
			// 130 is the "average" number of bytes per line. no big deal
			// if this is hardcoded.
			m_num_lines = (int)filestatus.st_size/130;
			if ( m_num_lines > m_max_num_lines ) 
				renameLogFile();
		}
				
		FILE *f;
		if( b_append ) f = fopen(m_filename, "a");
		else f = fopen(m_filename, "w");

		if( f )
		{
			fclose(f);
			if( chmod( m_filename, S_IROTH | S_IWOTH | S_IRGRP | S_IWGRP | S_IRUSR | S_IWUSR ) == -1 &&
				errno != EPERM )
			{
				std::cerr << "Warning: failed to chmod(\"" << m_filename << "\")" << std::endl;
				REASON;
			}
		}
		else
		{
			std::cerr << "couldn't open log file " << m_filename << std::endl;	
			REASON;
			// We don't return an error code here, because we will try to open the file again, when we do the logging
		}
	}
	else 
	{
		pthread_mutex_destroy(&m_log_mutex);
		if( !--m_num_instances ) { pthread_mutex_destroy(&m_static_log_mutex); }
		throw "Logger() ctor: must specify a valid file name";
	}

	if( !m_module_name )
	{
		if( m_filename ) delete[] m_filename;
		pthread_mutex_destroy(&m_log_mutex);
		if( !--m_num_instances ) { pthread_mutex_destroy(&m_static_log_mutex); }
		throw "Logger() ctor: must specify a valid module name";
	}
}

Logger::~Logger()
{
	delete[] m_filename;
	delete[] m_module_name;
	if (m_file) fclose(m_file);
	pthread_mutex_destroy(&m_log_mutex);
	if( !--m_num_instances ) { pthread_mutex_destroy(&m_static_log_mutex); }
}

void Logger::Log(char const * s_event, char const * s_query_token, char const * fid, char const * custom_s)
{
	char s[6144] = "";
	if( s_event ) sprintf(s,"NL.EVNT=%s",s_event);
	if( s_query_token ) { strcat(s," QUERY="); strcat(s,s_query_token); }
	if( fid != 0 )
	{
		char b[1024];
		sprintf(b," FID=%s",fid);
		strcat(s,b);
	}
	if( custom_s ) { strcat(s," "); strcat(s,custom_s); }
	Log_to_file(s);

//	if( chmod( m_filename, S_IROTH | S_IWOTH | S_IRGRP | S_IWGRP | S_IRUSR | S_IWUSR ) == -1 &&
//		errno != EPERM )
//	{
//		//cerr << "Warning: failed to chmod(\"" << fn << "\")" << endl;
//		//REASON;
//	}
}

void Logger::Log_to_file(char const * str, enum severity)
{
	++m_num_lines;
/************** NetLogger time format  */
	if( m_unsafe_locking ) pthread_mutex_lock(&m_log_mutex);
	else pthread_mutex_lock(&m_static_log_mutex);

	char buff[32];

	time_t t1 = 0;
	time(&t1);

#if defined(_WIN32)
	struct tm *t;
	t=localtime(&t1);
	strftime(buff, 32, "%Y%m%d%H%M%S", t);
#else
	struct tm t;
	localtime_r(&t1,&t);
	strftime(buff, 32, "%Y%m%d%H%M%S", &t);
#endif // defined(_WIN32)

	if ( m_num_lines > m_max_num_lines ) {
		fclose(m_file);
		m_file = 0;
		m_num_lines = 0;
		renameLogFile();
	}

	if( !m_file && !(m_file=fopen(m_filename,"a+")) )
	{
		std::cerr << "couldn't open log file " << m_filename << std::endl;
		REASON;
	}
	else
	{
		fseek(m_file,0,SEEK_END);
		fprintf(m_file, "DATE=%s HOST=blank PROG=%s LVL=Usage NL.SEC=%d NL.USEC=0 %s\n", buff, m_module_name, t1, str);
		fflush(m_file);
	}

	if( m_unsafe_locking ) pthread_mutex_unlock(&m_log_mutex);
	else pthread_mutex_unlock(&m_static_log_mutex);
}

void Logger::renameLogFile() {
	char buff[32];
	char filename[1024];

	time_t t1 = 0;
	time(&t1);

#if defined(_WIN32)
	struct tm *t;
	t=localtime(&t1);
	strftime(buff, 32, "%Y%m%d%H%M%S", t);
#else
	struct tm t;
	localtime_r(&t1,&t);
	strftime(buff, 32, "%Y%m%d%H%M%S", &t);
#endif // defined(_WIN32)

	strcpy(filename,m_filename);
	strcat(filename,".");
	strcat(filename,buff);

	if ( rename(m_filename,filename) != 0 ) {
		std::cerr << "Unable to rename " << m_filename << " to "
		<< filename << " (errno = " << errno << ")." << std::endl;
		if ( strncmp(m_filename,"/tmp/",5) == 0 )
			std::cerr << "Move " << m_filename << " out of /tmp." << std::endl;
	}

	return;
}
