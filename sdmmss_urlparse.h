//
// This file is a modified version of globus_url.h 
// Very specific to HRM's internal URL parsing

#ifndef URL_PARSE_H

#ifndef EXTERN_C_BEGIN
#ifdef __cplusplus
#define EXTERN_C_BEGIN extern "C" {
#define EXTERN_C_END }
#else
#define EXTERN_C_BEGIN
#define EXTERN_C_END
#endif
#endif

EXTERN_C_BEGIN

typedef enum {
    URL_PROTOCOL_FTP=0,
    URL_PROTOCOL_GSIFTP,
    URL_PROTOCOL_HTTP,
    URL_PROTOCOL_HTTPS,
    URL_PROTOCOL_LDAP,
    URL_PROTOCOL_FILE,
    URL_PROTOCOL_SRM,
    URL_PROTOCOL_HRM,
    URL_PROTOCOL_UNKNOWN // anything of the form <scheme>://<something> 
} url_protocol_t;

//
//  Other protocols defined in RFCs but _not_ supported here are 
// 'news', 'nntp', 'telnet', 'gopher', 'wais', 'mailto', and 'prospero'
//

/*
 * @struct url_t Parsed URLs.
 * @ingroup url
 */
typedef struct {
    char *protocol;		/* protocol (http, ftp, etc) name in RFC 1738 */
    url_protocol_t protocol_type;
    /* 
      ftp://[user[:password]@]host[:port]/[url_path]
      gsiftp://[user[:password]@]host[:port]/[url_path]
      http://host[:port]/url_path
      x-nexus://host:port
      x-gass-cache://url_path
      ldap://host[:port]/dn?attributes?scope?filter
      otherurl://host[:port]/url_path or
      otherurl://url_specific_part
    */
    char *user;			/* ftp, gsiftp */
    char *password;		/* ftp, gsiftp */
    char *host;			/* ftp, gsiftp, http, https, ldap, x-nexus */
    unsigned short port;	/* ftp, gsiftp, http, https, ldap, x-nexus */
    char *url_path;		/* ftp, gsiftp, http, https  */

    char *dn;			/* ldap */
    char *attributes;	/* ldap */
    char *scope;		/* ldap */
    char *filter;		/* ldap */
    
    char *url_specific_part;	/* x-gass-cache and for unknown url protocols */
} HRM_URL_T;

int hrm_url_parse(const char *url_string, HRM_URL_T *url);
int hrm_url_destroy(HRM_URL_T *url);

/* Return conditions */
#define URL_SUCCESS 0
#define URL_ERROR_NULL_STRING -1
#define URL_ERROR_NULL_URL -2
#define URL_ERROR_BAD_PROTOCOL -3
#define URL_ERROR_BAD_USER -4
#define URL_ERROR_BAD_PASSWORD -5
#define URL_ERROR_BAD_HOST -6
#define URL_ERROR_BAD_PORT -7
#define URL_ERROR_BAD_PATH -8

/* for ldap URLs */
#define URL_ERROR_BAD_DN -9
#define URL_ERROR_BAD_ATTRIBUTES -10
#define URL_ERROR_BAD_SCOPE -11
#define URL_ERROR_BAD_FILTER -12

/* when malloc fails */
#define URL_ERROR_OUT_OF_MEMORY -13

/* for nexus errors/former assertion failures */
#define URL_ERROR_INTERNAL_ERROR -14

EXTERN_C_END
#endif
