/**
 * @file globus_url.c URL parsing utility functions
 *
 * Parses URLs of the form
 * @code
 *  <protocol>://[<user>[:<password>]@]<host>[:<port>]/<url-path>
 * @endcode
 *
 * @code
 * <protocol>://<protocol-specific-part>
 * @endcode
 */

#include "sdmmss_urlparse.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

static int url_get_substring(const char *src,
				    char **destp,
				    int nullp);

static int url_get_protocol(const char **stringp,
				 char **protocol,
				 url_protocol_t *protocol_type);

static int url_get_user_password(const char **stringp,
					char **user,
					char **password);
static int url_get_host_port(const char **stringp,
				    char **host,
				    unsigned short *port);
static int url_get_path(const char **stringp,
			       char **url_path);
static int url_get_ldap_specific(const char **stringp,
					char **dn,
					char **attributes,
					char **scope,
					char **filter);
	
static int
url_get_file_specific(const char **stringp,
			      char ** host,
			      char ** path);

static int url_issafe(char x);
static int url_isextra(char x);
static int url_isprotocol_special(char x);

/**
 * Parse a string containing a URL into a HRM_URL_T
 * @ingroup url
 *
 * @param url_string
 *        String to parse
 * @param url
 *        Pointer to HRM_URL_T to be filled with the fields of the url
 *
 * @retval true
 *         The string was successfully parsed.
 * @retval URL_ERROR_NULL_STRING
 *         The url_string was NULL.
 * @retval URL_ERROR_NULL_URL
 *         The URL pointer was NULL.
 * @retval URL_ERROR_BAD_PROTOCOL 
 *         The URL protocol contained invalid characters.
 * @retval URL_ERROR_BAD_USER 
 *         The user part of the URL contained invalid characters.
 * @retval URL_ERROR_BAD_PASSWORD
 *         The password part of the URL contained invalid characters.
 * @retval URL_ERROR_BAD_HOST
 *         The host part of the URL contained invalid characters.
 * @retval URL_ERROR_BAD_PORT
 *         The port part of the URL contained invalid characters.
 * @retval URL_ERROR_BAD_PATH
 *         The path part of the URL contained invalid characters.
 * @retval URL_ERROR_BAD_DN -9
 *         The DN part of an LDAP URL contained invalid characters.
 * @retval URL_ERROR_BAD_ATTRIBUTES -10
 *         The attributes part of an LDAP URL contained invalid characters.
 * @retval URL_ERROR_BAD_SCOPE -11
 *         The scope part of an LDAP URL contained invalid characters.
 * @retval URL_ERROR_BAD_FILTER -12
 *         The filter part of an LDAP URL contained invalid characters.
 * @retval URL_ERROR_OUT_OF_MEMORY -13
 *         The library was unable to allocate memory to create the
 *         the HRM_URL_T contents.
 * @retval URL_ERROR_INTERNAL_ERROR -14
 *         Some unexpected error occurred parsing the URL.
 */
int hrm_url_parse(const char *url_string, HRM_URL_T *url) {
    const char *substring;		/* where we are in the parse */
    int rc;			/* return code from helper functions */
    
  try {
    if(url == NULL) {
		return URL_ERROR_NULL_URL;
    }

    url->protocol_type = URL_PROTOCOL_UNKNOWN;
    url->protocol = NULL;
    url->host = NULL;
    url->port = 0;
    url->user = NULL;
    url->password = NULL;
    url->url_path = NULL;
    url->url_specific_part = NULL;
    url->dn = NULL;
    url->attributes = NULL;
    url->scope = NULL;
    url->filter = NULL;

    if(url_string == NULL) {
		return URL_ERROR_NULL_STRING;
    }
    
    substring = url_string;

    rc = url_get_protocol(&substring, &(url->protocol), &(url->protocol_type));
    if (rc != true) {
		goto parse_error;
    }

    if(strncmp(substring, "://", 3) != 0 &&
       url->protocol_type != URL_PROTOCOL_FILE) {
		rc = URL_ERROR_BAD_PROTOCOL;
		goto parse_error;
    }
    else if(url->protocol_type == URL_PROTOCOL_FILE) {
		substring++;
    }
    else {
		substring+=3;
    }
    switch(url->protocol_type) {
	    case URL_PROTOCOL_FTP:
	    case URL_PROTOCOL_GSIFTP:
			/* optional part of an ftp protocol, password is
			   only set if user is set
			*/
			rc = url_get_user_password(&substring, &(url->user), &(url->password));
			if(rc != true) {
			    goto parse_error;
			}
	    case URL_PROTOCOL_HTTP:
	    case URL_PROTOCOL_HTTPS:
	    case URL_PROTOCOL_SRM:
	    case URL_PROTOCOL_HRM:
			/* port not http or ftp */
			rc=url_get_host_port(&substring, &(url->host), &(url->port));
			if(rc != true) {
			    rc = URL_ERROR_BAD_PORT;
			    goto parse_error;
			}

			rc=url_get_path(&substring, &(url->url_path));
			if(rc != true) {
			    goto parse_error;
			}
			break;

	    case URL_PROTOCOL_LDAP:
			rc = url_get_host_port(&substring, &(url->host), &(url->port));
			if(rc != true) {
			    goto parse_error;
			}
			if(*substring != '/') {
			    rc = URL_ERROR_BAD_DN;
			    goto parse_error;
			}
			else {
			    substring++;
			}
			rc = url_get_ldap_specific(&substring,
					  &(url->dn), &(url->attributes),
					  &(url->scope), &(url->filter));
			if(rc != true) {
			    goto parse_error;
			}
			break;
   		 case URL_PROTOCOL_FILE:
			rc = url_get_file_specific(&substring, &(url->host),
					   &(url->url_path));
			if(rc != true) {
			    goto parse_error;
			}
			break;
   		 default:
			rc = url_get_host_port(&substring,
				      &(url->host), &(url->port));
			if(rc == URL_ERROR_INTERNAL_ERROR) {
			    goto parse_error;
			}
			if(rc == true) {
			    rc = url_get_path(&substring, &(url->url_path));
			    if(rc == URL_ERROR_INTERNAL_ERROR) {
					goto parse_error;
			    }
			    if(rc == true) {
					break;
			    }
			}
			rc = url_get_substring(substring,
				      &(url->url_specific_part), strlen(substring));
			if(rc != true) {
			    goto parse_error;
			}
   		 }
    return rc;
  }
  catch (...) {
		return URL_ERROR_INTERNAL_ERROR;
  }

parse_error:
    hrm_url_destroy(url);
    return rc;
}
/* hrm_url_parse() */

/*
 * Destroy a HRM_URL_T structure.
 * @ingroup url
 *
 * This function frees all memory associated with a
 * HRM_URL_T structure.
 *
 * @param url
 *        The url structure to destroy
 *
 * @retval true
 *         The URL was successfully destroyed.
 */
int hrm_url_destroy(HRM_URL_T *url) {
  try {
    if(url == NULL) {
		return URL_ERROR_NULL_URL;
    }
    if(url->protocol != NULL) {
		free(url->protocol);
		url->protocol=NULL;
    }
    if(url->user != NULL) {
		free(url->user);
		url->user=NULL;
    }
    if(url->password != NULL) {
		free(url->password);
		url->password = NULL;
    }
    if(url->host != NULL) {
		free(url->host);
		url->host = NULL;
    }
    if(url->url_path != NULL) {
		free(url->url_path);
		url->url_path = NULL;
    }
    if(url->dn != NULL) {
		free(url->dn);
		url->dn = NULL;
    }
    if(url->attributes != NULL) {
		free(url->attributes);
		url->attributes = NULL;
    }
    if(url->scope != NULL) {
		free(url->scope);
		url->scope = NULL;
    }
    if(url->filter != NULL) {
		free(url->filter);
		url->filter = NULL;
    }
    if(url->url_specific_part != NULL) {
		free(url->url_specific_part);
		url->url_specific_part = NULL;
    }
  }
  catch (...) {
  }
  return true;
}

/******************************************************************************
Function: url_get_substring()
Description: Copy a substring of the src string
Parameters:
Returns: true on success, URL_ERROR on ERROR
******************************************************************************/
static int url_get_substring (const char *src, char **destp, int nulpos) {
    int i; int j;
    
  try {
    *(destp) = (char*) malloc (sizeof(char)*(nulpos+1));
    if(*destp == NULL) {
		return URL_ERROR_NULL_STRING;
    }
    
    for(i = 0, j = 0; i < nulpos; i++, j++) {
		if(src[i] == '%' && i + 2 < nulpos) {
		    if(isxdigit(src[i+1]) && isxdigit(src[i+2])) {
				char hexstring[3];

				hexstring[0] = src[i+1];
				hexstring[1] = src[i+2];
				hexstring[2] = '\0';

				(*(destp))[j] = (char) (int) strtol(hexstring, NULL, 16);
				i += 2;
				continue;
		    }
		}
		(*(destp))[j] = (src)[i];
    }

    (*(destp))[j] = '\0';
  }
  catch (...) {
		return URL_ERROR_NULL_STRING;
  }
    return true;
}

/******************************************************************************
Function: url_get_protocol()

Description:
  Copy the protocol type of the url in the second argument

Parameters: (stringp in) (protocol out)

Returns: true on success, URL_ERROR_INTERNAL_ERROR on ERROR
******************************************************************************/
static int url_get_protocol(const char **stringp, 
		      char **protocol, url_protocol_t *protocol_type) {
  int pos = 0;

 try {
  /* scan stringp for a URL protocol */
  if(stringp == NULL) {
      return URL_ERROR_BAD_PROTOCOL;
  }
  if(*stringp == NULL) {
      return URL_ERROR_BAD_PROTOCOL;
  }
  if(protocol == NULL || protocol_type == NULL) {
      return URL_ERROR_INTERNAL_ERROR;
  }

  while(islower((*stringp)[pos]) || isdigit((*stringp)[pos]) ||
		(*stringp)[pos] == '+' || (*stringp)[pos] == '-' ||
		(*stringp)[pos] == '.') {
      pos++;
  }

  if((*stringp)[pos] != ':') {
      return URL_ERROR_BAD_PROTOCOL;
  }

  if(pos == 0) {
      return URL_ERROR_BAD_PROTOCOL;
  }

  if(url_get_substring(*stringp, protocol, pos) != true) {
      return URL_ERROR_INTERNAL_ERROR;
  }

  (*stringp) += pos++;

  if(strcmp(*protocol, "ftp") == 0) {
      *protocol_type=URL_PROTOCOL_FTP;
  }
  else if(strcmp(*protocol, "gsiftp") == 0) {
      *protocol_type=URL_PROTOCOL_GSIFTP;
  }
  else if(strcmp(*protocol, "http") == 0) {
      *protocol_type=URL_PROTOCOL_HTTP;
  }
  else if(strcmp(*protocol, "https") == 0) {
      *protocol_type=URL_PROTOCOL_HTTPS;
  }
  else if(strcmp(*protocol, "srm") == 0) {
      *protocol_type=URL_PROTOCOL_SRM;
  }
  else if(strcmp(*protocol, "hrm") == 0) {
      *protocol_type=URL_PROTOCOL_HRM;
  }
  else if(strcmp(*protocol, "ldap") == 0) {
      *protocol_type=URL_PROTOCOL_LDAP;
  }
  else if(strcmp(*protocol, "file") == 0) {
      *protocol_type=URL_PROTOCOL_FILE;
  }
  else {
      *protocol_type=URL_PROTOCOL_UNKNOWN;
  }
 }
 catch (...) {
      *protocol_type=URL_PROTOCOL_UNKNOWN;
      return URL_ERROR_BAD_PROTOCOL;
 }
  return true;
}

/******************************************************************************
Function: url_get_user_password()

Description:
  Copy the optional user and optional password into the out variables

  Can return true even if neither user or password is found, because
  these are optional parts of the url.

Parameters: (stringp in) (user out) (password out)

Returns: true on success, URL_ERROR_INTERNAL_ERROR on ERROR
******************************************************************************/
static int
url_get_user_password(const char **stringp, char **user, char **password) {
    int pos = 0;
    int startpos = 0;
    int lastpos=0;
    int rc;

  try {
    /* scan stringp for [user[:password]@] */
    if(stringp == NULL) {
       return URL_ERROR_INTERNAL_ERROR;
    }
    if(*stringp == NULL) {
       return URL_ERROR_INTERNAL_ERROR;
    }
    if(user == NULL || password == NULL) {
       return URL_ERROR_INTERNAL_ERROR;
    }
    /* pre-scan for '@' so we don't get confused by ':' */
    do {
		lastpos=pos;
		while(isalnum((*stringp)[pos]) ||
		      url_issafe((*stringp)[pos]) ||
		      url_isextra((*stringp)[pos]) ||
		      (*stringp)[pos] == ';' ||
		      (*stringp)[pos] == '?' ||
		      (*stringp)[pos] == '&' ||
		      (*stringp)[pos] == '=' ||
		      (*stringp)[pos] == ':') {
		    pos++;
		}
		if((*stringp)[pos] == '\0') {
		    /* didn't find anything, but it's optional, anyway */
		    return true;
		}
		if((*stringp)[pos] == '%') {
		    pos++;
		    if(isxdigit((*stringp)[pos])) {
				pos++;
				if(isxdigit((*stringp)[pos])) {
				    pos++;
				}
				else {
				    return URL_ERROR_BAD_USER;
				}
		    }
		    else {
				return URL_ERROR_BAD_USER;
		    }
		}
    } while((*stringp)[pos] != '@' && pos != lastpos);

    if((*stringp)[pos] != '@') {
		return true;
    }

    pos = 0;

    do {
		lastpos=pos;
		while(isalnum((*stringp)[pos]) ||
		      url_issafe((*stringp)[pos]) ||
		      url_isextra((*stringp)[pos]) ||
		      (*stringp)[pos] == ';' ||
		      (*stringp)[pos] == '?' ||
		      (*stringp)[pos] == '&' ||
		      (*stringp)[pos] == '=') {
		    pos++;
		}
		if((*stringp)[pos] == '\0') {
		    /* didn't find anything, but it's optional, anyway */
		    return true;
		}
		if((*stringp)[pos] == '%') {
		    pos++;
		    if(isxdigit((*stringp)[pos])) {
				pos++;
				if(isxdigit((*stringp)[pos])) {
				    pos++;
				}
				else {
				    return URL_ERROR_BAD_USER;
				}
		    }
		    else {
				return URL_ERROR_BAD_USER;
		    }
		}
    } while((*stringp)[pos] != '@' && (*stringp)[pos] != ':' && pos != lastpos);
    
    if(pos == startpos) {
		return URL_ERROR_BAD_USER;
    }

    if((*stringp)[pos] == '@') {
		rc = url_get_substring(*stringp, user, pos);
		(*stringp) += pos+1;
		return rc;
    }
    else if((*stringp)[pos]==':') {
		rc = url_get_substring(*stringp, user, pos);
	
		if(rc != true) {
		    return rc;
		}
		(*stringp) += pos+1;	/* skip ':' */
		pos=0;
		startpos = pos;

		do {
		    lastpos=pos;
		    if(isalnum((*stringp)[pos]) ||
		       url_issafe((*stringp)[pos]) ||
		       url_isextra((*stringp)[pos]) ||
		       (*stringp)[pos] == ';' ||
		       (*stringp)[pos] == '?' ||
		       (*stringp)[pos] == '&' ||
		       (*stringp)[pos] == '=') {
				pos++;
		    }

		    if((*stringp)[pos] == '\0') {
				return URL_ERROR_BAD_PASSWORD;
		    }
		    else if((*stringp)[pos] == '%') {
				pos++;
				if(isxdigit((*stringp)[pos])) {
				    pos++;
				    if(isxdigit((*stringp)[pos])) {
						pos++;
				    }
				    else {
						return URL_ERROR_BAD_PASSWORD;
				    }
				}
				else {
				    return URL_ERROR_BAD_PASSWORD;
				}
		    }
		} while((*stringp)[pos] != '@' && pos != lastpos);
		if(pos == startpos) {
		    return URL_ERROR_BAD_PASSWORD;
		}
		if((*stringp)[pos] == '@') {
	    	rc = url_get_substring(*stringp, password, pos);
		    (*stringp) += pos+1;
		    return rc;
		}
    }
    else {
		return true;
    }
  }
  catch (...) {
       return URL_ERROR_INTERNAL_ERROR;
  }
    return true;
}


/******************************************************************************
Function: url_get_host_port()

Description: look for a host:port in the specified string.
             NOTE: Does not ensure that the hostname is a valid RFC 1738

Parameters: 

Returns: true if in the character class, false otherwise
******************************************************************************/
static int
url_get_host_port(const char **stringp, char **host, unsigned short *port) {
    int pos = 0;
    int startpos = 0;
    int rc;

  try {
    if(stringp == NULL || host == NULL || port == NULL) {
        return URL_ERROR_INTERNAL_ERROR;
    }
    if(*stringp == NULL) {
        return URL_ERROR_INTERNAL_ERROR;
    }

    *port = 0;

    while (isalnum((*stringp)[pos]) || 
		(*stringp)[pos] == '-' ||
		(*stringp)[pos] == '.') {
		pos++;
    } 

    if(pos == startpos) {
		return URL_ERROR_BAD_HOST;
    }
    
    if((*stringp)[pos] == ':' || (*stringp)[pos] == '/') {
		char *tmp;
		rc = url_get_substring(*stringp, host, pos);
		(*stringp) += pos;
		if(rc != true) {
		    return rc;
		}
	
		pos = 0;

		if((*stringp)[pos] == ':') {
		    (*stringp)++;
		    while(isdigit((*stringp)[pos])) {
				pos++;
		    }
		    if(pos == 0) {
				return URL_ERROR_BAD_PORT;
		    }
	    
		    rc = url_get_substring(*stringp, &tmp, pos);
		    (*stringp) += pos++;
		    if(rc != true) {
				return rc;
		    }
		    else {
				*port = (unsigned short) atoi(tmp);
				free(tmp);
				return true;
		    }
		}
		else {
		    return true;
		}
    }
    else if((*stringp)[pos] == '\0') {
		rc = url_get_substring(*stringp, host, pos);
		(*stringp) += pos;
		return rc;
    }
    else {
		return URL_ERROR_BAD_HOST;
    }
  }
  catch (...) {
        return URL_ERROR_INTERNAL_ERROR;
  }
}

/******************************************************************************
Function: url_get_ldap_specific()

Description: Look for a properly formatted ldap protocol-specific information:
             namely <dn>?<attributes>?<scope>?<filter>
	     with all of the bits in <>'s legal URL characters

Parameters: 

Returns: true if the parsing turns out OK, URL_ERROR_*
         otherwise
******************************************************************************/
static int
url_get_ldap_specific(const char **stringp,
					char **dn,
					char **attributes,
					char **scope,
					char **filter) {
    int pos = 0;
    int lastpos;
    char ***which[4];
    int errs[4];
    int i;
    int rc;
    
  try {
    which[0] = &dn;
    which[1] = &attributes;
    which[2] = &scope;
    which[3] = &filter;
    errs[0] = URL_ERROR_BAD_DN;
    errs[1] = URL_ERROR_BAD_ATTRIBUTES;
    errs[2] = URL_ERROR_BAD_SCOPE;
    errs[3] = URL_ERROR_BAD_FILTER;

    for(i = 0; i < 4; i++) {
		pos = 0;
		do {
		    lastpos = pos;
		    if(isalnum((*stringp)[pos]) ||
		       url_isextra((*stringp)[pos]) ||
		       url_issafe((*stringp)[pos]) ||
		       (*stringp)[pos] == '=') {
				pos++;
		    }
	    
		    if((*stringp)[pos] == '%') {
				pos++;
				if(isxdigit((*stringp)[pos])) {
				    pos++;
				    if(isxdigit((*stringp)[pos])) {
						pos++;
				    }
				    else {
						return errs[i];
					}
				}
				else {
				    return errs[i];
				}
		    }
		} while(pos != lastpos && (*stringp)[pos] != '?');

		if(pos == 0) {
		    return errs[i];
		}
		else {
		    if(((*stringp)[pos] == '?'  && i != 3) ||
		       ((*stringp)[pos] == '\0' && i == 3)) {
				rc = url_get_substring(*stringp, *(which[i]), pos);
				if(rc != true) {
				    return rc;
				}
				(*stringp) += pos+1;
		    }
		    else {
				return errs[i];
		    }
		}	
    }
  }
  catch (...) {
    return URL_ERROR_BAD_DN;
  }
    return true;
}
	
/******************************************************************************
Function: url_get_file_specific()

Description: Look for properly formatted file protocol-specific information:
		/some/path
		//hostname/some/path
		(Note: only the second form is valid in the RFC definition
		of file URLS; however, the first and is used in common
		practice

Parameters: 

Returns: true if the parsing turns out OK, URL_ERROR_*
         otherwise
******************************************************************************/
static int url_get_file_specific(const char **stringp, char ** host, char ** path) {
    int    rc;
    size_t pos = 0;

  try {
    while((*stringp)[pos] == '/') {
		pos++;
    }

    if(pos == 0) {
		return URL_ERROR_BAD_PATH;
    }
    if(pos == 2) {
		(*stringp) += pos;

		pos = 0;
		/* Parse host name */
		while(isalnum((*stringp)[pos]) ||
		      (*stringp)[pos] == '-' ||
		      (*stringp)[pos] == '.') {
		    pos++;
		} 

		if((*stringp)[pos] == '/' && pos != 0) {
		    rc = url_get_substring(*stringp, host, pos);
		    (*stringp) += pos;
		    if(rc != true) {
				return rc;
		    }
		}
		pos = 0;
    }
    /* We've consumed the host name, now consume any leading /'s,
     * except for the last one */
    while((*stringp)[pos] == '/') {
		pos++;
    }

    if(pos > 1) {
		(*stringp) += (pos - 1);
    }
    pos = 0;

    if((*stringp)[pos] != '/') {
        rc = URL_ERROR_BAD_PATH;
    }
    else {
        rc = url_get_path(stringp,
			          path);
    }
    return rc;
  }
  catch (...) {
		return URL_ERROR_BAD_PATH;
  }
}

/******************************************************************************
Function: url_get_path()
Description: look for a path in the specified string.
Returns: true if in the character class, false otherwise
******************************************************************************/
static int url_get_path(const char **stringp, char **url_path) {
    int rc;
    size_t pos = 0;
    size_t lastpos;
    
  try {
    do {
		lastpos = pos;
		if(isalnum((*stringp)[pos]) ||
		   url_issafe((*stringp)[pos]) ||
		   url_isextra((*stringp)[pos]) ||
		   url_isprotocol_special((*stringp)[pos]) ||
		   (*stringp)[pos] == '~' || /* incorrect, but de facto */
		   (*stringp)[pos] == '/') {
		    pos++;
		}

		if((*stringp)[pos] == '%') {
		    pos++;
		    if(isxdigit((*stringp)[pos])) {
				pos++;
				if(isxdigit((*stringp)[pos])) {
				    pos++;
				}
				else {
//				    return URL_ERROR_BAD_PATH;
				}
		    }
		    else {
//				return URL_ERROR_BAD_PATH;
		    }
		}
    } while((*stringp)[pos] != '\0' && lastpos != pos);

    if(pos == 0) {
		return true;
    }
    if(pos != strlen(*stringp)) {
		return URL_ERROR_BAD_PATH;
    }

    /* reduce /~ to ~ */

    if(pos > 1 && **stringp == '/' && *(*stringp + 1) == '~') {
		*stringp = *stringp + 1;
    }

    rc = url_get_substring(*stringp, url_path, pos);

    #if defined(TARGET_ARCH_CYGWIN) 
	{
	/* 
       Problem arises for a path //home/larsson/foo, which is interpreted
	   as file /larsson/foo on host home. This will reportedly go away in
	   future Cygwin releases (current is B20.1). The problem is only for
	   a starting // -- the path /home//larsson/foo works fine -- so it
	   is enough to just fix the first instance of //
	*/
	
        char *  q = *url_path;
        char *  p = strstr(q,"//");
		int     pos2, i;
		int     len = strlen(q);
		if (p) { 
		    pos2 = (int)(p - q);
		    for (i=pos2+1; i<len; i++)
			q[i] = q[i+1];
		}
    }
    #endif /* defined(TARGET_ARCH_CYGWIN) */

    return rc;
  }
  catch (...) {
    return URL_ERROR_BAD_PATH;
  }
}

/******************************************************************************
Function: url_issafe()
Description: predicate returns true if the specified character is in the
             'safe' character class from RFC 1738
Returns: true if in the character class, false otherwise
******************************************************************************/
static int url_issafe(char x) {
    if(x == '$' || x == '-' || x == '_' || x == '.' || x == '+') {
		return true;
    }
    else {
		return false;
    }
}

/******************************************************************************
Function: url_isextra()
Description: predicate returns true if the specified character is in the
             'extra' character class from RFC 1738
Returns: true if in the character class, false otherwise
******************************************************************************/
static int url_isextra(char x) {

    if(x == '!' || x == '*' || x == '\'' || x == '(' ||
       x == ')' || x == ',') {
		return true;
    }
    else {
		return false;
    }
}

/******************************************************************************
Function: url_isprotocol_special()
Description: predicate returns true if the specified character is in the
             'protocol special' character class from RFC 1738
Returns: true if in the character class, false otherwise
******************************************************************************/
static int url_isprotocol_special(char x) {
    if(x == ';' || x == '/' || x == '?' || x == ':' ||
       x == '@' || x == '=' || x == '&') {
        return true;
    }
    else {
        return false;
    }
}

/**
 * Get the protocol of an URL.
 * @ingroup url
 *
 * This function determines the protocol type of the url string, and populates
 * the variable pointed to by second parameter with that value.
 * This performs a less expensive parsing than hrm_url_parse() and is
 * suitable for applications which need only to choose a handler based on
 * the URL protocol.
 *
 * @param url_string
 *        The string containing the URL.
 * @param protocol_type
 *        A pointer to a url_protocol_t which will be set to
 *        the protocol.
 *
 * @retval true
 *         The URL protocol was recogized, and protocol_type has been updated.
 * @retval URL_ERROR_BAD_PROTOCOL
 *         The URL protocol was not recogized.
 */
int url_get_protocol(const char *url_string, url_protocol_t *protocol_type) {
  try {
    if(strncmp(url_string, "ftp:", 4) == 0) {
		*protocol_type = URL_PROTOCOL_FTP;
    }
    else if(strncmp(url_string, "gsiftp:", 7) == 0) {
		*protocol_type = URL_PROTOCOL_GSIFTP;
    }
    else if(strncmp(url_string, "http:", 5) == 0) {
		*protocol_type = URL_PROTOCOL_HTTP;
    }
    else if(strncmp(url_string, "https:", 5) == 0) {
		*protocol_type = URL_PROTOCOL_HTTPS;
    }
    else if(strncmp(url_string, "srm:", 4) == 0) {
		*protocol_type = URL_PROTOCOL_SRM;
    }
    else if(strncmp(url_string, "hrm:", 4) == 0) {
		*protocol_type = URL_PROTOCOL_HRM;
    }
    else if(strncmp(url_string, "ldap:", 5) == 0) {
		*protocol_type = URL_PROTOCOL_LDAP;
    }
    else if(strncmp(url_string, "file:", 5) == 0) {
		*protocol_type = URL_PROTOCOL_FILE;
    }
    else if(strncmp(url_string, "gsiftp:", 7) == 0) {
		*protocol_type = URL_PROTOCOL_GSIFTP;
    }
    else {
		*protocol_type = URL_PROTOCOL_UNKNOWN;
        return URL_ERROR_BAD_PROTOCOL;
    }
    return true;
  }
  catch (...) {
		*protocol_type = URL_PROTOCOL_UNKNOWN;
        return URL_ERROR_BAD_PROTOCOL;
  }
}
/* url_get_protocol() */
