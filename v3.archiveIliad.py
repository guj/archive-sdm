import time
import random
import sys
import os

from multiprocessing import Process, Queue, current_process, freeze_support, Pool


from ctypes import cdll
basePath = os.path.dirname(os.path.realpath(__file__))
lib = cdll.LoadLibrary(basePath+'/libsdm.so')

configFilename = basePath+'/mss.rc'
logFilename = basePath+'/msslogs/mss.log'
if not os.path.exists(os.path.dirname(logFilename)):
    os.makedirs(os.path.dirname(logFilename))

with open(configFilename, "w") as f:
    f.write("mss*HSI=hsi\n");
    f.write("mss*MSSHostName=archive.nersc.gov\n")
    f.write("mss*HPSSHSIHostName=archive.nersc.gov\n")
    f.write("mss*MSSLOCATION=MSS_LBNL\n")
    f.write("mss*MSSSECURITY=NONE\n")
    f.write("mss*MSSMaximumAllowed=15\n")
    f.write("mss*EnableDetailedLog=true\n")
    f.write("mss*EnableLogging=true\n")
    f.write("mss*MSSLogFile="+logFilename+"\n");

f.close()

#
# functions available from C library
#
class sdm(object):
    def __init__(self):
        #self.obj = lib.create_new(basePath+'/sdmmss.rc')
        self.obj = lib.create_new(configFilename)

    def put(self, src, tgt):
        return lib.putFile(self.obj, src, tgt)

    def get(self, src, tgt):
        return lib.getFile(self.obj, src, tgt)

    def mkdir(self, path):
        return lib.mkdir(self.obj, path)

    def done(self):
        lib.deleteObj(self.obj);



def report(progress):
    """ prints out progress for users """
    print "[ ", progress, "] "

#
# archive only the files with given extensions from local to nersc (i.e. "remote") 
#
def oneDirArchiveWithFilter(path, extensions, remote):
    """
    archives to from given "path" to "remote" nersc hpss location
    only files that match "extensions" are archived    
    """
    tasks = []
    f = sdm();

    import os, sys

    # Open a file
    dirs = os.listdir( path )

    # This would print all the files and directories
    for entry in dirs:
        localpath=path+"/"+entry
        remoteLoc=remote+"/"+entry
        if os.path.isdir(localpath):
            #tasks.append((mkdirnersc, (f, remoteLoc)));
            lenWas = len(tasks);
            tasks.extend(oneDirArchiveWithFilter(localpath, extensions, remoteLoc));
            lenNow = len(tasks);
            if (lenNow > lenWas):
                tasks.append((mkdirnersc, (f, remoteLoc)));
        else:
            if (len(extensions) == 0):
                tasks.append((putToNersc, (f, localpath, remoteLoc)));
            else:
                if any([entry.endswith(ext) for ext in extensions]):
                    tasks.append((putToNersc, (f, localpath, remoteLoc)));

    return tasks;
        

def worker(input, output):
    for func, args in iter(input.get, 'STOP'):
        result = doTransfer(func, args)
        output.put(result)

#
# Function used to doTransfer result
#

def doTransfer(func, args):
    result = func(*args)
    if (result):
        #return '%s is successful.' % args
        #return '%s archived successfully' % (args)
        return 'success'
    else:
        #return '%s is failed.' % args
        return 'failed'
    #return '%s says that %s%s = %s' % \
    #    (current_process().name, func.__name__, args, result)

#
# Functions referenced by tasks
#
def readFromNersc(f, nerscfile, localfile):
    return f.get(nerscfile, localfile);

def putToNersc(f, localfile, nerscfile):
    return f.put(localfile, nerscfile);

def mkdirnersc(f, remoteloc):
    return f.mkdir(remoteloc);

#def testDirs(dirName, num_processes):

def runTask(TASKS1, num_processes):
    # Create queues
    task_queue = Queue()
    done_queue = Queue()

    # Submit tasks
    for task in TASKS1:
        task_queue.put(task)

    # Start worker processes
    for i in range(num_processes):
        Process(target=worker, args=(task_queue, done_queue)).start()

    # Get and print results
    for i in range(len(TASKS1)):
        print i,'th file archived? ', done_queue.get()

    # Add more tasks using `put()`

    # Tell child processes to stop
    for i in range(num_processes):
        task_queue.put('STOP')


    

def iliadCaseCollection(iliadDirectory,extension = None):
    """ using function from climate library to tar files in climate directory"""

    report("generating  iliad tar file dictionary")
    import outputFileCollection as ofc
    #Set the default value for extension if needed
    if extension is None:
        extension = 'tar'

    #Get the list of output archive files and their contents
    archiveFileDict = ofc.outputFileCollection(iliadDirectory + '/run')

    #Check that we have archive files
    if len(archiveFileDict) == 0:
        raise RuntimeError,"No ILIAD netCDF files were found in {}/run; is {} an ILIAD run directory?".format(iliadDirectory,iliadBaseDirectory)

    #Walk through the output file directory and filter out any files matching {iliadBaseDirectory}/run/iliad*.nc
    dirWalk = os.walk(iliadDirectory)
    nonRunFiles = ["{}/{}".format(wtuple[0],ffile) for wtuple in dirWalk for ffile in wtuple[2] if not (wtuple[0].split('/')[-1] == 'run' and ffile[:5] == 'iliad' and ffile[-3:] == '.nc')  ]

    #Get the base name of the ILIAD run directory
    iliadBaseDirectory = os.path.dirname(iliadDirectory).split('/')[-1]

    report("preparing to tar files in "+iliadBaseDirectory);

    #Set the name of the tarfile containing the case directory files
    caseTarFile = "{}.casedirectory.{}".format(iliadBaseDirectory,extension)
    archiveFileDict[caseTarFile] = nonRunFiles

    report("  number of tar files to create:"+str(len(archiveFileDict)))
    #Return the file dict
    return archiveFileDict

def createArchive(archiveName):
  """Create a tar of all the items in testCollection[archiveName].
  This function is suitable for use with Pool.map()
  """

  import tarfile
  report("  creating tar file: "+archiveName)
  #Add the tar scratch path to the archive name
  fullArchivePath = result.tar_path + archiveName

  #open the archive
  with tarfile.open(fullArchivePath, 'w') as fout:
      #Write all files to the archive
      #This uses a list comprehension to reduce loop overhead
      [fout.add(ffile) for ffile in testCollection[archiveName] ]
  #print "done with ", archiveName
  report("       tar created: "+archiveName);


def archiveAndTxf(archiveName):
  """Create a tar of all the items in testCollection[archiveName].
  Then transfer this tar file to hpss
  This function is suitable for use with Pool.map()
  """

  createArchive(archiveName);
  putResult =  putToNersc(sdm(), result.tar_path + archiveName,  result.hpss_path+archiveName)
  print "putResult=", putResult
  if putResult:
      print "removing tar file:", result.tar_path+archiveName
      os.remove(result.tar_path + archiveName)



def archiveTxfSeperately():
    import tarfile
    #testDirectory = '/scratch3/scratchdirs/taobrien/cascade/iliad/ne16np4/iliad-ne16np4-2005_01_01_00-2005_05_01_00/'
    testCollection = iliadCaseCollection(result.data_path)
    
    tarPool = Pool(5)
    tarPool.map(createArchive,sorted(testCollection))

    report("  archiving starts ")
    putTasks = oneDirArchiveWithFilter(result.tar_path, included_extensions, result.hpss_path);    

    # limit is 15 for hsi
    runTask(putTasks, 15); 

def archiveTxfInOneThread():
    import tarfile
    #testDirectory = '/scratch3/scratchdirs/taobrien/cascade/iliad/ne16np4/iliad-ne16np4-2005_01_01_00-2005_05_01_00/'
    
    tarPool = Pool(15)
    tarPool.map(archiveAndTxf,sorted(testCollection))
    
#
# main
#
if __name__ == '__main__':
    freeze_support()

    import argparse

    parser = argparse.ArgumentParser(description='Archive to hpss for Travis.')
    parser.add_argument(action="store", dest='data_path');
    parser.add_argument(action="store", dest='tar_path');
    parser.add_argument(action="store", dest='hpss_path');

    result = parser.parse_args();    

    #import tempfile
    #with tempfile.TemporaryDirectory() as tar_path:

    #tar_path =  tempfile.mkdtemp()+'/'
    #print tar_path

    #included_extensions = sys.argv[4:];
    included_extensions = ['.tar']
    
    #archiveTxfSeperately;

    testCollection = iliadCaseCollection(result.data_path)
    archiveTxfInOneThread();





