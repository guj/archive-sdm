// sdmmss_test.cpp
// Alex Sim <ASim@lbl.gov>
// Lawrence Berkeley National Laboratory
// 2015

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include "sdmmss_config.h"
#include "sdmmss_fetcher.h"
#include "sdmmss_fetcher_hsi.h"

//Config config("mss", "./sdmmss.rc", false);

//
//
//
class FileFetcherNersc : public FileFetcherHSI { 
public:
  FileFetcherNersc(Config* config, int dbglvl);
  ~FileFetcherNersc();

  bool putFile(char*, char*);
  bool getFile(char*, char*, long long);
  long long listFile(char*);

  Config* _config;
};

FileFetcherNersc::FileFetcherNersc(Config* config, int dbglvl)
  :FileFetcherHSI(config, MSS_LBNL, dbglvl, NULL, NULL),
   _config(config)
{}


FileFetcherNersc::~FileFetcherNersc()
{
  delete _config;
}


bool FileFetcherNersc::putFile(char* localfile,
			       char* remotefile)
{
  FILE* f = fopen(localfile, "r");
  if (f == NULL) {
    std::cout<<"sdmss::error unable to open file "<<localfile<<std::endl;
    return false;
  }

  fclose(f);
  
  long long filesize = 0;

  try {
    mssFilePut(localfile, remotefile, filesize);
  } catch(char const* s) {
    std::cout << "GET test failed..." << std::endl;
    std::cerr << s << std::endl;
    return false;
  }

  return true;
}

bool FileFetcherNersc::getFile(char* remoteFile, char* localDest, long long filesize)
{
  try {
    mssFileGet(localDest, remoteFile, filesize);    
  } catch(char const* s) {
    std::cout << "GET test failed..." << std::endl;
    std::cerr << s << std::endl;
    return false;
  }

  return true;
}

long long FileFetcherNersc::listFile(char* filepath) 
{
  try {
    MSS_ACCESS_INFO_T target;
    target.type = MSS_NONE;
    MSS_PATH_SET_T* result =  srmls(filepath, &target, false, false);
    if (result->files.size() == 1) {
      return result->files[0].size;
    }
    return -1;
  } catch(char const* s) {
    std::cout << "LS file failed..." << std::endl;
    std::cerr << s << std::endl;
    return -1;
  }
}

extern "C" {
  FileFetcherNersc* create_new(char* rcPath, int dbglvl){ 
    Config* config = new Config("mss", rcPath, false); 
    return new FileFetcherNersc(config, dbglvl); 
 }

  bool putFile(FileFetcherNersc* n, char* localFile, char* remoteFile) { 
       return n->putFile(localFile, remoteFile);
  }

  bool getFile(FileFetcherNersc* n, char* remoteFile, char* localDest, long long size) {
       return n->getFile(remoteFile, localDest, size);
  }

  void deleteObj(FileFetcherNersc* n) {
    delete n;
  }

  long long listFile(FileFetcherNersc* n, char* filepath) {
    return n->listFile(filepath);
  }

  bool mkdir(FileFetcherNersc* n, char* path) {
    std::cout<<" mkdir "<<path<<std::endl;

    MSS_PATH_SET_T p;
    p.dir = path;
    MSS_ACCESS_INFO_T target;
    target.type = MSS_NONE;
    MSS_STATUS_CODE_T* result =  n->makeDirectory(&p, &target);

    if (result->code == MSS_REQUEST_FAILED) {
      delete result;
      return false;
    } 
    delete result;
    return true;
  }
}

int main(int argc, char *argv[]) {
  Config config("mss", "./sdmmss.rc", false);
  FileFetcherNersc* n = new FileFetcherNersc(&config, 100);

  /* // ok
  char* localFile = "/scratch1/scratchdirs/junmin/testme";
  char* remoteFile = "/home/j/junmin/test/testme";
  char* stagedFile = "/scratch1/scratchdirs/junmin/testme2";
  long long size = 0;

  bool putSucc = n->putFile(localFile, remoteFile);
  bool getSucc = n->getFile(remoteFile, stagedFile, size);

  std::cout <<"putSucc? "<<putSucc<<std::endl;
  std::cout <<"getSucc? "<<getSucc<<std::endl;
  */


  char* subdir="/home/j/junmin/s3d/";
  /*
  MSS_PATH_SET_T p;
  p.dir = subdir;
  MSS_ACCESS_INFO_T target;
  target.type = MSS_NONE;
  MSS_STATUS_CODE_T* code = n->makeDirectory(&p, &target);
  std::cout<<" code = "<<code->code<<std::endl;
  delete code;
  */
  
  long long filesize = listFile(n, "/home/j/junmin/test/testme"); 
  std::cout << "found size of file: "<<filesize <<std::endl;
  //listFile(n, "/home/j/junmin/test/testmenoway"); // no such file
  // listFile(n, "/home/j/jared/"); // no permission
  mkdir(n, subdir);
  putFile(n, "/scratch1/scratchdirs/junmin/bl_flame.3.4536E-03.field.bp", "/home/j/junmin/s3d/bl_flame.3.4536E-03.field.bp");
}



