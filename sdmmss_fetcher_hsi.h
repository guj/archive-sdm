// HRM_FETCHERHSI.H 
// Alex Sim <ASim@lbl.gov>
// Lawrence Berkeley National Laboratory
// 1999-2007

// This assumes HSI with default unixtab

#ifndef hrm_fetcher_hsi
#define hrm_fetcher_hsi

#include <iostream>  
#include <stdio.h>          
#include <stdlib.h>      
#include "sdmmss_fetcher.h"

class FileFetcherHSI: public FileFetcher { 
public: 
        FileFetcherHSI(Config* config, mss_location_t mssloc=MSS_LBNL, int dlevel=0, 
					char* hpsslogin=NULL, char* hpsspasswd=NULL);
	~FileFetcherHSI();

// AS2015 OLDHRM
//	void setFileInfo(File* filePtr);

	mss_message_t checkMSS();
	void handleError(mss_message_t pftpmssg, char* sid, char* localfilename, char* remotefilename, char* ftp, char* ftptransferlog, bool nothingTxfed);
	bool updatePftpMsg(char* localfilename, char* ftptransferlog, mss_message_t& pftpmssg);
	bool checkLocalTarget(char* localfilename, char* sid, long long filesize);
//	void mssFileGet(void *);
	void mssFileGet(char* localFilePath, char* remoteFilePath, long long remoteFileSize, MSS_ACCESS_TYPE_T fileAccessType=MSS_NONE);
//	void mssFilePut(void *);
	void mssFilePut(char* localFilePath, char* remoteFilePath, long long localFileSize, MSS_ACCESS_TYPE_T fileAccessType=MSS_NONE);
	mss_message_t getMSSError(const char* logfile);
	int handleMSSError(int MSSerror);
	double getMSSTransferTime(const char* logfile);
	double getMSSTransferBytes(char* logfile);
	MSS_PATH_SET_T* srmls(const char* path, MSS_ACCESS_INFO_T* source,
						   bool recursive=false, bool toplevel=false);
	MSS_STATUS_CODE_T* makeDirectory(const MSS_PATH_SET_T *dirs, const MSS_ACCESS_INFO_T *target);
	MSS_STATUS_CODE_T* removePath(const char* path, MSS_ACCESS_INFO_T target,
						   bool isdirectory=false);
	mss_message_t checkHSI();

protected: 
	mss_message_t getHSIErrorByValue(int errcode);
	mss_message_t getHSIError(char* logpath);
	mss_message_t mssListing(const char* rpath, MSS_ACCESS_INFO_T* source, char* logftpmssg);
	char* makeDirectoryRecursive(const MSS_PATH_SET_T *dirs, char* temp);
// AS2015 OLDHRM
//	void removeAndServeNextFile(File* filePtr, bool addback=false, bool tohead=true);

private:
	char *hsipath;
//	int debugLevel;
};  


#endif 



