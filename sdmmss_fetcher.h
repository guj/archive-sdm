// HRM_FETCHER.H 
// Alex Sim <ASim@lbl.gov>
// Lawrence Berkeley National Laboratory
// 1999-2003

#ifndef hrm_fetcher
#define hrm_fetcher

#include <iostream>  
#include <stdio.h>          
#include <stdlib.h>      
#include <vector>
#include "sdmmss_util.h"
#include "sdmmss_config.h"

enum mss_message {
	mss_connection_ok, 
	mss_transfer_done, 
	mss_command_successful,
	mss_file_exists, 
	mss_no_such_file, 
	mss_no_such_directory, 
	mss_no_access_permission, 
	mss_limit_reached, 
	mss_error, 
	mss_system_not_available, 
	mss_wrong_login, 
	mss_gss_failure, 
	mss_other_error
};
typedef mss_message mss_message_t;

enum mss_location {
	MSS_LBNL 	// archive.nersc.gov
//	BNL,    // hpss.rcf.bnl.gov
//	ORNL,	// hpss.ccs.ornl.gov
//	NCAR,	// dataportal.ucar.edu
//	SCP		// mainly for Super Computers access with SSH
};
typedef mss_location mss_location_t;

class FileFetcher { 
public: 
        FileFetcher(mss_location_t mssloc, Config* config,  int dlevel=0,
		    char* msslogin=NULL, char* msspasswd=NULL);
	~FileFetcher();

	void setProbabilitySameTape(int n);
	double getTransferOverhead();
	float getProbabilitySameTape();
	char* getHostName();
	char* getHostPort();
	void setTransferOverhead(double to);
	aOut* qout;
	char* getStringID();

	virtual mss_message_t checkMSS()=0;
			// checking MSS during initial time of TRM 
			// only if default login and default passwd is provided
// AS2015 OLDHRM
//	virtual void setFileInfo(File* filePtr)=0;
			// check the existence of the file in MSS 
			// and sets the correct file size and the tape ID

	//virtual void mssFileGet(void *)=0;
	virtual void mssFileGet(char* localFilePath, char* remoteFilePath, long long remoteFileSize, MSS_ACCESS_TYPE_T fileAccessType=MSS_NONE)=0;
			// getting the files
	//virtual void mssFilePut(void *)=0;
	virtual void mssFilePut(char* localFilePath, char* remoteFilePath, long long localFileSize, MSS_ACCESS_TYPE_T fileAccessType=MSS_NONE)=0;
			// archiving the file
	virtual mss_message_t getMSSError(const char* logfile) =0;
			// parsing the MSS error codes
	virtual int handleMSSError(int code)=0;
			// deprecated, but also handling some MSS error codes
	virtual double getMSSTransferTime(const char* logfile) =0;
			// calculating the MSS transfer time from the log file
	virtual double getMSSTransferBytes(char* logfile) =0;
			// find out the actual bytes from/to MSS from the log file
	virtual MSS_PATH_SET_T* srmls(const char* path, MSS_ACCESS_INFO_T* source,
				      bool recursive, bool toplevel) =0;
			// handlig MSS directory listing request
	virtual MSS_STATUS_CODE_T* makeDirectory(const MSS_PATH_SET_T *dirs, 
						 const MSS_ACCESS_INFO_T *target) =0;
			// handlig MSS directory creation request
	virtual MSS_STATUS_CODE_T* removePath(const char* path, 
					      MSS_ACCESS_INFO_T target,
					      bool isdirectory) =0;
			// handlig MSS directory creation request

protected: 
//	int	MEGABYTE = 1048576;
	int WAIT_TIME; // wait time in secs between mss connection retrials
	int LONG_WAIT_TIME; // // wait time in secs between mss connection retrials
	int max_num_trials;
	char *mssHostName;
	char *mssHostPort;
	char *mssLogin;
	char *mssPasswd;
	bool mssDefaultLogin;
	float probabilitySameTape;
	double transferOverhead;
	aOut* aout;
	pthread_mutex_t tO_mutex;	// locks threads when setting transferOverhead
	pthread_mutex_t pST_mutex;	// locks probabilitySameTape
	mss_location_t mssLocation;
	int debugLevel;

	Config* _config;
private:
	//int debugLevel;

};  


#endif 



