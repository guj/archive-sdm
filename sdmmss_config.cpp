//	File: hrm_config.h
//	Author: Alex Sim <ASim@lbl.gov>
//	Copyright 2001 the Regents of the University of California
//
//  Purpose: The implementation of class Config
//
//	Notes:
//		class Config handles reading of a configuration from a file.
//
//		Usage: Config config("hrm", file_name);
//
//		when used like this, it scans the file specified by file_name
//		for lines beginning with "hrm" OR "common" and parses them.
//
//		example config file:
//		common:HRMName=HPSSResourceManager
//		common:HRMRefFile=/tmp/HRM.ref
//		hrm:CacheDirectory=/tmp/hrmcache
//
//	Created: 111601
//	Last Modified: 111601

#include <list>			// list<>
#include <utility>		// pair<>, make_pair()

#include <cstring>		// strlen(), strcpy()
#include <iostream>	// cout, cerr
#include <limits.h>	// MAX_LINE
#include <syslimits.h>	// MAX_LINE

//#include <hrm_const.h>	// REASON etc.
//#include <hrm_util.h>	// strnewdup()

#include "sdmmss_config.h"	// class Config

Config::~Config()
{ 
	for(listpair::const_iterator it = m_entries.begin();
	it != m_entries.end(); ++it)
	{
			delete[] (*it).first;
			delete[] (*it).second;
	}

	if( m_prefix ) delete[] m_prefix;
}

Config& Config::operator = (const Config& rhs)
{
	if( &rhs == this ) return *this;
	m_entries = rhs.m_entries;
	if( m_prefix ) delete[] m_prefix;
	if( rhs.m_prefix ) m_prefix = strnewdup( rhs.m_prefix ); 
	else m_prefix = strnewdup("none");
	m_verbose = rhs.m_verbose;
	m_is_ok = rhs.m_is_ok;
	return *this;
}

const char* Config::operator [] (const char *name) const
{
	for(listpair::const_iterator it=m_entries.begin();
	it != m_entries.end(); ++it)
	{
		if( strcmp( (*it).first, name) == 0 )
		{
			return (*it).second;
		}
	}
	return 0;

	/*
	this is what the code would look like if we had a compiler that works with std::map<>
	if( ( std::map< std::string, std::string >::iterator it = m_entries.find( name ) ) != m_entries.end() )
		return 0;
	else
		return (*it).second;
	*/
}

void Config::readConfig(const char* fn)
{	

	FILE* config = fopen(fn ? fn : "./sdmmss.rc","r");
	if( !config )
	{
		std::cerr << "ERROR: Could not find configuration file \"" << (fn ? fn : "./sdmmss.rc") << "\" for read.\n" ;
		REASON;
		m_is_ok = false;
		return;
	}
	else
	{
		if( m_verbose ) std::cout << "Using configuration file \"" << (fn ? fn : "./sdmmss.rc") << "\"." << std::endl; 
		char line[MAX_LINE];
		char *name, *value;
		while( !feof(config) )
		{
			if( fgets(line,MAX_LINE,config) == 0 ) break;

			// '!' and '#' denotes comment
			if( !line[0] || line[0]=='\n' || line[0]=='\r' ||
					line[0]=='!' || line[0]=='#' ) continue; 

			line[ strlen(line) - 1 ] = 0; // get rid of the '\n'

			if (strncasecmp(m_prefix, "all", strlen(m_prefix) ) == 0 ||
				strncasecmp(m_prefix, line, strlen(m_prefix) ) == 0 ||
				strncasecmp("common", line, strlen("common") ) == 0 )
			{
				name = strchr(line,'*') + 1;
				value = strchr(line,'=') + 1;
				value[-1] = 0;
				addNameValuePair( name, value );
			}
			// else if( m_verbose ) cerr << "Config: Skipping: " << line ;
		}
		fclose(config);
		if( m_verbose ) printConfig();
		m_is_ok = true;
	}
}

void Config::printConfig() const
{
	std::cout << "**************** begin config ****************" << std::endl;
	for(listpair::const_iterator it = m_entries.begin();
	it != m_entries.end(); ++it)
	{
		std::cout << (*it).first << " = " << (*it).second << std::endl;
	}
	std::cout << "***************** end config *****************" << std::endl;
}

bool Config::is_true(const char* name) const
{
	char* value = NULL;
	if (getValue(name)) value=strnewdup( getValue(name) );
	else return false;
	if( strcasecmp(value,"(null)") == 0 ) { delete[] value; return false; }
	if( strlen(value) == 0 ) { delete value; return false; }
	if( strcasecmp(value,"true") == 0 ||
		strcasecmp(value,"yes") == 0 ||
		value[0] == '1' ||
		strcasecmp(value,"defined") == 0 ) { delete[] value; return true; }
	delete[] value;
	return false;
}

