// sdmmss_test.cpp
// Alex Sim <ASim@lbl.gov>
// Lawrence Berkeley National Laboratory
// 2015

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include "sdmmss_config.h"
#include "sdmmss_fetcher.h"
#include "sdmmss_fetcher_hsi.h"

const char usage[] = "sdmmss_test [-options ...]\n\n\
where options include:\n\
  -help               print this message\n\
  -conf path/file     path for configuration file (default ./sdmmss.rc) \n\
  -checkcache         enable checking disk cache availability (default false) \n\
  -debug              enable debugging (debug info displayed) (default true) \n\
  -diskpath path      data directory to bring in files  \n\
  -hsi path           enable HSI and its path (default hsi) \n\
  -logdetail          enable detailed logging (default false) \n\
  -log path/file      enable logging to a file (default true and ./msslog) \n\
  -msshost hostnames  host names and port number (if any) for HPSS (default archive.nersc.gov) \n\
  -msslimit int       maximum number of HPSS connections allowed (default 15) \n\
  -overwrite          enable overwrite capability in archiving on HPSS (default false) \n\
  -passcheck          bypass checking for HPSS/HSI login (default false) \n\
  -size int           file size in bytes \n\
  -source path/file   local disk file path \n\
  -target path/file   remote HPSS file path \n\
  -v                  show version \n\n";


Config config("mss", 0, false);

int main(int argc, char *argv[]) {

	char hsi[1024]; strcpy(hsi, "hsi");
	char filesource[1024]; strcpy(filesource, "/tmp/test.txt");
	char filesourcesecond[1024]; strcpy(filesourcesecond, "/tmp/test2.txt");
	char filetarget[1024]; strcpy(filetarget, "/.../asim/2015archive.test.data");
	long long filesize = 0;
	char msshost[1024]; strcpy(msshost, "archive.nersc.gov");
	char output[1024]; strcpy(output, "./testoutput.txt");
	int nfiles = 0;	// nfiles+1 = number of files (average) to get from a tape
	bool haveHSI=true;
	int debugLevel = 10;
	mss_location_t msslocation=MSS_LBNL;

	config.addNameValuePair("HSI", "hsi");
	config.addNameValuePair("MSSHostName", "archive.nersc.gov");
	config.addNameValuePair("MSSLOCATION", "MSS_LBNL");
	config.addNameValuePair("MSSSECURITY", "NONE");
	config.addNameValuePair("MSSMaximumAllowed", "15");
	config.addNameValuePair("EnableDetailedLog", "true");
    config.addNameValuePair("EnableLogging", "true");
    config.addNameValuePair("MSSLogFile", "./mss.log");


   for (int i=1 ; i < argc; i++) {
        if (argv[i][0] != '-') {
            printf("%s : error, unknown argument %s\n -f pftp_path -w hpss_ls_path -h hpss_host\n",
                        argv[0],argv[i]);
            exit(1);
        }
        else {
            switch (argv[i][1]) {
				case 'c':
                    if (!strcmp(argv[i], "-conf")) {
                        if( config.is_ok() ) {
                            i++;
                        }
                        else {
                            config.readConfig( argv[++i] );
                        }
                    }
                    else if (!strcmp(argv[i], "-checkcache")) {
                        config.addNameValuePair("CheckCache", "true");
                    }
                    else {
                        fprintf(stderr,"%s [options]\n",argv[i]);
                        fprintf(stderr,"%s",usage);
                        exit(1);
                    }
                    break;
				case 'd':
                    if (!strcmp(argv[i], "-debug")) {
                        i++;
                        if ((i < argc) && (argv[i][0] != '-')) {
                            debugLevel = atoi(argv[i]);
                        } else {
                            i--;
                            debugLevel = 1;
                        }
                    }
                    else if (!strcmp(argv[i], "-diskpath")) {
                        config.addNameValuePair("DiskPath", argv[++i]);
                    }
                    else {
                        fprintf(stderr,"%s [options]\n",argv[i]);
                        fprintf(stderr,"%s",usage);
                        exit(1);
                    }
                    break;
                case 'h' :
                    if (!strcmp(argv[i], "-help")) {
                        fprintf(stderr,"%s [options]\n",argv[0]);
                        fprintf(stderr,"%s",usage);
                        exit(1);
                    }
                    else if (!strcmp(argv[i], "-hsi")) {
                        strcpy(hsi, argv[++i]);
                        haveHSI = true;
                        config.addNameValuePair("HSI", argv[++i]);
                    }
                    else {
                        fprintf(stderr,"%s [options]\n",argv[i]);
                        fprintf(stderr,"%s",usage);
                        exit(1);
                    }
                    break;
				case 'l':
                    if (!strcmp(argv[i], "-logdetail")) {
                        config.addNameValuePair("EnableDetailedLog", "true");
                    }
                    else if (!strcmp(argv[i], "-log")) {
                        config.addNameValuePair("EnableLogging", "true");
                        i++;
                        if ((i < argc) && (argv[i][0] != '-')) {
                            config.addNameValuePair("MSSLogFile", argv[i]);
                        }
                        else {
                            i--;
                        }
                    }
                    break;
                case 'm' :
					if (!strcmp(argv[i], "-msshost")) {
                        config.addNameValuePair("MSSHostName", argv[++i]);
						strcpy(msshost, argv[++i]);
                    }
                    else if (!strcmp(argv[i], "-msslimit")) {
                        config.addNameValuePair("MSSMaximumAllowed", argv[++i]);
                    }
                    else {
                        fprintf(stderr,"%s [options]\n",argv[i]);
                        fprintf(stderr,"%s",usage);
                        exit(1);
                    }
                    break;
				case 'o':
                    if (!strcmp(argv[i], "-overwrite")) {
                        config.addNameValuePair("OVERWRITE", "true");
                    }
                    else {
                        fprintf(stderr,"%s [options]\n",argv[i]);
                        fprintf(stderr,"%s",usage);
                        exit(1);
                    }
                    break;
                case 's' :
					if (!strcmp(argv[i], "-source")) {
						strcpy(filesource, argv[++i]);
						sprintf(filesourcesecond, "%s.2.txt", filesource);
					}
					else if (!strcmp(argv[i], "-size")) {
                        filesize =  atoi(argv[++i]);
                    }
                    else {
                        fprintf(stderr,"%s [options]\n",argv[i]);
                        fprintf(stderr,"%s",usage);
                        exit(1);
                    }
                    break;
                case 't' :
					if (!strcmp(argv[i], "-target")) {
						strcpy(filetarget, argv[++i]);
					}
                    else {
                        fprintf(stderr,"%s [options]\n",argv[i]);
                        fprintf(stderr,"%s",usage);
                        exit(1);
                    }
                    break;
				default :
					break;
			}
		}
	}

	std::cout << "SDM MSS test" << std::endl;
/*
	std::cout << "hsi test start" << std::endl;
	char cmd[1024];
	sprintf(cmd, "#! /bin/sh\n \
	%s -q \"out %s; ls -l %s ; end\" \
	echo \"HSI_ERROR_CODES=\"$? >> %s \
	echo \"Date: `date`\" >> %s",
	hsi, output, target,
	output,
	output);

   FILE *pftpPtr;
    if ((pftpPtr=popen(cmd, "r")) != NULL) {
        pclose(pftpPtr);
    }

	std::cout << "please see output ./testoutput.txt and send it to asim@lbl.gov" << std::endl;
	std::cout << "done" << std::endl;
*/

	FileFetcher         *fetcher=0;             // fetcher object
	try {
	    fetcher = new FileFetcherHSI(msslocation, debugLevel);
	} catch(char const* s) {
	    std::cout << s << std::endl;
	    return -1;
	}

    std::cout << "HSI connection is being checked..." << std::endl;
    std::cout << "    Make sure you have a proper credentials to access HPSS..." << std::endl;
    std::cout << "    e.g. at NERSC, DCE credential is needed with hsi -l" << std::endl;
    try {
        if ( ((FileFetcherHSI*) fetcher)->checkHSI() != mss_connection_ok ) {
            std::cout << "CAN'T LOGIN TO HSI ... HSI login is wrong..." << std::endl;
            std::cout << "Check the HSI manually, and try again" << std::endl;
            return -1;
        } else {
            std::cout << "\tHSI login/connection Ok." << std::endl;
        }
    } catch(char const* s) {
    	std::cout << "HSI check failed..." << std::endl;
        std::cerr << s << std::endl;
        return -1;
    }

    std::cout << "PUT test first..." << std::endl;
    try {
		fetcher->mssFilePut(filesource, filetarget, filesize);
	} catch(char const* s) {
    	std::cout << "PUT test failed..." << std::endl;
        std::cerr << s << std::endl;
        return -1;
    }

    std::cout << "GET test second..." << std::endl;
    try {
        fetcher->mssFileGet(filetarget, filesourcesecond, filesize);
    } catch(char const* s) {
    	std::cout << "GET test failed..." << std::endl;
        std::cerr << s << std::endl;
        return -1;
    }


	std::cout << "ALL done" << std::endl;

	return 0;

}
