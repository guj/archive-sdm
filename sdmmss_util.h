#ifndef HRM_UTIL_H
#define HRM_UTIL_H
//	File: hrm_util.h
//	Author: Alex Sim <ASim@lbl.gov>
//	Copyright 1998 - 2001 the Regents of the University of California
//
//  Purpose: Contains utility functions
//											   
//	Created: 111601
//	Last Modified: 111601

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <vector>
#include <string>

#include "sdmmss_const.h"
#include "sdmmss_logger.h"
#include <strstream>
#include <cerrno>

//#define edl "\n"

#ifdef _WIN32
	#include <windows.h>
	#include <process.h>	// Win32 threads
		#if !defined(_POSIX_C_SOURCE)
			#define _POSIX_C_SOURCE 200809L
		#endif // !defined(_POSIX_C_SOURCE)
		#include <pthread.h>    // POSIX thread library
#else
	#if !defined(_POSIX_C_SOURCE)
		#define _POSIX_C_SOURCE 200809L
		//#define _POSIX_C_SOURCE 199506L
	#endif // !defined(_POSIX_C_SOURCE)
	#include <pthread.h>	// POSIX threads
#endif

// Caesar's Code
char *rot13(char* phrase);

//
//	Same as strdup() but uses 'new' instead of 'malloc'
//	and if s == 0 then the return value is "(null)"
//
char*strnewdup(const char*s);
char*strdup_zero(const char*s);

//
//	get_token(str, token, tok_chars) gets the next token in str.
//	A token is defined as the characters in str prior to any of 
//	the characters in tok_chars.
//	If token is 0 on entry, space is allocated (using new), otherwise
//	it is assumed token points to memory which is already allocated.
//	RETURN VALUE: the token or 0 if there are no more tokens in str
//
char* get_token(char*& str, char*& token, const char* tok_chars);

#ifndef _WIN32
	#include <stdio.h> // sprintf()
	char *itoa(int value, char *str, int radix=10) ;
#endif 

//
//	Recursivly create directory "dir".
//
void MakeDirectory(const char*dir);

int checkConfigFile();
int checkHPSS(); 
int checkHPSSError(char* fid);

class Config;

class aOut {
public:
	enum outMethod {out_display, out_file};
	aOut(Config& config, outMethod method=out_display, int df=0);
	virtual ~aOut();

	virtual void out(char const* ss);
	virtual void out(int const ss);
	virtual void out(long const ss);
	virtual void out(long long const ss);
	virtual void out(unsigned long const ss);
	virtual void out(float const ss);
	virtual void out(double const ss);
	virtual void out(std::string ss);

	aOut& operator << (char const * ss) { if (dlevel > 0) out(ss); return *this; }
	aOut& operator << (int const ss) { if (dlevel > 0) out(ss); return *this; }
	aOut& operator << (long const ss) { if (dlevel > 0) out(ss); return *this; }
	aOut& operator << (long long const ss) { if (dlevel > 0) out(ss); return *this; }
	aOut& operator << (unsigned long const ss) { if (dlevel > 0) out(ss); return *this; }
	aOut& operator << (float const ss) { if (dlevel > 0) out(ss); return *this; }
	aOut& operator << (double const ss) { if (dlevel > 0) out(ss); return *this; }
	aOut& operator << (std::string ss) { if (dlevel > 0) out(ss); return *this; }

	void atime();
	void alog(int level=0, char const* s_event=0, char const* s_query_token=0, 
		char const* fid=0, char const* custom_s=0 );

	int dlevel;

protected:

private:
	pthread_mutex_t outLock;
	pthread_mutex_t alogLock;
	char logFile[1024];
	char logFileDetail[1024];
	Logger* logger;
	bool logFlag;
	FILE* logfd;
};


bool acmp(const char* left, const char* right);
bool acmp(char* left, char* right);
bool acmp(int left, int right);
bool acmp(unsigned short left, unsigned short right);
bool acmp(unsigned long left, unsigned long right);

int round(int n, int m);
long round(long n, int m);
long round(unsigned long n, int m);
long round(double n, int m);


#endif // HRM_UTIL_H

