//	File: hrm_config.h
//	Author: Alex Sim <ASim@lbl.gov>
//	Copyright 2001 the Regents of the University of California
//
//  Purpose: The declaration of class Config
//
//	Notes:
//		class Config handles reading of a configuration from a file.
//		See hrm_config.cpp for usage.
//
//	Created: 111601
//	Last Modified: 111601

#ifndef HRM_CONFIG_H
#define HRM_CONFIG_H

#include <list>			// list<>
#include <utility>		// pair<>, make_pair()

#include <cstring>		// strlen(), strcpy()

#include "sdmmss_const.h"	// REASON etc.
#include "sdmmss_util.h"	// strnewdup()

#if (defined _WIN32 || defined OS_NO_ALLOCATORS || !defined OS_NO_DEFAULT_TEMPLATE_ARGUMENTS)
        typedef std::list< std::pair< char*, char* > > listpair;
#else
        typedef std::list< std::pair< char*, char* >, allocator< std::pair< char*, char* > > > listpair;
#endif

class Config {
public:

	explicit Config(const char *prefix, const char *fn=0, bool read_default_config=true) :
		m_prefix( strnewdup(prefix) ), m_verbose( false ), m_is_ok( false )
	{
		if(fn || read_default_config) readConfig(fn);
	}

	Config(const Config& rhs) :
		m_entries( rhs.m_entries ),
		m_prefix( rhs.m_prefix? strnewdup(rhs.m_prefix) : strnewdup("none") ),
		m_verbose( rhs.m_verbose ), m_is_ok( rhs.m_is_ok )
	{
	}

	virtual ~Config();

	Config& operator = (const Config& rhs);
	const char* operator [] (const char *name) const;
	const char* getValue (const char *name) const {	return operator[](name); }

	void addNameValuePair( const char *name, const char *value )
	{
		if( !name || !value ) return;
//		m_entries.push_front( std::make_pair(strnewdup(name), strnewdup(value)) );
		m_entries.push_front( std::make_pair(strnewdup(name), strdup_zero(value)) );
	}

	void readConfig(const char* fn=0);
	void printConfig() const;

	void setVerbose(bool v) { m_verbose = v; }
	
	bool is_ok() const { return m_is_ok; }
	bool is_true(const char* name) const ;
	bool is_false(const char* name) const { return !is_true(name); }

private:

	listpair m_entries;
	char *m_prefix;
	bool m_verbose;
	bool m_is_ok;

};

#endif // HRM_CONFIG_H
