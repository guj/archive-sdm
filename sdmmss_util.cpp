//	File: gc_util.cpp
//	Author: Henrik Nordberg <HNordberg@lbl.gov> Lawrence Berkeley National Laboratory
//	Copyright 1998 - 1999 the Regents of the University of California
//
//  Purpose: Contains utility functions
//											   
//	Created: 970918
//	Last Modified: 990715

#include <iostream>
#include <fstream> 

#include <string.h>		// strlen(), strcpy(), strrchr()
#include <stdio.h>		// sprintf()
#include <ctype.h>		// rot13

#include <sys/stat.h>		// mkdir() on UNIX systems, stat()
#include <sys/types.h>		// mkdir() on UNIX systems, stat_t

#ifdef _WIN32
	#include <direct.h>		// mkdir()
#endif // defined(_WIN32)

#include "sdmmss_const.h"	// mmax()
#include "sdmmss_util.h"	// include the prototypes to give the compiler a chance to correct us

#include "sdmmss_config.h"
#include "sdmmss_fetcher.h"

//extern Config config;
//extern mss_location_t msslocation;

char *rot13(char* phrase) {
    char *rstring = new char[strlen(phrase)];;
    int count = 0;
    char c = '!';       //initialize c

    while(c) {
        c = phrase[count];
        if(isalpha(c)) {
            if(toupper(c)>='A' && toupper(c)<='M')
                c+=13;
            else
                c-=13;
        }
        rstring[count] = c;
        count++;
    }
    return rstring;

}


//
// Same as strdup() but uses 'new' instead of 'malloc'
//
char* strnewdup(const char*s)
{
	char*str;
	if( s!=0 && s[0]!=0 )
	{
//		str=new char[strlen(s)+1];
//		strcpy(str,s);
		str = strdup(s);
	}
	else
	{
//		str=new char[1];
//		strcpy(str,"");
		str=strdup("");
	}
	return str;
}

char* strdup_zero(const char*s)
{
    char*str;
    if( s!=0 && s[0]!=0 )
    {
        str=new char[strlen(s)+1];
        strcpy(str,s);
    }
    else
    {
        str=strdup("");
    }
    return str;
}


//
//	get_token(str, token, tok_chars) gets the next token in str.
//	A token is defined as the characters in str prior to any of 
//	the characters in tok_chars.
//	If token is 0 on entry, space is allocated (using new), otherwise
//	it is assumed token points to memory which is already allocated.
//	RETURN VALUE: the token or 0 if there are no more tokens in str
//
char* get_token(char*& str, char*& token, const char* tok_chrs)
{
	if( !str || !*str ) return 0;
	char* pc = strpbrk(str,tok_chrs);
	if( pc ) *pc=0;
	if( token ) strcpy(token,str);
	else token = strnewdup(str);
	if( pc ) str=pc+1;
	else str=0;
	return token;
}

#ifndef _WIN32
#include <stdio.h>	// sprintf()
char *itoa(int value, char *str, int /* radix */)
{
	sprintf(str,"%d",value);
	return str;
}
#endif 

//
//	Recursivly create directory "dir".
//
void MakeDirectory(const char*dir)
{
#ifdef _HRM64
	struct stat64 st;
	if( stat64(dir,&st) == 0 ) return;
#else
	struct stat st;
	if( stat(dir,&st) == 0 ) return;
#endif

	char *subdir=new char[PATH_MAX];
	char *recdir=new char[PATH_MAX]; *recdir = 0;
	char *pdir = strnewdup(dir);
	char *pdir_copy = pdir;
	while( get_token(pdir, subdir, "/\\") )
	{
		strcat(recdir,subdir);
		strcat(recdir,"/");
#ifdef _HRM64
		if( stat64(recdir,&st) == 0 ) continue;
#else
		if( stat(recdir,&st) == 0 ) continue;
#endif
		if( mkdir(recdir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1 && errno!=EEXIST )
		{
			char s[PATH_MAX+200];
			sprintf(s,"Failed to create directory \"%s\".\n",recdir);
			delete[] subdir;
			delete[] recdir;
			delete[] pdir_copy;
			throw s;
		}
		if( chmod( recdir, S_IRWXU | S_IRWXG | S_IRWXO ) == -1 && errno != EPERM )
		{
			std::cerr << "ERROR: failed to chmod(\"" << recdir << "\")" << std::endl;
		}
	}
	delete[] subdir;
	delete[] recdir;
	delete[] pdir_copy;
}


/*
//
// checkConfigFile() checks the configuration file entries
//
int checkConfigFile() {
	int test = 0;
    if(!config.getValue("EnableRecovery")) {
        cerr << "No entry \"EnableRecovery\" in the configuration file."
        << endl << "\t The default \"EnableRecovery=false\" is assumed." << endl;
    } else {
		if ( config.is_true("EnableRecovery") ) {
			if(!config.getValue("RecoveryPath")) {
				cerr << "No entry \"RecoveryPath\" in the configuration file."
				<< endl << "\t Cannot proceed..." << endl << endl;
				test = 1;
			}
		}
	}
	if ((msslocation == NCAR) && !config.getValue("SSHPublicKey")) {
	    cerr << "No entry \"SSHPublicKey\" in the configuration file."
        		<< endl << "\t A default value will be assumed." << endl;
	}
    if(!config.getValue("SupportedProtocols")) {
        cerr << "No entry \"SupportedProtocols\" in the configuration file."
        << endl << "\t A default value will be assumed." << endl;
    }   
	if (strcasecmp(config.getValue("MSSLOCATION"), "NCAR")) {
	    if(!config.getValue("HSI")) {
	        cerr << "No entry \"HSI\" in the configuration file."
	        << endl << "\t A default value will be assumed." << endl;
	    }   
	    if(!config.getValue("PFTP")) {
	        cerr << "No entry \"PFTP\" in the configuration file."
	        << endl << "\t A default value will be assumed." << endl;
	    }   
	    if(!config.getValue("MSSHostName")) {
	        cerr << "No entry \"MSSHostName\" in the configuration file."
	        << endl << "\t Cannot proceed..." << endl;
			test = 1;
	    }   
	}
    if(!config.getValue("MSSMaximumAllowed")) {
        cerr << "No entry \"MSSMaximumAllowed\" in the configuration file."
        << endl << "\t A default value will be assumed." << endl;
    }   
    if(!config.getValue("EnableLogging")) {
        cerr << "No entry \"EnableLogging\" in the configuration file."
        << endl << "\t The default \"EnableLogging=false\" is assumed." << endl;
    } else {
		if ( config.is_true("EnableLogging") ) {
			if(!config.getValue("TRMLogFile")) {
				cerr << "No entry \"TRMLogFile\" in the configuration file."
				<< endl << "\t Cannot proceed..." << endl << endl;
				test = 1;
			}
		}
	}
    	if(!config.getValue("NSHost")) {
        	cerr << "No entry \"NSHost\" in the configuration file."
        	<< endl << "\t Cannot proceed..." << endl;
			test = 1;
    	}   
    	if(!config.getValue("NSPort")) {
        	cerr << "No entry \"NSPort\" in the configuration file."
        	<< endl << "\t Cannot proceed..." << endl;
			test = 1;
    	}   
    	if(!config.getValue("TRMName")) {
        	cerr << "No entry \"TRMName\" in the configuration file."
        	<< endl << "\t Cannot proceed..." << endl;
			test = 1;
    	}   
		if(!config.getValue("TRMRefFile")) {
			cerr << "No entry \"TRMRefFile\" in the configuration file."
        	<< endl << "\t naming service will be used..." << endl;
    	}   
	return test;
}
*/



//////////////////////////////////////////////////////////////////////

aOut::aOut(Config& config, outMethod method, int df) 
{
	if( config.is_true("EnableLogging") ) 
		logFlag = true;
	else
		logFlag = false;
  dlevel = df;
  
  if (logFlag) {
  	strcpy(logFile, config["MSSLogFile"]);
    std::ofstream logFout(logFile, std::ios::app);
    if (!logFout) {
      std::cout << "SDMMSS cannot open a file to write " << logFile << std::endl;
    }
    logFout.close();
    logger = new Logger (logFile, "sdmmss");

	if (df > 9) {
	    strcpy(logFileDetail, config["MSSLogFile"]);
	    strcat(logFileDetail, ".details");
	    std::ofstream logFoutDetail(logFileDetail, std::ios::app);
	    if (!logFoutDetail) {
	      std::cout << "SDMMSS cannot open a file to write " << logFileDetail << std::endl;
	    }
	    logFoutDetail.close();
		if ( chmod(logFileDetail, S_IWUSR | S_IRUSR | S_IROTH | S_IWOTH | S_IRGRP | S_IWGRP) != 0 && errno != EPERM ) std::cerr << "Could not make " << logFileDetail << " writable." << std::endl;

        logfd = fopen(logFileDetail, "a");
        if (logfd) {
			std::cout << "######## MSS Detailed OUTPUT Start #######"  << std::endl;
			fprintf(logfd, "######## MSS Detailed OUTPUT Start #######\n\n" );
			fflush(logfd);
        }
		else {
	      std::cout << "SDMMSS cannot open a file to write " << logFileDetail << std::endl;
			std::cout << "exiting..." << std::endl;
			exit(1);
		}

	}
  }

  // pthread mutex lock initiation
  pthread_mutexattr_t    mutex_attr;
  pthread_mutexattr_init(&mutex_attr);
#ifndef LINUX
  pthread_mutexattr_setpshared(&mutex_attr, PTHREAD_PROCESS_SHARED);
#endif

  if (pthread_mutex_init(&outLock,&mutex_attr) != 0)
    std::cout << "ERROR: in autil.cpp, mutex_init failed in outLock";
  if (pthread_mutex_init(&alogLock,&mutex_attr) != 0)
    std::cout << "ERROR: in autil.cpp, mutex_init failed in alogLock";
  pthread_mutexattr_destroy(&mutex_attr);
}

aOut::~aOut() 
{
  if (logFlag) {
    std::cout << "deleting logger..."<<std::endl;;
    delete logger;
	if (dlevel > 9) fclose(logfd);
  }

  
  if (pthread_mutex_destroy(&outLock) != 0)
    std::cout << "ERROR: in autil.cpp, mutex_destroy failed in outLock";
  if (pthread_mutex_destroy(&alogLock) != 0)
    std::cout << "ERROR: in autil.cpp, mutex_destroy failed in alogLock";
}

void aOut::alog(int level, char const* s_event, char const* s_query_token, 
				char const* fid, char const* custom_s) {
	pthread_mutex_lock(&alogLock);
	if (dlevel > level) {
#ifdef STDCOUT
		std::cout << s_event;
		if (s_query_token) std::cout << " for query " << s_query_token;
		if (fid) std::cout << " with file " << fid;
		if (custom_s) std::cout << " " << custom_s; 
		std::cout << std::endl;
#else
		printf("%s",  s_event);
		if (s_query_token) printf(" for query %s", s_query_token);
		if (fid) printf(" with file %s", fid);
		if (custom_s) printf(" %s", custom_s); 
		printf("\n");
		if (dlevel > 9) {
//			FILE* logfd = fopen(logFileDetail, "a");
//			if (logfd) {
				fprintf(logfd, "%s",  s_event);
				if (s_query_token) fprintf(logfd, " for query %s", s_query_token);
				if (fid) fprintf(logfd, " with file %s", fid);
				if (custom_s) fprintf(logfd, " %s", custom_s); 
				fprintf(logfd, "\n");
				fflush(logfd);
//				fclose(logfd);
//			}
		}
#endif
	}
	if (logFlag) {
		if (s_query_token) {
			logger->Log(s_event, s_query_token, fid, custom_s);
		}
		else {
			*logger << s_event;
		}
	}
	pthread_mutex_unlock(&alogLock);
}

void aOut::atime() { 
	pthread_mutex_lock(&outLock);
	if (dlevel) {
		time_t currentTime = time(NULL); 
		char buff[256];
	        ctime_r(&currentTime,buff); 
		int n = strlen(buff);
	        if( buff[n-1] == '\n' ) buff[n-1] = 0; 
#ifdef STDCOUT
		std::cout << buff << std::endl;
#else
		printf("%s", buff);
		if (dlevel > 9) {
//            FILE* logfd = fopen(logFileDetail, "a");
//            if (logfd) {
				fprintf(logfd, "%s", buff);
				fflush(logfd);
//                fclose(logfd);
//            }
        }
#endif
	}
	pthread_mutex_unlock(&outLock);
}

void aOut::out(char const * outs) {
	pthread_mutex_lock(&outLock);
	if (outs!=NULL) {
#ifdef STDCOUT
        std::cout << outs;
#else
		printf("%s", outs);
		if (dlevel > 9) {
//            FILE* logfd = fopen(logFileDetail, "a");
//            if (logfd) {
				fprintf(logfd, "%s", outs);
				fflush(logfd);
//                fclose(logfd);
//            }
        }
#endif
	}
	pthread_mutex_unlock(&outLock);
}

void aOut::out(std::string outs) {
    pthread_mutex_lock(&outLock);
    if (!outs.empty()) {
#ifdef STDCOUT
        std::cout << outs;
#else
        printf("%s", outs.c_str());
        if (dlevel > 9) {
//            FILE* logfd = fopen(logFileDetail, "a");
//            if (logfd) {
                fprintf(logfd, "%s", outs.c_str());
                fflush(logfd);
//                fclose(logfd);
//            }
        }
#endif
    }
    pthread_mutex_unlock(&outLock);
}

void aOut::out(int const outs) {
	pthread_mutex_lock(&outLock);
#ifdef STDCOUT
        std::cout << (int) outs;
#else
		printf("%d", outs);
        if (dlevel > 9) {
//            FILE* logfd = fopen(logFileDetail, "a");
//            if (logfd) {
				fprintf(logfd, "%d", outs);
				fflush(logfd);
//                fclose(logfd);
//            }
        }
#endif
	pthread_mutex_unlock(&outLock);
}

void aOut::out(long const outs) {
	pthread_mutex_lock(&outLock);
#ifdef STDCOUT
        std::cout << (long) outs;
#else
		printf("%ld", outs);
        if (dlevel > 9) {
//            FILE* logfd = fopen(logFileDetail, "a");
//            if (logfd) {
				fprintf(logfd, "%ld", outs);
				fflush(logfd);
//                fclose(logfd);
//            }
        }
#endif
	pthread_mutex_unlock(&outLock);
}

void aOut::out(long long const outs) {
	pthread_mutex_lock(&outLock);
#ifdef STDCOUT
        std::cout << (long long) outs;
#else
		printf("%lld", outs);
        if (dlevel > 9) {
//            FILE* logfd = fopen(logFileDetail, "a");
//            if (logfd) {
				fprintf(logfd, "%lld", outs);
				fflush(logfd);
//                fclose(logfd);
//            }
        }
#endif
	pthread_mutex_unlock(&outLock);
}

void aOut::out(unsigned long const outs) {
	pthread_mutex_lock(&outLock);
#ifdef STDCOUT
        std::cout << (unsigned long) outs;
#else
		printf("%lu", outs);
        if (dlevel > 9) {
//            FILE* logfd = fopen(logFileDetail, "a");
//            if (logfd) {
				fprintf(logfd, "%lu", outs);
				fflush(logfd);
//                fclose(logfd);
//            }
        }
#endif
	pthread_mutex_unlock(&outLock);
}

void aOut::out(float const outs) {
	pthread_mutex_lock(&outLock);
#ifdef STDCOUT
        std::cout << (float) outs;
#else
		printf("%f", outs);
        if (dlevel > 9) {
//            FILE* logfd = fopen(logFileDetail, "a");
//            if (logfd) {
				fprintf(logfd, "%f", outs);
				fflush(logfd);
//                fclose(logfd);
//            }
        }
#endif
	pthread_mutex_unlock(&outLock);
}

void aOut::out(double const outs) {
	pthread_mutex_lock(&outLock);
#ifdef STDCOUT
        std::cout << (double) outs;
#else
		printf("%lf", outs);
        if (dlevel > 9) {
//            FILE* logfd = fopen(logFileDetail, "a");
//            if (logfd) {
				fprintf(logfd, "%lf", outs);
				fflush(logfd);
//                fclose(logfd);
//            }
        }
#endif
	pthread_mutex_unlock(&outLock);
}

////////////////////////////////////////////////////////////////

bool acmp(const char* left, const char* right) {
	if (strcmp(left, right))
		return false;
	return true;
}

bool acmp(char* left, char* right) {
	if (strcmp(left, right))
		return false;
	return true;
}

bool acmp(int left, int right) {
	if (left == right)
		return true;
	return false;
}

bool acmp(unsigned short left, unsigned short right) {
	if (left == right)
                return true;
        return false;
}

bool acmp(unsigned long left, unsigned long right) {
        if (left == right)
                return true;
        return false;
}

int round(int n, int m) {
	int d=1;
	for (int i=0; i<m; i++) d *= 10;
	int result = (int) floor( (double) ((n + d / 2) / d) );
	return result;
}

long round(long n, int m) {
	int d=1;
	for (int i=0; i<m; i++) d *= 10;
	long result = (long) floor( (double) ((n + d / 2) / d) );
	return result;
}

long round(unsigned long n, int m) {
	int d=1;
	for (int i=0; i<m; i++) d *= 10;
	long result = (long) floor( (double) ((n + d / 2) / d) );
	return result;
}

long round(double n, int m) {
        double d=1.0;
        for (int i=0; i<m; i++) d *= 10.0;
        long result = (long) floor( (double) ((n + d / 2.0) / d) );
        return result;
}

